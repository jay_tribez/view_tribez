//This file is mainly used for listing out the navigation among views of various components in a our Application.
import React from 'react';
const Loader = React.lazy(() => import('./Components/loader'))

//Define router paths
const routes = [
    { path: '/loading', name: 'Search', component: Loader },
    
]
export default routes;