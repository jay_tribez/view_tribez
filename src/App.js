//This file is mainly used for listing out the navigation among views of various components in a our Application,
//allows changing the browser URL, and keeps the UI in sync with the URL.
import React, { Component, Suspense } from "react";
import { BrowserRouter as Router, Switch, Route, } from "react-router-dom";
import Loader from "./Components/loader"
import './App.css';

// const Login = React.lazy(() => import('./Components/Login/Login'))
// const AppLayout = React.lazy(() => import('./appLayout'))
const CollectionLink = React.lazy(() => import('./Components/collectionLinkPage/collectionLinkPage'))
const ResourceNotFound = React.lazy(() => import('./Components/ResourceNotFound/resourceNotFound'))
const CalendSoPage = React.lazy(() => import('./Components/calendSoPage/calendSoPage'))
const CollectionDocEditor = React.lazy(() => import('./Components/CollectionDocEditor/CollectionDocEditor'))
const CropAssetImage = React.lazy(() => import('./Components/assetPreview/cropAssetImage'))
var env = require('./env').default;

// console.log(env.default)
class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      keycloak: null,
      authenticated: false,
      env: {}
    }
  }
  loading = () => {
    return <Loader />
  }

  componentDidMount() {
    this.setEnvironment('dev') //set default environment as 'dev'

    // const keycloak = Keycloak('/keycloak.json');
    // keycloak.init({ onLoad: 'login-required' }).then(authenticated => {
    //   this.setState({ keycloak: keycloak, authenticated: authenticated },()=>{
    //     localStorage.setItem("keycloak", JSON.stringify(keycloak))
    //     console.log(keycloak)
    //   })
    // })
    // console.log('state',this.state.keycloak,this.state.authenticated);
  }
  setEnvironment = (type) => {
    this.setState({ env: env[type] })
    localStorage.setItem('env', JSON.stringify(env[type]))
  }

  render() {

    // if (this.state.keycloak) {
    //   if (this.state.authenticated) {
    return (
      // Main routing 
      <div>
        {/* <LanguageSettings /> */}
        <Router>
          <Suspense fallback={this.loading()}>
            <div>
              <Switch>
                <Route exact path={['/', '/resources']}>
                  <ResourceNotFound />
                </Route>
                <Route exact path="/:linkid">
                  <CollectionLink />
                </Route>
                <Route exact path="/loading">
                  <Loader />
                </Route>
                <Route exact path="/calendso/followup">
                  <CalendSoPage username={"Varshni Gurumoorthy"} scheduleText={"Tribez Follow on Conversation"} poweredby={"CalendSo"} />
                </Route>
                <Route exact path="/assets/image/:id/edit">
                  <CropAssetImage />
                </Route>
                <Route exact path="/assets/:id">
                  {/* <AssetPreview /> */}
                  <CollectionDocEditor />
                </Route>
                {/* <Route path="/collections/:cid/:id">
                  <PersonalizedAssetView />
                </Route>
                <Route path="/collections/:cid/sections/:id">
                  <CollectionSectionView />
                </Route>
                <Route path="/collections/:cid/s/:sid/:id">
                  <CollectionSectionAnalyticView />
                </Route> */}
              </Switch>
            </div>
          </Suspense>

        </Router>
      </div>
    );
    //   }
    //   else {
    //     return (<div>Unable to authenticate!</div>)
    //   }
    // }
    // else return (<div>Initializing!</div>)
  }
}

export default App;
