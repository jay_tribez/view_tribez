//This file is to add multiple environment apis

//DEVELOPMENT Environment APIS
const dev = {
    type: 'dev',
    authenticateUrl: 'http://127.0.0.1:8080', //keycloak Authenticate url
    client_id: 'tribez-campaign-app',
    apiUrl:'https://api.tribez.ai/api/v1'
}
let environment = { dev }
export default environment