import React, { Component } from 'react';
import './calendso.scss'

class CalendSoPage extends Component {
    //This is a static calendly page for rendering
    //Use props for dynamic name & schedule text 
    render() {
        return (<React.Fragment>
            <div className="calendly-wrapper">
                <div className="booking-container">
                    <div className="side-panel">
                        <div className="side-panel-header">
                            <img alt="" src={"https://d3v0px0pttie1i.cloudfront.net/uploads/organization/logo/2780198/22a0f958.png"}></img>
                        </div>
                        <div>
                            <img className="profile-pic" alt="" src={"https://d3v0px0pttie1i.cloudfront.net/uploads/user/avatar/9427316/856e6b0d.jpeg"}></img>
                            <p className="username" data-testid="username">{this.props.username}</p>
                            <p className="sub-heading" data-testid="subheading">{this.props.scheduleText}</p>
                            <p className="duration-text"><i class="fas fa-clock"></i> 15 min</p>
                            <p className="video-conference"><i class="fa fa-video-camera" ></i> Web conferencing details provided upon <br /> <span style={{ marginLeft: 40 }}>confirmation.</span></p>
                            <p className="comment-text">Here's where you can choose a slot at a time of <br />your convenience, where we'll discuss the next <br></br>steps.</p>
                        </div>
                    </div>
                    <div className="rightside-panel">
                        <div className="rightside-panel-ribbon">
                            <div data-id="branding" class="branding">
                                <div class="poweredby-text" >powered by</div>
                                <div class="calendly-text" data-testid="poweredby">{this.props.poweredby}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>);
    }
}

export default CalendSoPage;