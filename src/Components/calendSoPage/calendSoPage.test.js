import React from 'react';
import { render, screen, fireEvent } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";
import { mount } from 'enzyme';
import { createMemoryHistory } from "history";
import CalendlyPage from './calendSoPage';
// import SearchComponenet from "../search/searchComponent";

describe('Initial component rendering', () => {
    it('on render', async () => {
        let wrapper = render(
            <CalendlyPage
                username={"Varshni Gurumoorthy"}
                scheduleText={"Paperflite Follow on Conversation"}
                poweredby={"Calendly"} />
        );
        expect(wrapper).toMatchSnapshot()
    })
    it('Check for username and calendly text and poweredby should render', async () => {
        let wrapper = render(
            <CalendlyPage username={"Varshni Gurumoorthy"}
                scheduleText={"Paperflite Follow on Conversation"}
                poweredby={"Calendly"} />
        )
        expect(wrapper.getByTestId('username').textContent).toBe('Varshni Gurumoorthy')
        expect(wrapper.getByTestId('subheading').textContent).toBe('Paperflite Follow on Conversation')
        expect(wrapper.getByTestId('poweredby').textContent).toBe('Calendly')
    })
})