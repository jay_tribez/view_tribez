/**
 * Default sample
 */
//  import * as ReactDOM from 'react-dom';
import * as React from "react";
import { withRouter } from "react-router-dom";
import { ProgressBarComponent } from '@syncfusion/ej2-react-progressbar';
import "./index.scss"

class Loader extends React.Component {
    render() {
        return (
            <div className='control-pane'>
                <div className="control-section progress-bar-parent">
                    <div className="row">
                        <div className="col-lg-3 col-md-3 col-3 progress-container">
                            <div className="row progress-container-align">
                                <div className="col-lg-12 col-md-12 col-12">
                                    <ProgressBarComponent id="rounded-container"
                                        type='Circular'
                                        width='60px'
                                        height='60px'
                                        cornerRadius='Round'
                                        isIndeterminate={true}
                                        value={20}
                                        animation={{
                                            enable: true,
                                            duration: 2000,
                                            delay: 0,
                                        }}
                                    >
                                    </ProgressBarComponent>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default withRouter(Loader);