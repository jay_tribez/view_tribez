import React from 'react'
import { withRouter } from 'react-router';
import "./index.scss"

const SidePreviewPanel = (props) => {
    const closeClick = () => {
        props.closeClick()
    }
    const submitClick = () => {
        props.submitClick()
    }
    const cancelClick = () => {
        props.cancelClick()
    }
    return (
        <div className="slide-content-wrap">
            <div className="slide-content-container" style={{ padding: props.isPad && '0px' }}>
                {props.action ? null : <i class="fas fa-times-circle close-icon" onClick={() => closeClick()}></i>}

                {props.content !== undefined ? <div>{props.content}</div> : <div style={{ width: "100%", float: "left", padding: "60px 40px", boxSizing: "border-box" }}>
                    <div className="side-panel-hd">
                        <h1 style={{ lineHeight: "0.8" }}>{props.hd}<br />{props.subhd ? <span style={{ fontWeight: "normal" }}>{props.subhd}</span> : null}</h1>

                    </div>
                    <div className="side-panel-desc">
                        <p>{props.desc}</p>
                    </div>
                </div>}
                {props.action ? <div className="button-wrapper">
                    {props.submitBtn != undefined ? <div className="preview-btn confirm" onClick={() => submitClick()}><span className="round"><i className="fa fa-angle-right"></i></span>
                        {props.submitBtn}
                    </div> : null}
                    {props.cancelBtn != undefined ? <div data-testid="cancel-test-btn" className="preview-btn cancel" onClick={() => cancelClick()}>{props.cancelBtn}</div> : null}
                </div> : null}

            </div>
        </div>
    )
}
export default withRouter(SidePreviewPanel)