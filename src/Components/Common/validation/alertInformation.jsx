import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { ToastComponent, ToastCloseArgs } from '@syncfusion/ej2-react-notifications';

class UserAlertInformation extends Component {
    constructor(props) {
        super(props);
        this.position = { X: 'Right' }
        this.state = {
            status: {
                "success": {
                    cssClass: 'e-toast-success', icon: 'e-success toast-icons'
                },
                "failure": {
                    cssClass: 'e-toast-danger', icon: 'e-error toast-icons' 
                }
            },
            // toasts: [
            //     { title: 'Warning!', content: 'There was a problem with your network connection.', cssClass: 'e-toast-warning', icon: 'e-warning toast-icons' },
            //     { title: 'Success!', content: 'Your message has been sent successfully.', cssClass: 'e-toast-success', icon: 'e-success toast-icons' },
            //     { title: 'Error!', content: 'A problem has been occurred while submitting your data.', cssClass: 'e-toast-danger', icon: 'e-error toast-icons' },
            //     { title: 'Information!', content: 'Please read the comments carefully.', cssClass: 'e-toast-info', icon: 'e-info toast-icons' }
            // ]
        }
    }
    componentDidMount(){
        this.create()
    }
    create() {
        console.log(this.props)
        // setTimeout(function () {
            this.toastObj.show({ content: this.props.info.message, ...this.state.status[this.props.info.status] },);
        // }.bind(this), 200);
    }
    render() {
        return (<ToastComponent ref={(toast) => { this.toastObj = toast }} id='toast_type' position={this.position} created={this.create.bind(this)}></ToastComponent>)
    }
}
export default withRouter(UserAlertInformation)