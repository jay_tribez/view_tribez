import { withRouter } from "react-router-dom";
import noDataImg from "../../../Assets/images/no-data.png"
const NoDataComponent = (props) => {
    return (
        <div className="no-data-found">
            <h3>{props.title}</h3>
            <img src={noDataImg} />
        </div>
    )
}

export default withRouter(NoDataComponent)
