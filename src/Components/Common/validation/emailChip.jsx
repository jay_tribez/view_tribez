import React from "react";
import { withRouter } from 'react-router-dom';
import "./index.scss"
class EmailChipInput extends React.Component {
    constructor(props) {
        super(props);
        console.log(this.props)
        this.state = {
            items: [],
            value: "",
            error: null,
            label: this.props.label == undefined ? "" : this.props.label,
            actionBtn: this.props.actionBtn == undefined ? [] : this.props.actionBtn,
            ccShow: false,
            bccShow: false,
            ccItems: [],
            bccItems: [],
            ccValue: "",
            bccValue: ""
        };
    }

    handleKeyDown = (evt, type, v) => {
        if (["Enter", "Tab", ","].includes(evt.key)) {
            evt.preventDefault();

            var value = this.state[v].trim();

            if (value && this.isValid(value)) {
                this.setState({
                    [type]: [...this.state[type], value],
                    [v]: ""
                }, () => {
                    this.props.emailrecipient({
                        to: this.state.items,
                        cc: this.state.ccItems,
                        bcc: this.state.bccItems
                    })
                });
            }
        }
    };

    handleChange = (evt, type, v) => {
        this.setState({
            [v]: evt.target.value,
            error: null
        });
    };

    handleDelete = (item, type) => {
        this.setState({
            [type]: this.state[type].filter(i => i !== item)
        }, () => {
            this.props.emailrecipient({
                to: this.state.items,
                cc: this.state.ccItems,
                bcc: this.state.bccItems
            })
        });
    };

    handlePaste = (evt, type, v) => {
        evt.preventDefault();

        var paste = evt.clipboardData.getData("text");
        var emails = paste.match(/[\w\d\.-]+@[\w\d\.-]+\.[\w\d\.-]+/g);

        if (emails) {
            var toBeAdded = emails.filter(email => !this.isInList(email));

            this.setState({
                [type]: [...this.state[type], ...toBeAdded]
            });
        }
    };

    isValid(email) {
        let error = null;

        if (this.isInList(email)) {
            error = `${email} has already been added.`;
        }

        if (!this.isEmail(email)) {
            error = `${email} is not a valid email address.`;
        }

        if (error) {
            this.setState({ error });

            return false;
        }

        return true;
    }

    isInList(email) {
        return this.state.items.includes(email);
    }

    isEmail(email) {
        return /[\w\d\.-]+@[\w\d\.-]+\.[\w\d\.-]+/.test(email);
    }
    actionEvent = (item) => {
        if (item == "CC") {
            this.setState({ ccShow: !this.state.ccShow })
        } else {
            this.setState({ bccShow: !this.state.bccShow })
        }
    }
    render() {
        return (
            <>
                <>

                    {this.state.items.map(item => (
                        <div className="tag-item" key={item}>
                            {item}
                            <button
                                type="button"
                                className="button"
                                onClick={() => this.handleDelete(item, "items")}
                            >
                                &times;
                            </button>
                        </div>
                    ))}
                    <div className="email-input-wrap">
                        {this.state.label.length && <label>{this.state.label}</label>}
                        <input
                            className={"input " + (this.state.error && " has-error")}
                            value={this.state.value}
                            onKeyDown={(e) => this.handleKeyDown(e, "items", "value")}
                            onChange={(e) => this.handleChange(e, "items", "value")}
                            onPaste={(e) => this.handlePaste(e, "items", "value")}
                            autoFocus
                        />

                        {this.state.actionBtn.length > 0 ? this.state.actionBtn.map((item, i) => <span key={i} onClick={() => this.actionEvent(item)}>{item}</span>) : null}
                    </div>
                </>
                {this.state.ccShow && <>
                    {this.state.ccItems.map(item => (
                        <div className="tag-item" key={item}>
                            {item}
                            <button
                                type="button"
                                className="button"
                                onClick={() => this.handleDelete(item, "ccItems")}
                            >
                                &times;
                            </button>
                        </div>
                    ))}
                    <div className="email-input-wrap">
                        <label>CC</label>
                        <input
                            className={"input " + (this.state.error && " has-error")}
                            value={this.state.ccValue}
                            onKeyDown={(e) => this.handleKeyDown(e, "ccItems", "ccValue")}
                            onChange={(e) => this.handleChange(e, "ccItems", "ccValue")}
                            onPaste={(e) => this.handlePaste(e, "ccItems", "ccValue")}
                            autoFocus
                        />
                    </div>
                </>}
                {this.state.bccShow && <>

                    {this.state.bccItems.map(item => (
                        <div className="tag-item" key={item}>
                            {item}
                            <button
                                type="button"
                                className="button"
                                onClick={() => this.handleDelete(item, "bccItems")}
                            >
                                &times;
                            </button>
                        </div>
                    ))}

                    <div className="email-input-wrap">
                        <label>BCC</label>
                        <input
                            className={"input " + (this.state.error && " has-error")}
                            value={this.state.bccValue}
                            onKeyDown={(e) => this.handleKeyDown(e, "bccItems", "bccValue")}
                            onChange={(e) => this.handleChange(e, "bccItems", "bccValue")}
                            onPaste={(e) => this.handlePaste(e, "bccItems", "bccValue")}
                            autoFocus
                        />
                    </div>
                </>}
                {this.state.error && <p className="error">{this.state.error}</p>}
            </>
        );
    }
}
export default withRouter(EmailChipInput);
