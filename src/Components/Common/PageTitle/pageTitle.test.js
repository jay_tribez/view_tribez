import React from 'react';
import { mount, shallow } from 'enzyme';
import PageTitle from "./PageTitle";
import toJson from 'enzyme-to-json';

import { render, screen, fireEvent } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";

describe('<PageTitle />', () => {
    // let wrapper;
    // beforeEach(() => {
    //     wrapper = shallow(<PageTitle />
    //     );
    // });

    it('should render correctly', async () => {
        render(
            <MemoryRouter>
                <PageTitle />
            </MemoryRouter>
        );
        screen.getByTestId('title')
        screen.getByTestId('subtitle')
        // expect(toJson(wrapper)).toMatchSnapshot();
    })
    it('component should load for any title and any subtitle', async () => {
        render(
            <MemoryRouter>
                <PageTitle title="test" subTitle="sub test" />
            </MemoryRouter >
        );
        expect(screen.getByTestId('title').textContent).toBe('test')
        expect(screen.getByTestId('subtitle').textContent).toBe('sub test')
        // expect(toJson(wrapper)).toMatchSnapshot();
    })

    it('On changing the search input value should be editable and for usubmit action', async () => {
        render(
            <MemoryRouter>
                <PageTitle searchBar={true} title="test" subTitle="sub test" />
            </MemoryRouter>
        );
        expect(screen.getByTestId('title').textContent).toBe('test')
        expect(screen.getByTestId('subtitle').textContent).toBe('sub test')

        fireEvent.change(screen.getByTestId('search-input'), { target: { value: "test" } })
        // expect(fireEvent.click(screen.getByTestId('search-button'))).toHaveBeenCalled()
    })


})
