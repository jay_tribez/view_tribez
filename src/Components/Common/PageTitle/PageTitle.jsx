import react, { useState } from "react";
import "../../collections/collection.scss";
import { TextBoxComponent } from '@syncfusion/ej2-react-inputs';
import { ButtonComponent } from '@syncfusion/ej2-react-buttons';

const PageTitle = (props) => {
    //Functional component use useState Hooks to set the states in the component
    const { title, subTitle, searchBar } = props
    const [searchValue, setSearchValue] = useState("")
    //On clicking the search button in the search which will be called to caller component prop.
    const handleSearch = () => {
        props.onHandleSearch()
    }
    //Render method for functional component
    return (
        <div className="page-header-wrapper" style={{ display: "flex" }}>
            <div className="page-header">
                <h6 className="page-title" data-testid="title">{title}</h6>
                <p className="page-subtitle" data-testid="subtitle">{subTitle}</p>
            </div>
            {searchBar &&
                <div className="search-header">
                    <div className="search-box">
                        <TextBoxComponent
                            style={{ width: '100%' }}
                            type="text"
                            data-testid="search-input"
                            className="search-inputBox"
                            placeholder="Type here to search"
                            onChange={(e) => setSearchValue(e.target.value)}
                            value={searchValue}
                        />
                        <ButtonComponent cssClass='e-primary search-btn' data-testid="search-button"
                            onClick={handleSearch}>Search</ButtonComponent>
                    </div>
                </div>
                }
        </div>
    )
}
export default PageTitle