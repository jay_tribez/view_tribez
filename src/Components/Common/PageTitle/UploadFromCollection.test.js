import React from 'react';
import { mount, shallow } from 'enzyme';
import UploadFromCollection from "./UploadFromCollection";
import toJson from 'enzyme-to-json';

import { render, screen, fireEvent } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";


describe('<UploadFromCollection />', () => {
    // let wrapper;
    // beforeEach(() => {
    //     wrapper = shallow(<UploadFromCollection />
    //     );
    // });
    it('On component render ', async () => {
        render(
            <MemoryRouter>
                <UploadFromCollection />
            </MemoryRouter>
        );
        expect(screen.getByTestId('title').textContent).toBe('From streams & collections')
        // fireEvent.change(screen.getByTestId('search-input'), { target: { value: "" } })
        // fireEvent.click(screen.getByTestId('search-button'))
    })


    it('On changing the search input value should be editable and for usubmit action', async () => {
        render(
            <MemoryRouter>
                <UploadFromCollection />
            </MemoryRouter>
        );
        expect(screen.getByTestId('title').textContent).toBe('From streams & collections')
        fireEvent.change(screen.getByTestId('search-input'), { target: { value: "test" } })

    })


})
