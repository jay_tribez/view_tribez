import { ButtonComponent } from '@syncfusion/ej2-react-buttons';
import { TextBoxComponent } from '@syncfusion/ej2-react-inputs';
import React, { useState } from 'react';
import "../../collections/collection.scss";
import "../../CollectionItem/previewNavItem.scss";
import { TabComponent, TabItemDirective, TabItemsDirective } from '@syncfusion/ej2-react-navigations';
import { TreeViewComponent } from '@syncfusion/ej2-react-navigations';
const data = require('../../../Assets/Data/data').default


const UploadFromCollection = (props) => {
    const [activeTab, setActiveTab] = useState(0)
    const [isChecked, setIschecked] = useState(true)
    const [nodeChecked, setNodechecked] = useState(true)
    const [mystreamfield, setMystreamfield] = useState({
        dataSource: data.myStream,
        id: 'id',
        parentID: 'pid',
        text: 'name',
        hasChildren: 'hasChild'
    })
    const [myCollectionfield, setMycollectionfield] = useState({
        dataSource: data.myCollection,
        id: 'id',
        parentID: 'pid',
        text: 'name',
        hasChildren: 'hasChild'
    })

    const selectTab = (tab) => {
        setActiveTab(tab);
    }
    const onGetItem = (item) => {
    }
    return (
        <div className="_upload-from-stream" style={{ paddingTop: '1em' }}>
            <p className="title" data-testid="title">From streams & collections</p>
            <p className="desc">Add assets from streams & existing collections</p>
            <div >
                <div className="search-box" style={{ paddingTop: '10px' }}>
                    <TextBoxComponent
                        style={{ width: '100%' }}
                        type="text"
                        className="search-inputBox"
                        placeholder="Type here to search"
                        value={''}
                        data-testid="search-input"
                    />
                    <ButtonComponent cssClass='e-primary search-btn' data-testid="search-btn"
                    >Search</ButtonComponent>
                </div>
            </div>
            <div className="_addAsset-tabs-wrap">
                <div className="_upload-from-stream" style={{ paddingTop: '1em' }}>
                    <div className="_addAsset-tabs-wrap" style={{ minHeight: '200px', overflowY: 'hidden', border: 'none' }}>
                        <div className="tab-wrapper">
                            <div data-testid="mystream-tab" className={`my-stream-header wrapper ${activeTab === 0 ? 'active' : ''}`} onClick={() => selectTab(0)}>My Streams</div>
                            <div data-testid="mycollection-tab" className={`my-collection-header wrapper ${activeTab === 1 ? 'active' : ''}`} onClick={() => selectTab(1)}>My Collection</div>
                        </div>
                        <hr style={{ margin: 0, padding: 0 }}></hr>
                        {activeTab === 0 && <div className="treeview-wrapper">
                            <TreeViewComponent fields={mystreamfield} showCheckBox={false} onClick={onGetItem} nodeChecked={nodeChecked} />
                        </div>}
                        {activeTab === 1 && <div className="treeview-wrapper">
                            <TreeViewComponent fields={myCollectionfield} showCheckBox={false} nodeChecked={nodeChecked} />
                        </div>}
                    </div>
                </div>

            </div>
        </div>
    )
}

export default UploadFromCollection