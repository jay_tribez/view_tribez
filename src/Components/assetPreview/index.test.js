import React from 'react';
import AssetPreview from "./index";
import { render, screen, fireEvent } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";
import { shallow } from "enzyme";

describe('<AssetPreview />', () => {
    it("on render component page", async () => {
        let wrapper = render(
            <MemoryRouter>
                <AssetPreview />
            </MemoryRouter>
        );
    })
    it("on email input changes", async () => {
        let wrapper = render(
            <MemoryRouter>
                <AssetPreview />
            </MemoryRouter>
        );
        fireEvent.change(screen.getByTestId('emailAddress'))

    })
    it("on submit without email address", async () => {
        let wrapper = render(
            <MemoryRouter>
                <AssetPreview />
            </MemoryRouter>
        );
        fireEvent.click(screen.getByTestId('submit'))

    })
    it("on submit with email address and continue", async () => {
        let wrapper = render(
            <MemoryRouter>
                <AssetPreview />
            </MemoryRouter>
        );
        fireEvent.change(screen.getByTestId('emailAddress'), { target: { value: "guhanr_bigtapp@tribez.ai" } })
        fireEvent.click(screen.getByTestId('submit'))
        
        const component = shallow(<AssetPreview />);
        component.instance().setTimeoutFn();
    })
})