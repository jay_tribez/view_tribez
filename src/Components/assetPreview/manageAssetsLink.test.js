import React from 'react';
import ManageLinks from "./manageAssetsLink";
import { render, screen, fireEvent } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";

describe('<ManageLinks />', () => {
    it("on render component page", async () => {
        let wrapper = render(
            <MemoryRouter>
                <ManageLinks />
            </MemoryRouter>
        );
    })
    it("on managelist select", async () => {
        let wrapper = render(
            <MemoryRouter>
                <ManageLinks />
            </MemoryRouter>
        );
        fireEvent.click(screen.getByTestId('listview_template'))
    })
})