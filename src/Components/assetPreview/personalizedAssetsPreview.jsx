import React, { useState } from 'react';
import { withRouter } from 'react-router-dom';
import {
    PdfViewerComponent, Toolbar, Magnification, Navigation, LinkAnnotation, BookmarkView,
    ThumbnailView, Print, TextSelection, TextSearch, Annotation, FormFields, Inject
} from '@syncfusion/ej2-react-pdfviewer';
import "../CollectionPdf/CollectionPdf.scss";
import { DraggableCore } from 'react-draggable';
import ReactPlayer from 'react-player'
import CollectionDocEditor from '../CollectionDocEditor';
import img_asset from "../../Assets/images/img_asset.jpeg";
import Compose from '../Compose';
import SidePreviewPanel from "../sidePreviewPanel";
import '../CollectionItem/previewNavItem.scss';

const PersonalizedAssetView = (props) => {
    //declaring State variables using useState because of functional components
    // const location = useLocation();
    const yt = "https://www.youtube.com/watch?v=PQb2ekeczRE";
    const [showComposePanel, setShowComposepanel] = useState(false)
    const [isWideScreen, setIsWideScreen] = useState(false)
    //this function used for document view. If props value come pdf, pdf case block will be visible
    const switchContent = (item) => {
        switch (item) {
            case 'PDF':
                return (
                    <PdfViewerComponent id="container"
                        documentPath="PDF_Succinctly.pdf"
                        serviceUrl="https://ej2services.syncfusion.com/production/web-services/api/pdfviewer"
                        style={{ 'height': '640px' }}>
                        <Inject services={[Toolbar, Magnification, Navigation, LinkAnnotation,
                            BookmarkView, ThumbnailView, Print, TextSelection, TextSearch, Annotation,
                            FormFields]} />
                    </PdfViewerComponent>
                )
            case 'IMAGE':
                return (<>
                    <DraggableCore>
                        <div>
                            <img src={img_asset} className="handle mt2"
                                draggable="true" width="300px" alt="content" />

                        </div>
                    </DraggableCore>

                </>)
            case 'VIDEO':
                return (
                    <ReactPlayer style={{ padding: '3em 2em' }} playing={true}
                        controls={true} width="90%" url={yt} />
                )
            case 'DOC':
                return (
                    <CollectionDocEditor />
                )
            default:
                return "Loading ..."
        }
    }
    //Sidepanel expend functionality
    const handleWideWidth = () => {
        setIsWideScreen(true)
    }
    //Sidepanel close
    const handleCloseTemplate = () => {
        setIsWideScreen(false)
    }

    const content = () => {
        return (
            showComposePanel ?
                <Compose
                    handleWideWidth={() => handleWideWidth()}
                    handleCloseTemplate={() => handleCloseTemplate()}
                    data-testid="compose"
                /> : '')
    }
    //close the sidebar
    const closeClick = () => {
        let body = document.getElementsByTagName('body')
        body[0].className = body[0].className.replace(' sidecontentnav')

        setShowComposepanel(false)
    }
    //open the sidebar
    const openClick = () => {
        // this.sidebarobj.show();
        let body = document.getElementsByTagName('body')
        body[0].className = body[0].className + ' sidecontentnav';
        setShowComposepanel(true)

    }

    return (
        <React.Fragment>
            <div className="_collectionpdf_container ">
                {!props.hideTitle && props.hideTitle ?
                    <div className="_collectionpdf_header">
                        <div className="_collectionpdf_pdfName">
                            <p className="title">
                                (sample)Tribez Support Center</p>
                            <p className="desc">151.94 | PDF | Tribez Cloud</p>
                        </div>
                    </div>
                    : ''}
                <div className="__collectionPdf_Main_wrap" style={{
                    "width": "80%",
                    "margin": "2em auto 0"
                }}>
                    {switchContent("PDF")}
                </div>
                <div className="pdf-share-calender-wrap">
                    <i class="fa fa-calendar" aria-hidden="true"></i>
                    <i class="fa fa-share-alt" aria-hidden="true" data-testid="sidepanal-open" onClick={() => openClick()}></i>
                    <i class="fa fa-download" aria-hidden="true"></i>
                </div>
            </div>
            {showComposePanel ?
                <div className={`sidecontentnav templatepreview 
                    ${isWideScreen ? 'share-collection-wrap-wide' : 'share-collection-wrap'}
                    `}>
                    <SidePreviewPanel
                        content={content()}
                        action={true}
                        submitBtn={"Send"}
                        cancelBtn={"Cancel"}
                        cancelClick={() => closeClick()}
                        closeClick={() => closeClick()}
                        isPad={true} data-testid="sidepanel-close" />
                </div>
                :
                null}
        </React.Fragment>

    )
}

export default withRouter(PersonalizedAssetView)