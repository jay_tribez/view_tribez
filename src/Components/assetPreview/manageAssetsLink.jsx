import React, { useState } from 'react'
import { withRouter } from 'react-router';
import PageTitle from '../Common/PageTitle';
import { ListViewComponent } from '@syncfusion/ej2-react-lists';
import DaysBetweenDate from "../../utils/daysBetweenDate"
import "../Compose/conversation.scss";

const ManageLinks = (props) => {
    const [manageLinkData,setManageLinkData]=useState([{
        name:"Guhanraja",
        "activityDate": 1628074932689,
        "email":"guhanr_bigtapp@tribez.ai"
    }])
    const listTemplate=(data)=> {
        let timeDiff = DaysBetweenDate(data.activityDate);
        return (<div className={"clearfix e-list-wrapper e-list-multi-line e-list-avatar"} style={{textAlign:"left",paddingLeft:"6.8em"}}>
            {data.icon !== undefined ?
                <img className='e-avatar  e-avatar-xlarge' src={`${data.icon}`} alt=""/> : <div className='e-avatar  e-avatar-xlarge'>{String(data.name).substr(0,1)}</div>}
            <span className="e-list-content"><i>Last Active {timeDiff}</i></span>
            <span className="e-list-item-header">{data.name} </span>
                <div>
                    <div className="timeStamp"><i>{data.email}</i></div>
                </div>
        </div>);
    }
    const onMangeLink=()=>{
        props.onMangeLink()
    }
    return (
        <div style={{ padding: '0px 30px 0 20px' }}>
            <PageTitle title={"Manage Links"} subTitle={""} searchBar={false} />
            <ListViewComponent data-testid="list-preview" id='listview_template' dataSource={manageLinkData} showHeader={false} cssClass='e-list-template' select={onMangeLink} template={listTemplate}></ListViewComponent>
        </div>
    )
}
export default withRouter(ManageLinks)