import React, { useState, useCallback } from 'react';
import { withRouter } from 'react-router-dom';
import Cropper from 'react-easy-crop';
import { SliderComponent } from '@syncfusion/ej2-react-inputs';
import { ButtonComponent } from '@syncfusion/ej2-react-buttons';
import '../CollectionItem/previewNavItem.scss';
import './index.scss';
import cropImage from "../../Assets/images/_collectionItem_headerbg.jpeg";
import getCroppedImg from './cropImage'

const CropAssetImage = (props) => {
    const [crop, setCrop] = useState({ x: 0, y: 0 })
    const [rotation, setRotation] = useState(0)
    const [zoom, setZoom] = useState(1)
    const [croppedAreaPixels, setCroppedAreaPixels] = useState(null)
    const [croppedImage, setCroppedImage] = useState(null)
    //The image cropped using mouse
    const onCropComplete = useCallback((croppedArea, croppedAreaPixels) => {
        setCroppedAreaPixels(croppedAreaPixels)
    }, [])
    //The cropped image will save
    const showCroppedImage = useCallback(async () => {
        try {
            const croppedImage = await getCroppedImg(
                props.image == undefined ? cropImage : props.image,
                croppedAreaPixels,
                rotation
            )
            // console.log('donee', { croppedImage })
            props.cropImageResponse(croppedImage)
            setCroppedImage(croppedImage)
        } catch (e) {
            // console.error(e)
        }
    }, [croppedAreaPixels, rotation])
    //This function used for zoom and rotate the image
    const onCropOptions = (e, option) => {
        switch (option) {
            case "zoom":
                setZoom(e.value)
                break;
            case "rotation":
                setRotation(e.value)
                break;

            default:
                break;
        }
    }
    //Cancel and back to main page
    const cancelClick = () => {
        props.history.push('/app/collectionItem')
    }
    return (
        <React.Fragment>
            <div className={props.isHeaderNeed == undefined ? "_collectionpdf_container" : "_collectionpdf_container crop-wrap"}>
                {props.isHeaderNeed == undefined ? <div className="_collectionpdf_header">
                    <div className="_collectionpdf_pdfName" style={{ display: "inline-block" }}>
                        <p className="title">
                            (sample)Tribez Support Center</p>
                        <p className="desc">151.94 | PDF | Tribez Cloud</p>
                    </div>
                    <div className="crop-action-wrap" style={{ float: "right", display: "inline-block" }}>
                        <div className="button-wrapper">
                            <div className="preview-btn confirm"
                                data-testid="crop-image-save" onClick={showCroppedImage()}><span className="round"><i className="fa fa-angle-right"></i></span>
                                SAVE
                            </div>
                            <div data-testid="cancel-test-btn"
                                data-testid="cancel" className="preview-btn cancel" onClick={() => cancelClick()}>CANCEL</div>
                        </div>
                    </div>
                </div> : null}
                <div className="crop-container">
                    <Cropper
                        image={props.image == undefined ? cropImage : props.image}
                        crop={crop}
                        zoom={zoom}
                        aspect={4 / 3}
                        onCropChange={setCrop}
                        onCropComplete={onCropComplete}
                        onZoomChange={setZoom}
                        rotation={rotation}
                        onRotationChange={setRotation}
                        data-testid="crop-image"
                    />
                    <div className="crop-option-wrap">
                        <div className="crop-option-zoom">
                            <p>Zoom</p>
                            <SliderComponent
                                value={zoom}
                                min={1}
                                max={3}
                                step={0.1}
                                aria-labelledby="Zoom"
                                data-testid="crop-zoom"
                                change={(e) => onCropOptions(e, "zoom")}
                            />
                        </div>
                        <div className="crop-option-rotate">
                            <p>Rotate</p>
                            <SliderComponent
                                value={rotation}
                                min={0}
                                max={360}
                                step={1}
                                aria-labelledby="Rotation"
                                data-testid="crop-rotate"
                                change={(e) => onCropOptions(e, "rotation")}
                            />
                        </div>
                        {props.submitBtnTxt != undefined ? <div className="crop-save">
                            <ButtonComponent
                                onClick={showCroppedImage}
                                variant="contained"
                                color="primary"
                            >
                                {props.submitBtnTxt}
                            </ButtonComponent>
                        </div> : null}
                    </div>
                </div>
            </div>
        </React.Fragment>

    )
}

export default withRouter(CropAssetImage)