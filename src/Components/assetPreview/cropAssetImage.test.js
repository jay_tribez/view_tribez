import React from 'react';
import CropAssetImage from "./cropAssetImage";
import { render, screen, fireEvent } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";

describe('<CropAssetImage />', () => {
    it("on render component page", async () => {
        let wrapper = render(
            <MemoryRouter>
                <CropAssetImage />
            </MemoryRouter>
        );
    })
    it("crop the image", async () => {
        let wrapper = render(
            <MemoryRouter>
                <CropAssetImage />
            </MemoryRouter>
        );
        fireEvent.click(screen.getByTestId('crop-image'))
    })
    it("zoom on image", async () => {
        let wrapper = render(
            <MemoryRouter>
                <CropAssetImage />
            </MemoryRouter>
        );
        fireEvent.click(screen.getByTestId('crop-zoom'))
    })
    it("rotate on image", async () => {
        let wrapper = render(
            <MemoryRouter>
                <CropAssetImage />
            </MemoryRouter>
        );
        fireEvent.click(screen.getByTestId('crop-rotate'))
    })
    it("on save crop image", async () => {
        let wrapper = render(
            <MemoryRouter>
                <CropAssetImage />
            </MemoryRouter>
        );
        fireEvent.click(screen.getByTestId('crop-image-save'))
    })
    it("on cancel crop image", async () => {
        let wrapper = render(
            <MemoryRouter>
                <CropAssetImage />
            </MemoryRouter>
        );
        fireEvent.click(screen.getByTestId('cancel'))
    })
})