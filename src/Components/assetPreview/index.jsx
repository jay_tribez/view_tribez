import React, { Component } from 'react';
import { withRouter } from 'react-router';

import { ButtonComponent } from '@syncfusion/ej2-react-buttons';
import "./index.scss";
import { TextBoxComponent } from '@syncfusion/ej2-react-inputs';
class AnonymousLogin extends Component {
    constructor(props) {
        //declaring State variables using state obj because of class component
        super(props);
        this.state = {
            bgImage: "https://views.paperflite.com/public/f6833a7f2490565f46333c6c948b6eab.jpeg",
            imageLogo: 'https://d2uav5q06z9nv6.cloudfront.net/60fab5b857b3bb204aba0af2/images/60fac0ccb1c0282f61a7ee66/e3dbe1a8-e3c2-49c1-94af-cc7f4064a8a0',
            emailAddress: '',
            // name: '(sample)Tribez Support Center',
            entityName: '',
            userType: 'anonymous',
            buttonText: 'Continue',
            errMzg: ''
        }
    }
    //On handling the emailAddress field, assign the value which typed , set the values in emailAddress state variable. 
    handleInput = (e) => {
        this.setState({ emailAddress: e.target.value, errMzg: '' });
    }
    //on validating the emailAddress field and values cannot be empty. If its valid redirect into specific collection item / Asset Item page
    // Button text field also replaced to 'Processing' when its valid else 'Continue' if its invalid 
    //Show the error message under the email input field
    handleContinue = () => {
        if (this.state.emailAddress.length !== 0 && this.state.emailAddress.includes('@')) {
            this.setState({ buttonText: 'Processing..', errMzg: '' });
            setTimeout(() => {
                // this.props.history.push('/collections/6149407886208413ede9a15a')
            }, 1500);
        }
        else {
            this.setState({ buttonText: 'Continue', errMzg: 'Enter a valid email' })
        }
    }
    //Rendering asset preview component
    render() {
        return (
            this.state.userType === 'anonymous' ?
                <React.Fragment>
                    <div className="overall-wrapper">
                        <div style={{
                            backgroundImage: `url(${this.state.bgImage})`,
                            backgroundRepeat: 'no-repeat',
                            height: '100vh',
                            width: '100%',
                            backgroundSize: '100%',
                            position: "absolute",
                            top: 0,
                            bottom: 0,
                            left: 0,
                            backgroundPosition: 'center center',
                            right: 0
                        }}>
                        </div>
                        <div className="email-request-wrapper">
                            <div className="get-email-form">
                                <div className="image-wrapper">
                                    <img src={this.state.imageLogo} alt="" className="image-pos"></img>
                                </div>
                                <p className="label-hd">May we know who we are talking to, please?</p>

                                <TextBoxComponent floatLabelType="Auto"
                                    data-testid="emailAddress"
                                    name="emailAddress"
                                    type="email"
                                    className="anonymous-emailAddress"
                                    style={{ width: '100%' }}
                                    placeholder={'Please enter your email here'}
                                    onChange={(e) => { this.handleInput(e) }}
                                    value={this.state.emailAddress} />
                                {this.state.errMzg && <p className="warning-mzg" style={{ fontSize: "12px", color: "red" }}>{this.state.errMzg}</p>}
                                <ButtonComponent cssClass='e-primary continue-btn' data-testid="submit" name="continue"
                                    onClick={this.handleContinue}>{this.state.buttonText}</ButtonComponent>
                            </div>
                        </div>
                        <div className="content-wrapper">
                            <p className="asset-name-hd">{this.state.entityName}</p>
                            <p className="desc-text">We would like to know who is reading this please. All we need is an email.</p>
                        </div>
                    </div>
                </React.Fragment>
                : null
        );
    }
}

export default withRouter(AnonymousLogin);
