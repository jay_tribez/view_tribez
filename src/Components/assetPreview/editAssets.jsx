import { TextBoxComponent } from '@syncfusion/ej2-react-inputs';
import React, { useState } from 'react';
import { withRouter } from 'react-router-dom';
import { CheckBoxComponent } from '@syncfusion/ej2-react-buttons';
import { DropDownListComponent } from '@syncfusion/ej2-react-dropdowns';
import { RichTextEditorComponent, Toolbar, Inject, Image, Link, HtmlEditor, Count, QuickToolbar, Table } from '@syncfusion/ej2-react-richtexteditor';
const EditAssets = (props) => {

    const [assetData, setAssetData] = useState({
        assetName: "(sample)Tribez Support Center",
        assetImg: 'https://d2uav5q06z9nv6.cloudfront.net/60fab5b857b3bb204aba0af2/images/6142e740a4114c1ae09e2419/afbc0e7e-f240-4028-b38e-9fef130d6a2b',
        size: '151KB',
        contentType: 'Image',
        imgSource: 'Tribez Cloud',
        publisher: "Karthi",
        lastUpdated: "16 Sep 2021"
    })
    const [campaignData,setCampaignData]=useState([])
    return (
        <div className="edit-assets-wrap">
            <div className="analytics-image">
                <img src={assetData.assetImg} className="asset-image-preview" alt="Asset image preview"></img>
                <div class="_1Hdg">
                    <div class="_1YY7">
                        <div class="_1T4f">
                            <label>Source:</label>
                            <a href="#">Paperflite Cloud<i class="fa fa-external-link" aria-hidden="true"></i></a>
                        </div>
                        <div>
                            <label>Publisher:</label>
                            <span>Karthi</span>
                        </div>
                        <div>
                            <label>Last update:</label>
                            <span>16 Sep 2021</span></div></div>
                    <div class="_2vkE"><i class="fa fa-upload" aria-hidden="true"></i>UPLOAD NEW VERSION<input type="file" id="uploadStreamIcon" /></div></div>
            </div>
            <div>
                <p style={{ textAlign: 'left' }}>
                    <span className="assets-spec">{assetData.size}</span>
                    <span className="assets-spec">{assetData.contentType}</span>
                    <span className="assets-spec">{assetData.imgSource}</span>
                </p>
            </div>

            <div className="input-wrap">
                <p className="name-title">NAME</p>
                <TextBoxComponent
                    cssClass="e-outline" floatLabelType="Auto"
                    type="text" placeholder="Enter Name"
                    //  onChange={(e) => { this.setState({ searchUser: e.target.value }) }}
                    value={'GUIDES'}
                />
                <p className="name-title" style={{ paddingTop: '1em' }}>DESCRIPTION</p>
                <RichTextEditorComponent id="toolsRTE"
                    showCharCount={true}
                >
                    <Inject
                        services={[Toolbar, Image, Link, HtmlEditor, Count, QuickToolbar, Table]} />
                </RichTextEditorComponent>
                <p className="name-title name-title-pad10">Tags</p>
                <TextBoxComponent
                    cssClass="e-outline" floatLabelType="Auto"
                    type="text" placeholder="Enter Tag"
                />
                <p className="name-title name-title-pad10">ASSET ACCESS</p>
                <div style={{ float: "left", width:"100%", textAlign: "left", paddingBottom:"15px" }}>
                    <CheckBoxComponent label='Share' ></CheckBoxComponent>&nbsp;
                    <CheckBoxComponent label='Download' ></CheckBoxComponent>
                </div>
                <p className="name-title name-title-pad10">CAMPAIGN STAGE</p>
                <DropDownListComponent id="games" dataSource={campaignData} placeholder="CAMPAIGN STAGE" popupHeight="220px" />
                <p className="name-title name-title-pad10">CONTENT TYPE</p>
                <DropDownListComponent id="games" dataSource={campaignData} placeholder="CONTENT TYPE" popupHeight="220px" />

            </div>
        </div>
    )
}
export default withRouter(EditAssets)