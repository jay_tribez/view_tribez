import React from 'react';
import PersonalizedAssetView from "./personalizedAssetsPreview";
import { render, screen, fireEvent } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";

describe('<PersonalizedAssetView />', () => {
    it("on render component page", async () => {
        let wrapper = render(
            <MemoryRouter>
                <PersonalizedAssetView />
            </MemoryRouter>
        );
    })
    it("on sidepanel open", async () => {
        let wrapper = render(
            <MemoryRouter>
                <PersonalizedAssetView />
            </MemoryRouter>
        );
        fireEvent.click(screen.getByTestId('sidepanal-open'))
    })
    it("on sidepanel close", async () => {
        let wrapper = render(
            <MemoryRouter>
                <PersonalizedAssetView />
            </MemoryRouter>
        );
        fireEvent.click(screen.getByTestId('sidepanal-open'))
        // fireEvent.click(screen.getByTestId('sidepanel-close'))
        
        console.log(wrapper,'erapper')
    })
    it("on composs event", async () => {
        let wrapper = render(
            <MemoryRouter>
                <PersonalizedAssetView />
            </MemoryRouter>
        );
        console.log(wrapper)
        // fireEvent.click(screen.getByTestId('sidepanal-open'))
        // fireEvent.click(screen.getByTestId('compose'))
    })
})