import React, { useEffect, useState } from 'react';
import { withRouter } from 'react-router';
import "../CollectionItem/CollectionItem.scss";
import "../hub/index.scss";
import "../CollectionItem/previewNavItem.scss"
import axios from 'axios';
import { useHistory } from 'react-router-dom';
import linkExpired from '../../Assets/images/link-expired.png';
import CollectionPreview from '../collectionPreview/collectionPreview';
import AnonymousLogin from '../assetPreview/index';
import PasswordLoginPage from '../CollectionItem/PasswordLogin/passwordLogin';
// import collectionResponse from './view-tribez-collectionRes.json';
// import { TabComponent } from '@syncfusion/ej2-react-navigations';

// import { RichTextEditorComponent, FileManager, Toolbar, Inject, Image, Link, HtmlEditor, Count, QuickToolbar, Table } from '@syncfusion/ej2-react-richtexteditor';
// import CollectionItem from '../CollectionItem';

function CollectionLink(props) {
    const [collectionId, setCollectionId] = useState("")
    const [env, setEnv] = useState(JSON.parse(localStorage.getItem("env")))
    const [collectionItemData, setCollectionItemData] = useState({})
    const [isLoading, setIsloading] = useState(true)

    const [isLinkActive, setIslinkactive] = useState(true)
    const [isAuthorized, setIsAuthorized] = useState(true)
    const [entityId, setEntityId] = useState('')
    const [linkType, setLinktype] = useState("Personal")
    const [getLinkId, setGetLinkId] = useState('')
    const [linkResponse, setLinkresponse] = useState({})
    const [passwordProtected, setIsPasswordProtected] = useState(false)
    const [linkAuthenticated, setIslinkAuthenticated] = useState(false)

    const sampleLinkIdRes = {
        entityType: "collections",
        entityId: "614afd3029254e27d87f5c3b",
        linkActive: true,
        passwordProtected: true,
        // linkType: 'Anonymous',
        linkType: 'Personal'
    }

    useEffect(() => {
        let getLinkId = window.location.pathname.split('/')[1]
        setGetLinkId(getLinkId)
        localStorage.setItem('linkId', getLinkId)
        let entityId = sampleLinkIdRes.entityId
        localStorage.setItem('entityId', entityId)

        setLinkresponse(sampleLinkIdRes)
        setIslinkactive(sampleLinkIdRes.linkActive)
        setEntityId(sampleLinkIdRes.entityId)
        setLinktype(sampleLinkIdRes.linkType)
        setIsPasswordProtected(sampleLinkIdRes.passwordProtected)

        // setIsloading(false)
        axios.get(`${env['apiUrl']}/link/${getLinkId}`)
            .then(res => {
                let linkResponse = res.data
                setIsloading(false)
                setLinkresponse(linkResponse)
            }).catch((err => {
                let resp = sampleLinkIdRes
                setLinkresponse(sampleLinkIdRes)
                setIslinkactive(resp.linkActive)
                setEntityId(resp.entityId)
                setLinktype(resp.linkType)
                setIsPasswordProtected(resp.passwordProtected)
                setIsloading(false)
            }))

        let payload = {
            "type": "CLICKED",
            linkId: getLinkId,
            // "userId": this.state.userDetails.userId != undefined ? this.state.userDetails.userId != undefined ? this.state.userDetails.userId : null : null
        }
        axios.post(`${env['apiUrl']}/link/analytics`, payload)
            .then(res => {
                console.log('response', res)
            }).catch(e => { })

    }, [])

    const authenticated = (type) => {
        setIslinkAuthenticated(type)
        let getLinkId = window.location.pathname.split('/')[1]
        setGetLinkId(getLinkId)
        localStorage.setItem('linkId', getLinkId)
        let entityId = sampleLinkIdRes.entityId
        localStorage.setItem('entityId', entityId)
        setLinkresponse(sampleLinkIdRes)
        setIslinkactive(sampleLinkIdRes.linkActive)
        setEntityId(sampleLinkIdRes.entityId)
        setLinktype(sampleLinkIdRes.linkType)
        setIsPasswordProtected(false)

        // setIsloading(false)
        axios.get(`${env['apiUrl']}/link/${getLinkId}`)
            .then(res => {
                let linkResponse = res.data
                setIsloading(false)
                setLinkresponse(linkResponse)
            }).catch((err => {
                let resp = sampleLinkIdRes
                setLinkresponse(sampleLinkIdRes)
                setIslinkactive(resp.linkActive)
                setEntityId(resp.entityId)
                setLinktype(resp.linkType)
                setIsloading(false)
            }))
    }
    return (
        <React.Fragment>
            <div className="collection-link-container">

                {!isLinkActive ?
                    <div className="collection-link-container" style={{ width: '100%', display: 'flex', alignItems: 'center', justifyContent: "center" }}>
                        <img src={linkExpired} alt="link expired" style={{ width: '60%' }} />
                    </div>
                    :
                    passwordProtected ?
                        <React.Fragment>
                            <PasswordLoginPage authenticated={authenticated} />
                        </React.Fragment>
                        :
                        (linkAuthenticated && !isLinkActive ?
                            <div className="collection-link-container" style={{ width: '100%', display: 'flex', alignItems: 'center', justifyContent: "center" }}>
                                <img src={linkExpired} alt="link expired" style={{ width: '60%' }} />
                            </div>
                            : <React.Fragment>
                                <div className="">
                                    {linkType.toLowerCase() == "personal" ?
                                        <React.Fragment>
                                            <CollectionPreview linkId={getLinkId} linkResponse={linkResponse} />
                                        </React.Fragment>
                                        : linkType.toLowerCase() == "anonymous" ?
                                            <React.Fragment>
                                                <AnonymousLogin linkId={getLinkId} linkResponse={linkResponse} />
                                            </React.Fragment>
                                            : <React.Fragment>
                                                <p>You are not authorized...!!! </p>
                                            </React.Fragment>
                                    }
                                </div>
                            </React.Fragment>
                        )}
            </div >
        </React.Fragment>

    )
}

export default withRouter(CollectionLink)