import { TextBoxComponent } from '@syncfusion/ej2-react-inputs';
import React, { Component } from 'react';
import addImg from "../../Assets/images/stream_add.png"
import UploadFromCollection from '../Common/PageTitle/UploadFromCollection';
import { UploaderComponent } from '@syncfusion/ej2-react-inputs';
import "./previewNavItem.scss"
import axios from 'axios';
import UserAlertInformation from "../Common/validation/alertInformation"

//Defining as a class Component
class AddAssetCollection extends Component {
    // Constructor method -->The constructor is a method used to initialize an object's state in a class.
    // It automatically called during the creation of an object in a class.
    constructor(props) {
        super(props);
        // Initializing state variables as an object because of class component
        this.state = {
            env: JSON.parse(localStorage.getItem('env')),
            userDetails: JSON.parse(localStorage.getItem('whoami')),
            recipientEmail: '',
            mailSubject: '',
            streamData: [],
            isStreamList: false,
            assetUrl: '',
            uploadedFiles: [],
            userInfoShow: false,
            userInfo: {}
        }
    }
    //On handling input text box for 'Enter Asset URL' to set asseturl in the state variable 
    onHandleAssetUrl = (e) => {
        this.setState({ assetUrl: e.target.value })
    }

    //Add Url function
    handleAddUrl = () => {
    }

    // handleCloseTemplate = () => {
    //     this.props.handleCloseTemplate()
    //     this.setState({ isTemplate: false, isAttachment: false })
    // }

    //On Click function to render the stream lists panel and set the  state to true
    handleAddClick = () => {
        this.setState({ isStreamList: true })
    }
    //On Click function to close the stream lists panel  and set the  state to false
    handleBack = () => {
        this.setState({ isStreamList: false })
    }
    closeClick = () => {
        console.log('uploaded files', this.state.uploadedFiles)
        this.props.closeClick()
    }
    saveChanges = () => {
        let payload = ["615fea18551e1f6e889eed67"] //asset id
        if (this.props.uploadType == "stream") {
            axios.post(`${this.state.env['apiUrl']}/stream/${this.props.id}/assets`, payload)
                .then(res => {
                    this.setState({ userInfoShow: true, userInfo: { status: "success", message: "Assets has been added successully" } })
                    console.log('response', res.data)
                    this.closeClick()
                }).catch(err => {
                    this.closeClick()
                    this.setState({ userInfoShow: true, userInfo: { status: "failure", message: "Failure" } })
                    console.log('error', err)
                })
        }
        else if (this.props.uploadType == "compose") {
        }
        else {
            console.log(this.props)
            let payload2 = {
                "name": this.props.collectionName ? this.props.collectionName : '',
                // "assets": this.state.uploadedFiles ? this.state.uploadedFiles : ["615ffa3edad36c21a1b5dffa"],
                "assets": ["1"],
                "userId": this.state.userDetails.userId != undefined ? this.state.userDetails.userId != undefined ? this.state.userDetails.userId : null : null,
                "folderId": '1'
            }
            axios.post(`${this.state.env['apiUrl']}/collections/create`, payload2)
                .then(res => {
                    this.setState({ userInfoShow: true, userInfo: { status: "success", message: "Assets has been added successully" } })
                    console.log('response', res.data)
                    this.closeClick()
                }).catch(err => {
                    this.setState({ userInfoShow: true, userInfo: { status: "failure", message: "Failure" } })
                    this.closeClick()
                    console.log('error', err)
                })
        }
    }
    onFileSelect = (args) => {
        console.log(args.filesData)
        let files = this.state.uploadedFiles
        for (let i = 0; i < args.filesData.length; i++) {
            const fileItem = args.filesData[i];
            files.push(fileItem)
            this.setState({ uploadedFiles: files });

            let formdata = new FormData()
            formdata.append('file', fileItem.rawFile)
            formdata.append('name', fileItem.name)
            formdata.append('userId', '1')
            formdata.append('contentType', fileItem.rawFile.type)
            axios.post(`${this.state.env['apiUrl']}/file_upload`, formdata)
                .then(res => {
                    console.log('response', res.data)
                    this.setState({ userInfoShow: true, userInfo: { status: "success", message: "File has been uploaded successully" } })
                    // this.closeClick()
                    // this.state.uploadedFiles.push(res.data)
                }).catch(err => {
                    this.setState({ userInfoShow: true, userInfo: { status: "failure", message: "Failure" } })
                    // this.closeClick()
                    console.log('error', err)
                })
        }

    }
    //Render the class componenent
    render() {
        const { isStreamList } = this.state
        return (
            <React.Fragment>
                <div className="_share_dflex" >
                    <div className="_attach-template-Selection _addAsset-main-wrap">
                        <p className="back-btn" onClick={this.handleBack}><i className="fa fa-chevron-left" style={{ fontSize: '12px', paddingRight: '5px' }}></i> {`Back`}</p>
                        {!isStreamList &&
                            <p className="attach-assets" style={{ paddingBottom: '1em', textAlign: 'left' }} data-testid="add-assets-head">Add assets to your email </p>}
                        <div className="add-assets-main-wrap">
                            {!isStreamList ?
                                <div className="add-assets-wrap" style={{ padding: '2em 0' }}>
                                    <img src={addImg} alt="add" width={30} height={35} />
                                    <div className="desc-wrap" style={{ padding: '0px 2em' }}>
                                        <p className="title">From streams & collections</p>
                                        <p className="desc">Choose your assets</p>

                                    </div>
                                    <div className="add-btn" onClick={() => this.handleAddClick()}>
                                        Add +
                                    </div>
                                </div> :
                                <UploadFromCollection />
                            }
                            <div className="add-assets-wrap" style={{ padding: '2em 0' }}>
                                {/* <img src={addImg} alt="add" width={30} height={35} /> */}
                                <i className="fa fa-arrows-alt" style={{ fontSize: '20px', color: '#717274' }}></i>
                                <div className="desc-wrap" style={{ padding: '0px 2.4rem' }}>
                                    <p className="title">Drag & Drop</p>
                                    <p className="desc">Files from anywhere or browse</p>

                                </div>
                                <div className="add-btn">
                                    <div id='dropArea' className='dropArea' ref={this.dropRef}>
                                        <UploaderComponent id='fileUpload' type='file' ref={(scope) => { this.uploadObj = scope; }} selected={this.onFileSelect.bind(this)}></UploaderComponent>
                                    </div>
                                </div>
                            </div>
                            {!isStreamList && <div className="add-assets-wrap" style={{ padding: '2em 0' }}>
                                <img src={addImg} alt="add" width={30} height={35} />
                                <div className="desc-wrap" style={{ padding: '0px 2em' }}>
                                    <p className="title">Add content URL</p>
                                    <p className="desc">Add a web URL link here</p>
                                    <div
                                        style={{ marginTop: '10px' }}>
                                        <TextBoxComponent
                                            cssClass="e-outline" floatLabelType="Auto"
                                            type="text" placeholder="Enter URL"
                                            //  onChange={(e) => { this.setState({ searchUser: e.target.value }) }}
                                            value={this.state.assetUrl}
                                            onChange={this.onHandleAssetUrl}
                                        />
                                    </div>
                                </div>
                                <div className="add-btn" onClick={() => this.handleAddUrl()}>
                                    Add +
                                </div>

                            </div>
                            }
                        </div>
                    </div>
                    {this.props.uploadType !== "compose" ?
                        <div className="_share-assets-wrap">
                            <div className="title-wrap">
                                <p className="template-subheading basicPtag" style={{
                                    fontSize: '20px', textAlign: 'left', width: '65%', padding: '0 2rem', fontWeight: '600'
                                }}>{this.state.uploadedFiles.length} Assets</p>
                                {this.state.uploadedFiles.length == 0 && <div className="no-assets-wrap">
                                    <p className="desc" style={{ color: '#000', textAlign: 'left' }}>
                                        You did not add any document in your collection, please add files from connecting sources / stream &
                                        collection or upload from your computer
                                    </p>
                                </div>}
                            </div>
                        </div> :
                        <div className="_share-assets-wrap">
                            <div className="title-wrap">
                                <p className="template-subheading basicPtag">{this.state.uploadedFiles.length} Assets</p>
                                {this.state.uploadedFiles.length == 0 && <div className="no-assets-wrap">
                                    <p className="vacant">This space vacant / available / to let</p>
                                    <p className="desc">No assets added yet. Click the big pink + to add them from your collections, or just drag & drop them in from your device.</p>
                                </div>
                                }
                            </div>
                        </div>}
                    <div className="button-wrapper popup" style={{ border: 'none', justifyContent: 'flex-end' }}>
                        <div className="preview-btn confirm" onClick={() => this.saveChanges()}>
                            <span className="round"><i className="fa fa-angle-right"></i></span>
                            Save Changes
                        </div>
                        <div className="preview-btn cancel" onClick={() => this.closeClick()}>Cancel</div>
                    </div>
                </div >
                {this.state.userInfoShow ? <UserAlertInformation info={this.state.userInfo} /> : null}
            </React.Fragment>);
    }
}

export default AddAssetCollection;