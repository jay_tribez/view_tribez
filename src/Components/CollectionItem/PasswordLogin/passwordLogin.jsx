import React, { Component } from 'react';
import { withRouter } from 'react-router';

import { ButtonComponent } from '@syncfusion/ej2-react-buttons';
import "../../assetPreview/index.scss";
import { TextBoxComponent } from '@syncfusion/ej2-react-inputs';
import axios from 'axios';

class PasswordLoginPage extends Component {
    constructor(props) {
        //declaring State variables using state obj because of class component
        super(props);
        this.state = {
            env: JSON.parse(localStorage.getItem("env")),
            bgImage: "https://views.paperflite.com/public/f6833a7f2490565f46333c6c948b6eab.jpeg",
            imageLogo: 'https://d2uav5q06z9nv6.cloudfront.net/60fab5b857b3bb204aba0af2/images/60fac0ccb1c0282f61a7ee66/e3dbe1a8-e3c2-49c1-94af-cc7f4064a8a0',
            password: '',
            linkId: localStorage.getItem('linkId') !== undefined ? localStorage.getItem('linkId') : '',
            entityName: localStorage.getItem('entityName') !== undefined ? localStorage.getItem('entityName') : '',
            userType: 'password',
            buttonText: 'Continue',
            errMzg: ''
        }
    }
    //On handling the emailAddress field, assign the value which typed , set the values in emailAddress state variable. 
    handleInput = (e) => {
        this.setState({ password: e.target.value, errMzg: '' });
    }
    //on validating the password field and values cannot be empty. If its valid redirect into specific collection item / Asset Item page
    // Button text field also replaced to 'Processing' when its valid else 'Continue' if its invalid 
    //Show the error message under the email input field
    handleContinue = () => {
        if (this.state.password.length !== 0) {
            this.setState({ buttonText: 'Processing..', errMzg: '' });

            let payload = {
                linkId: this.state.linkId,
                password: this.state.password,
            }
            axios.post(`${this.state.env['apiUrl']}/authenticate`, payload)
                .then((res) => {
                    this.props.history.push({
                        pathname: `/${this.state.linkId}`,
                        state: {
                            authenticated: true
                        }
                    })
                    this.props.authenticated(true)
                }).catch(err => {
                    console.log(err)
                    this.props.history.push({
                        pathname: `/${this.state.linkId}`,
                        state: {
                            authenticated: true
                        }
                    })
                   this.props.authenticated(true)
                })
        }
        else {
            this.setState({ buttonText: 'Continue', errMzg: 'Password cannot be empty' })
        }
    }
    //Rendering asset preview component
    render() {
        return (
            <React.Fragment>
                <div className="overall-wrapper">
                    <div style={{
                        backgroundImage: `url(${this.state.bgImage})`,
                        backgroundRepeat: 'no-repeat',
                        height: '100vh',
                        width: '100%',
                        backgroundSize: '100%',
                        position: "absolute",
                        top: 0,
                        bottom: 0,
                        left: 0,
                        backgroundPosition: 'center center',
                        right: 0
                    }}>
                    </div>
                    <div className="email-request-wrapper">
                        <div className="get-email-form">
                            <div className="image-wrapper">
                                <img src={this.state.imageLogo} alt="" className="image-pos"></img>
                            </div>
                            <p className="label-hd">Please enter your password to continue viewing</p>

                            <TextBoxComponent floatLabelType="Auto"
                                data-testid="password"
                                name="password"
                                type="password"
                                className="anonymous-emailAddress"
                                style={{ width: '100%' }}
                                placeholder={'Enter Password'}
                                onChange={(e) => { this.handleInput(e) }}
                                value={this.state.password} />
                            {this.state.errMzg && <p className="warning-mzg" style={{ fontSize: "12px", color: "red" }}>{this.state.errMzg}</p>}
                            <ButtonComponent cssClass='e-primary continue-btn' data-testid="submit" name="continue"
                                onClick={this.handleContinue}>{this.state.buttonText}</ButtonComponent>
                        </div>
                    </div>
                    <div className="content-wrapper">
                        <p className="asset-name-hd">{this.state.entityName}</p>
                        {/* <p className="desc-text">We would like to know who is reading this please. All we need is an email.</p> */}
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default withRouter(PasswordLoginPage);
