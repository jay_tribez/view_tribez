import { TextBoxComponent } from '@syncfusion/ej2-react-inputs';
import React, { Component } from 'react';
import './previewNavItem.scss';

//Defining a class component for add asset section
class AddAssetSection extends Component {
    //Since there is no need for state variables constructor method is not necessary
    //Render method for the class component
    render() {
        return (<React.Fragment>
            <div className="preview-container-wrap">
                <div className="preview-container">
                    <p className="turnon-template-heading" data-testid="add-section-head">Add Section</p>
                    <div
                        style={{ marginTop: '10px' }}>
                        <TextBoxComponent
                            cssClass="e-outline" floatLabelType="Auto"
                            type="text" placeholder="Enter a section name"
                            value={''}
                        />
                        <p className="sec-input mt20">
                            Organizing your collection in sections is most suited when you have more
                            than ten pieces of content to share.
                            <br />
                            Add multiple sections as your content grows to structure your collection
                            and help your audience get a birds eye view of your story board.
                        </p>
                    </div>

                </div>
            </div>
        </React.Fragment>);
    }
}

export default AddAssetSection;