import React, { Component } from 'react';
import './previewNavItem.scss';

class TurnOffTemplate extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }
    closeClick = () => {

    }
    render() {
        return (<React.Fragment>
            <div className="preview-container-wrap">
                <div className="preview-container">
                    <p className="turnon-template-heading" data-testid="turnon-head">turn ON<br /> Template</p>
                    <p className="template-subheading">Turning this feature ON will mark this Collection as a template.<br />
                        All templates can be found in the ‘Template’ folder.</p>

                </div>
            </div>
        </React.Fragment>);
    }
}

export default TurnOffTemplate;