import React from 'react';
import greyImg1 from "../../Assets/images/grey-dotted-1.png";
import greyImg2 from "../../Assets/images/grey-dotted-2.png";
import greyImg3 from "../../Assets/images/grey-dotted-3.svg";
import greyImg4 from "../../Assets/images/grey-dotted-4.svg";
import greyImg5 from "../../Assets/images/grey-dotted-5.svg";
import greyImg6 from "../../Assets/images/grey-dotted-6.svg";
import greyImg7 from "../../Assets/images/grey-dotted-7.svg";
import { ColorPickerComponent } from '@syncfusion/ej2-react-inputs';



const BannerFilter = (props) => {
    const {handleClose} = props
    const filterList = [{ name: 'none', item: '/', },
    { name: 'grey', img: greyImg1 },
    { name: 'light-grey', img: greyImg2 },
    { name: 'dotted', img: greyImg3 },
    { name: 'line', img: greyImg4 },
    { name: 'cross', img: greyImg5 },
    { name: 'up-white', img: greyImg6 },
    { name: 'down-white', img: greyImg7 },
    { name: 'down-white' }
    ]
    return (
        <div className="_banner-filter" data-testid="banner-filter-container">
            <div className="close-btn-wrap" onClick={handleClose} data-testid="close-banner">
                X
            </div>
            {filterList.map((item, i) => (
                <div className="box-wrap" key={i}>
                    {item.item && <span className="primaryColor">{item.item}</span>}
                    {item.img && <img src={item.img} alt="greyDot" width={20} height={20} />}
                </div>
            ))}
            <div className="color-select-wrap">
                <p>COLOR</p>
                <ColorPickerComponent id='color-picker'></ColorPickerComponent>
            </div>
        </div>
    )
}
export default BannerFilter