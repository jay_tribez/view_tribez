import React, { Component } from 'react';
import { TextBoxComponent } from '@syncfusion/ej2-react-inputs';
import { AccordionComponent, AccordionItemDirective, AccordionItemsDirective } from '@syncfusion/ej2-react-navigations';
import { ButtonComponent, SwitchComponent } from '@syncfusion/ej2-react-buttons';
import { RadioButtonComponent } from '@syncfusion/ej2-react-buttons';
import { DateTimePickerComponent } from '@syncfusion/ej2-react-calendars';
import ManageLinks from "../assetPreview/manageAssetsLink";
import './previewNavItem.scss';

//Defining a class components for generate link panel
class GenerateLink extends Component {
    // Constructor method -->The constructor is a method used to initialize an object's state in a class.
    // It automatically called during the creation of an object in a class.
    constructor(props) {
        // Initializing state variables as an object because of class component
        super(props);
        this.state = {
            recipientEmail: '',
            campaignTag: '',
            isGetLink: false,
            createLink: false,
            copyTxt: "Copy Link",
            isGetAnonymousLink: false,
            createAnonymousLink: false,
            showButton: false,
            isManageLink: false
        }
    }
    //On Clicking the getLink button, new panel should render to get a dynamic link
    //This is a common function which we reused for getting anonymous link and personalized link and Manage link by entering a campaign tag
    handleGetlink = () => {
        this.props.handleWideWidth()
        this.setState({ isGetLink: true, createLink: true, isGetAnonymousLink: false, createAnonymousLink: false, isManageLink: false })
    }
    //On clicking copy link button, the link which we pass will copied to clipboard, will to the operation (CTRL+C)
    onCopyLink = () => {
        navigator.clipboard.writeText("http://localhost:3001/collections/61494ac208d4f33fd3be895b/6142e72fd28b5d6f704dfa82")
        this.setState({ copyTxt: "Copied" }, () => {
            setTimeout(() => {
                this.setState({ copyTxt: "Copy Link" })
            }, 2000);
        })
    }
    //
    //On Clicking the getLink button, new panel should render to get a dynamic link
    getAnonymouslink = () => {
        this.props.handleWideWidth()
        this.setState({ isGetLink: false, createLink: false, isGetAnonymousLink: true, createAnonymousLink: true, isManageLink: false })
    }
    //On clicking copy link button, the anonymous link which we pass will copied to clipboard, will to the operation (CTRL+C)
    onCopyAnonymousLink = () => {
        navigator.clipboard.writeText("http://localhost:3001/assets/6148813938ad163dd0f4f12d/a/6148813we343df3dd0")
        this.setState({ copyTxt: "Copied" }, () => {
            setTimeout(() => {
                this.setState({ copyTxt: "Copy Link" })
            }, 2000);
        })
    }
    //On Clicking the delete link button , which will close the panel for both personalized and anonymous link
    onDeleteLink = () => {
        this.setState({ isGetLink: false, isGetAnonymousLink: false })
    }
    //Following toggle functions which used for switch operation for password,expiry data,Enable NDA, Email gating ec
    toggleSwitchPass = () => {
        return (
            <div className="toggle">
                <label htmlFor="checked" > Set a password </label>
                <SwitchComponent id="checked" checked={false}></SwitchComponent>
            </div>)
    }
    toggleSwitchTime = () => {
        return (
            <div className="toggle">
                <label htmlFor="checked" > Set an expiry date & time </label>
                <SwitchComponent id="checked" checked={false}></SwitchComponent>
            </div>)
    }
    toggleSwitchNDA = () => {
        return (
            <div className="toggle">
                <label htmlFor="checked" > Enable NDA</label>
                <SwitchComponent id="checked" checked={false}></SwitchComponent>
            </div>)
    }
    toggleEmailgating = () => {
        return (
            <div className="toggle">
                <label htmlFor="checked" > Enable email gating</label>
                <SwitchComponent id="checked" checked={false}></SwitchComponent>
            </div>)
    }
    passwordCont = () => {
        return (
            <div className="ad-input">
                <TextBoxComponent floatLabelType="Never"
                    style={{ width: '100%' }} type="password" placeholder="Enter Password"
                    onChange={(e) => { this.setState({ mailSubject: e.target.value }) }}
                    value={this.state.mailSubject} />
            </div>)
    }
    dateAndTimeCont = () => {
        return (
            <div className="ad-input">
                <DateTimePickerComponent></DateTimePickerComponent>
            </div>
        )
    }
    //Accordion content method which will render the following UI
    acrdnContent2 = () => {
        this.setState({ showButton: true })
        return (
            <div className="_share_advanced_settings" style={{ background: '#fff', border: 'none' }}>
                <div className="title">
                    <label htmlFor="checked" > Allow recipients to download </label>
                    <SwitchComponent id="checked" checked={true}></SwitchComponent>
                </div>
                <div className="edit-block">
                    <div id="collaborator-tab" className="_share-radio-group">
                        <RadioButtonComponent label="Editable" name="Collaborators" cssClass="radio-btn" />
                        <RadioButtonComponent label="Read Only" name="Collaborators" cssClass="radio-btn" />
                    </div>
                    <p className="warning-text">*This applies to PPTs, Docs, Google Slides and Google Docs.</p>
                </div>
                <div className="title">
                    <label htmlFor="checked" > Allow recipients to reshare </label>
                    <SwitchComponent id="checked" checked={true}></SwitchComponent>
                </div>
                <div className="setpasswordWrap">
                    <AccordionComponent>
                        <AccordionItemsDirective>
                            <AccordionItemDirective header={this.toggleEmailgating} content={this.dateAndTimeCont} />
                        </AccordionItemsDirective>
                    </AccordionComponent>
                </div>
                {/* <p className="warning-text" style={{ fontStyle: 'italic' }}>Validate work email</p> */}
                <RadioButtonComponent label="Validate Work Email" name="emailgating" cssClass="radio-btn email-gating" />

                <div className="setpasswordWrap">
                    <AccordionComponent>
                        <AccordionItemsDirective>
                            <AccordionItemDirective header={this.toggleSwitchPass} content={this.passwordCont} />
                        </AccordionItemsDirective>
                    </AccordionComponent>
                </div>
                <div className="setpasswordWrap">
                    <AccordionComponent>
                        <AccordionItemsDirective>
                            <AccordionItemDirective header={this.toggleSwitchTime} content={this.dateAndTimeCont} />
                        </AccordionItemsDirective>
                    </AccordionComponent>
                </div>
                <div className="setpasswordWrap">
                    <AccordionComponent>
                        <AccordionItemsDirective>
                            <AccordionItemDirective header={this.toggleSwitchNDA} content={this.dateAndTimeCont} />
                        </AccordionItemsDirective>
                    </AccordionComponent>
                </div>

            </div>
        );
    }
    //On clicking the manage link button, which will render the manage link panel by setting a state variable to true
    handleManagelink = () => {
        this.props.handleWideWidth()
        this.setState({ isManageLink: true })
    }
    //Render method for class components 
    render() {
        return (<React.Fragment>
            <div className="preview-container-wrap" style={{ float: "left", width: this.state.isGetLink || this.state.isGetAnonymousLink || this.state.isManageLink ? "50%" : "100%" }}>
                <div className="preview-container">
                    <p className="turnon-template-heading">Generate Link<br /></p>
                </div>
                <div className="personalize-link" data-testid="personalize-link">
                    <p className="title">Personalized link</p>
                    <p className="sub-title">Need a personalized link for specific prospect?</p>
                    <div className="text-box-link">
                        <TextBoxComponent floatLabelType="Never" data-testid="recipient-mail"
                            style={{ width: '100%' }} type="text" placeholder="Enter Recipient Email Here"
                            onChange={(e) => { this.setState({ recipientEmail: e.target.value }) }}
                            value={this.state.recipientEmail} />
                        <ButtonComponent cssClass='e-primary getlink-btn' data-testid="getlink-btn"
                            onClick={() => this.handleGetlink()}>Get Link</ButtonComponent>
                    </div>
                </div>

                <div className="anonymous-link">
                    <p className="title">Anonymous Link
                        <span className="link-title" onClick={() => this.getAnonymouslink()}>Get Link</span></p>
                    <p className="sub-title">Use this link for a quick share. User level tracking is not available for this link.</p>
                </div>

                <div className="newcampaign-link">
                    <p className="title">Executing a new campaign?</p>
                    <p className="sub-title">Enter name and/or more information to generate a UTM link to this asset for a campaign.</p>
                    <div className="text-box-link">
                        <TextBoxComponent floatLabelType="Never" data-testid="campaign-tag"
                            style={{ width: '100%' }} type="text" placeholder="Enter campaign tag Here"
                            onChange={(e) => { this.setState({ campaignTag: e.target.value }) }}
                            value={this.state.campaignTag} />
                        <ButtonComponent cssClass='e-primary getlink-btn' data-testid="getlink-btn2"
                            onClick={() => this.handleGetlink()}>Get Link</ButtonComponent>
                    </div>
                </div>
                <p className="manage-link" onClick={() => this.handleManagelink()} ><span><i className="fa fa-plus"></i></span>Manage Links</p>
            </div>
            {this.state.isManageLink ? <div className="_share-assets-wrap" style={{ float: "left", width: (this.state.isManageLink && this.state.isGetLink) ? "24%" : "49%" }}><ManageLinks onMangeLink={() => this.handleGetlink()} /></div> : null}
            {this.state.isGetLink ? <div className="_share-assets-wrap" style={{ float: "left", width: this.state.isManageLink ? "23%" : "49%", padding: "0", background: '#fff', borderLeft: '1px solid #dbdbdb' }}>
                <div className="title-wrap">
                    <p className="template-subheading pad1" style={{ textAlign: "left", padding: "0em 2em" }}>Here's your link
                        <span className="delete-link" onClick={() => this.onDeleteLink()}
                            style={{ float: "right", color: "#0ba2e3", cursor: "pointer" }}>Delete Link</span></p>
                    {this.state.createLink ?
                        <p className="template-subheading basicPtag link_item"
                            style={{ wordBreak: "break-all" }}>
                            http://localhost:3001/assets/6148813938ad163dd0f4f12d
                            <span onClick={() => this.onCopyLink()}
                                style={{ textAlign: "left", color: "#0ba2e3", cursor: "pointer" }} className="basicPtag copyText">
                                {this.state.copyTxt}</span>
                        </p> : null}
                    <AccordionComponent>
                        <AccordionItemsDirective>
                            <AccordionItemDirective header={"+ Advanced Setting"} content={this.acrdnContent2} />
                        </AccordionItemsDirective>
                    </AccordionComponent>
                </div>
            </div> : null}

            {this.state.isGetAnonymousLink ?
                <div className="_share-assets-wrap" style={{ float: "left", width: "49%", padding: "0", background: '#fff', borderLeft: '1px solid #dbdbdb' }}>
                    <div className="title-wrap">
                        <p className="template-subheading pad1" style={{ textAlign: "left", padding: "0em 2em" }}>Here's your link
                            <span className="delete-link" onClick={() => this.onDeleteLink()}
                                style={{ float: "right", color: "#0ba2e3", cursor: "pointer" }}>Delete Link</span></p>
                        {this.state.createAnonymousLink ?
                            <p className="template-subheading basicPtag link_item"
                                style={{ wordBreak: "break-all" }}>
                                http://localhost:3001/assets/6148813938ad163dd0f4f12d/a/6148813we343df3dd0
                                <span onClick={() => this.onCopyAnonymousLink()}
                                    style={{ color: "#0ba2e3", cursor: "pointer" }} className="basicPtag copyText">{this.state.copyTxt}</span>
                            </p> : null}
                        <br></br>
                        <AccordionComponent>
                            <AccordionItemsDirective>
                                <AccordionItemDirective header={"+ Advanced Setting"} content={this.acrdnContent2} />
                            </AccordionItemsDirective>
                        </AccordionComponent>
                    </div>
                </div> : null}
            {(this.state.isGetAnonymousLink || this.state.isGetLink) && (this.state.showButton) ?
                <div className="button-wrapper popup" style={{ border: 'none', justifyContent: 'flex-end' }}>
                    <div className="preview-btn confirm"><span className="round"><i className="fa fa-angle-right"></i></span>
                        Save Changes
                    </div>
                    <div className="preview-btn cancel" onClick={() => this.onDeleteLink()}>Cancel</div>
                </div>
                : null}
        </React.Fragment >);
    }
}

export default GenerateLink;