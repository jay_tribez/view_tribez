import React from 'react';
import { mount, shallow } from 'enzyme';
import CollectionItem from "./CollectionItem";
import toJson from 'enzyme-to-json';
import { BrowserRouter as Router } from 'react-router-dom';
import { createMemoryHistory } from 'history'

import { render, screen, fireEvent } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";

describe('<CollectionItem />', () => {

    it("on render component page", async () => {
        let wrapper = render(
            <MemoryRouter>
                <CollectionItem />
            </MemoryRouter>
        );
        screen.getByTestId('container')
    })
    it("on opening the collection item expect page change", async () => {
        let wrapper = render(
            <MemoryRouter>
                <CollectionItem />
            </MemoryRouter>
        );
        console.log('screeen', screen.getByTestId('card-item1'))
        fireEvent.click(screen.getByTestId('card-item1'))

    })
    it('on clicking the collection navlist to open the sidebar', async () => {
        let wrapper = render(
            <MemoryRouter>
                <CollectionItem />
            </MemoryRouter>
        );
        console.log('screeen', screen.getByTestId('collection-item1'))
        fireEvent.click(screen.getByTestId('collection-item1'))

    })


})

