import React, { useState } from 'react';


const BannerPosition = (props) => {
    const { handleClose } = props
    const [activeStep, setActiveStep] = useState('')
    const filterList = [{ name: 'none', icon: 'rot320' },
    { name: 'grey', icon: 'new' },
    { name: 'light-grey', icon: 'rot50' },
    { name: 'dotted', icon: 'rot270' },
    { name: 'line' },
    { name: 'cross', icon: 'rot90' },
    { name: 'up-white', icon: 'rot220' },
    { name: 'down-white', icon: 'rot180' },
    { name: 'down-white', icon: 'rot130' }
    ]
    const handleClick = (e, i) => {
        setActiveStep(i)
    }
    return (
        <div className="_banner-filter" style={{ left: '31%' }} data-testid="banner-pos-container">
            <div className="close-btn-wrap" onClick={handleClose} data-testid="close-bannner">
                X
            </div>
            {filterList.map((item, i) => (
                <div className="box-wrap-pos" key={i}
                    onClick={() => handleClick(item, i)}
                    data-testid={`banner-pos${i + 1}`}
                    style={{ background: activeStep === i ? 'rgb(229 16 88)' : 'rgb(229 16 88 / 20%)' }}>
                    {item.icon && <i className={`fas fa-arrow-up ${item.icon} iconw-wrap`}
                        style={{ opacity: activeStep === i && '1' }}></i>}
                    {item.img && <img src={item.img} alt="greyDot" width={20} height={20} />}
                </div>
            ))}
        </div>
    )
}
export default BannerPosition