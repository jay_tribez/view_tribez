import React from 'react';
import { mount, shallow } from 'enzyme';
import GenerateLink from "./generateLink";
import toJson from 'enzyme-to-json';

import { render, screen, fireEvent } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";

describe('<GenerateLink />', () => {
    it("on render component page", async () => {
        let wrapper = render(
            <MemoryRouter>
                <GenerateLink />
            </MemoryRouter>
        );
        screen.getByTestId('personalize-link')
    })
    it("on change the recipient mail", async () => {
        render(<GenerateLink />);
        console.log('screen', screen.getByTestId('recipient-mail'))
        fireEvent.change(screen.getByTestId('recipient-mail'), { target: { value: "" } })
        fireEvent.change(screen.getByTestId('recipient-mail'), { target: { value: "Tribez@123" } })
        fireEvent.click(screen.getByTestId('getlink-btn'))
    })
    it("on change the campaign tag", async () => {
        render(<GenerateLink />);
        console.log('screen', screen.getByTestId('campaign-tag'))
        fireEvent.change(screen.getByTestId('campaign-tag'), { target: { value: "" } })
        fireEvent.change(screen.getByTestId('campaign-tag'), { target: { value: "Tribez@123" } })
        fireEvent.click(screen.getByTestId('getlink-btn2'))
    })
})

