import React from 'react';
import { mount, shallow } from 'enzyme';
import BannerPosition from "./BannerPosition";
import toJson from 'enzyme-to-json';

import { render, screen, fireEvent } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";

describe('<BannerPosition />', () => {

    it("on render component page", async () => {
        let wrapper = render(
            <MemoryRouter>
                <BannerPosition />
            </MemoryRouter>
        );
        screen.getByTestId('banner-pos-container')
    })
    it("on clicking close button banner position component", async () => {
        let wrapper = render(
            <MemoryRouter>
                <BannerPosition />
            </MemoryRouter>
        );
        fireEvent.click(screen.getByTestId('close-bannner'))
    })
    // let wrapper;
    // beforeEach(() => {
    //     wrapper = shallow(<BannerPosition />);
    // });

    // it('should render correctly', async () => {
    //     expect(toJson(wrapper)).toMatchSnapshot();
    // })
    // it('check handleClick', () => {
    //     let btn = wrapper.find('.box-wrap-pos').at(0)
    //     btn.simulate('click', { item: 'grey', i: 1 })
    //     wrapper.update()
    // })

})

