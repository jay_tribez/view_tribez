import React, { Component } from 'react';
import './previewNavItem.scss';

class LiveCollection extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }
    closeClick = () => {

    }
    render() {
        return (<React.Fragment>
            <div className="preview-container-wrap">
                <div className="preview-container">
                    <p className="turnon-template-heading" data-testid="turnon-heading">turn ON<br />live collection</p>
                    <p className="template-subheading">Turning this feature ON will keep this collection live and in sync<br />
                        with all updates even after you've shared with recipients.</p>
                    <p className="template-subheading" style={{marginTop:20}}>This collection was created on 23 Jul 2021. This collection wasn't shared with anyone.</p>
                </div>
               
            </div>
        </React.Fragment>);
    }
}

export default LiveCollection;