import React, { Component } from 'react';
import CropAssetImage from "../assetPreview/cropAssetImage";
// import { TextBoxComponent } from '@syncfusion/ej2-react-inputs';
import { SwitchComponent } from '@syncfusion/ej2-react-buttons';
import collectionBg from '../../Assets/images/_collectionItem_headerbg.jpeg';

import { DialogComponent } from '@syncfusion/ej2-react-popups';
import BannerList from './BannerList';
import ReactCrop from 'react-image-crop';
import BannerFilter from './BannerFilter';
import BannerPosition from './BannerPosition';
import "../../theme.scss"

import './previewNavItem.scss';
import "../CollectionItem/CollectionItem.scss"

//Defining as Class component for banner edit functionality
class StreamBannerEdit extends Component {
    // Constructor method -->The constructor is a method used to initialize an object's state in a class.
    // It automatically called during the creation of an object in a class.
    constructor(props) {
        super(props);
        // Initializing state variables as an object because of class component
        this.state = {
            searchUser: '',
            users: [
                { "name": "Admin", },
                { "name": "Karthik Kumar" }
            ],
            bannerNavList: [
                { name: 'ADD BANNER', icon: 'far fa-image' },
                { name: 'CROP', icon: 'fas fa-crop-alt' },
                { name: 'POSITION', icon: 'fas fa-columns' },
            ],
            isBannerList: false,
            isBannerCrop: false,
            mainBannerImg: collectionBg,
            crop: {
                unit: '%',
                width: 50,
                height: 50,
            },
            isBannerFilter: false,
            isBannerPosition: false,
            showCrop: false,
            cropImge: "",
            uploadImg: false
        }

    }
    //On clicking the 
    openClick = (item, type) => {
        switch (type) {
            case 1:
                return (this.setState({ isBannerList: true }))
            case 2:
                return (this.setState({ isBannerCrop: true }))
            case 3:
                return (this.setState({ isBannerFilter: true }))
            case 4:
                return (this.setState({ isBannerPosition: true }))
            default:
                break;
        }
    }

    cropImageResponse = (img) => {
        this.props.cropImageResponse(img)
        this.setState({ showCrop: false })
    }
    header(val) {
        return (
            <>
                {!this.state.isBannerList ?
                    <div className="_collectionItem_navList" style={{ width: '70%' }} data-testid="banner-header">
                        {val.map((item, index) => (
                            <>
                                <div className="dFlexCenter">
                                    {item.icon && <i style={{ color: '$primaryColor' }} className={`${item.icon}  nav-list-icon`}></i>}
                                    {item.toggle && <SwitchComponent id="checked" checked={true}></SwitchComponent>}
                                    <p style={{ color: '$primaryColor', fontSize: '12px', marginLeft: '5px' }}
                                        onClick={() => this.openClick(item, index + 1)}
                                    >{item.name}</p>
                                </div>
                            </>
                        ))}
                    </div> :
                    <div className="_collectionItem_navList" style={{ width: '70%' }}>
                        <p style={{ color: 'inherit', cursor: 'pointer' }} onClick={this.closeBannerList}>
                            <i class="fas fa-chevron-left"
                                style={{ marginRight: '10px', color: 'inherit', cursor: 'pointer' }}></i>
                            Customize your banner
                        </p>
                    </div>
                }
            </>
        );
    }
    handleBanner = (item) => {
        this.setState({ isBannerList: false, mainBannerImg: item }, () => {
            setTimeout(() => {
                // this.props.cropImageResponse(item)
                this.setState({ showCrop: true })
            }, 1000);
        })
    }
    closeBannerList = () => {
        this.setState({ isBannerList: false })

    }
    handleClose = () => {
        this.setState({ isBannerFilter: false, isBannerPosition: false })

    }
    footerTemplate() {
        return (<div></div>);
    }

    content() {
        return (
            <>{this.state.isBannerList ?
                <BannerList handleBanner={this.handleBanner} /> :
                <div>{this.state.showCrop ?
                    <CropAssetImage isHeaderNeed={false} image={this.state.mainBannerImg} submitBtnTxt={"Upload"} cropImageResponse={this.cropImageResponse} />
                    : null}
                </div>}
                {this.state.isBannerFilter && <BannerFilter handleClose={this.handleClose} />}
                {this.state.isBannerPosition && <BannerPosition handleClose={this.handleClose} />}
            </>
        );
    }
    render() {
        const { openDialog, closeBanner } = this.props
        const { bannerNavList } = this.state
        return (
            <React.Fragment>
                {openDialog &&
                    <div className="_edit-banner-backdrop" data-testid="banner-assetedit">
                        <DialogComponent
                            header={() => this.header(bannerNavList)}
                            footerTemplate={this.footerTemplate}
                            content={() => this.content()}
                            showCloseIcon={true}
                            ref={dialog => this.dialogInstance = dialog}
                            // width={'437px'}
                            // open={this.dialogOpen}
                            close={closeBanner}
                            visible={true}
                            className={'dialog-wrap stream-banner-wrap'}
                        >
                        </DialogComponent>
                    </div>}
            </React.Fragment>
        );
    }
}

export default StreamBannerEdit;