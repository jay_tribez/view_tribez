import React, { Component } from 'react';
import './previewNavItem.scss';
import { TextBoxComponent } from '@syncfusion/ej2-react-inputs';
import { RadioButtonComponent } from '@syncfusion/ej2-react-buttons';
import userImage from '../../Assets/images/user-img.svg';

class Collabarator extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchUser: '',
            users: [
                { "name": "Admin", },
                { "name": "Karthik Kumar" }
            ]
        }
    }

    render() {
        return (<React.Fragment>
            <div className="preview-container-wrap">
                <div className="preview-container">
                    <p className="turnon-template-heading">collaborate</p>
                    <p className="template-subheading" style={{ marginTop: 0 }}>collaborate within your team, decide who can access this collection
                    </p>

                    <div className="radio-btns-wrapper">
                        <div id="collaborator-tab" className="radio-group">
                            <RadioButtonComponent label="Collaborators" cssClass="radio-btn" name="Collaborators" />
                            <RadioButtonComponent label="View All" name="Collaborators" cssClass="radio-btn" />
                            <RadioButtonComponent label="Edit All" name="Collaborators" cssClass="radio-btn" />
                        </div>
                    </div>
                    <hr className="hr-tag position" style={{ margin: 0 }}></hr>
                    <TextBoxComponent floatLabelType="Never"
                        style={{ width: '100%' }} type="text" placeholder="Search Users"
                        onChange={(e) => { this.setState({ searchUser: e.target.value }) }}
                        value={this.state.searchUser} />

                    <div className="users-lists-wrap">
                        {this.state.users.map((item, index) => {
                            return (<React.Fragment>
                                <div className="usersList-wrapper">
                                    <div style={{ display: 'flex', alignItems: 'center', marginBottom: '10px' }}> <img className="user-img" style={{ width: 20, marginRight: 10 }} src={userImage}></img><p className="users-username">{item.name}</p></div>
                                    <div style={{ marginTop: '-10px' }}><RadioButtonComponent label="Read" cssClass="e-small radio-btn" name={`readOption ${index}`} />
                                        <RadioButtonComponent label="Write" name={`readOption ${index}`} cssClass="e-small radio-btn" />
                                    </div>
                                </div>
                            </React.Fragment>)
                        })}
                    </div>

                </div>
            </div>
        </React.Fragment>);
    }
}

export default Collabarator;