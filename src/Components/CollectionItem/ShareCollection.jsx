import React, { Component } from 'react';
import { TextBoxComponent } from '@syncfusion/ej2-react-inputs';
import { AccordionComponent, AccordionItemDirective, AccordionItemsDirective } from '@syncfusion/ej2-react-navigations';
import { ButtonComponent, SwitchComponent } from '@syncfusion/ej2-react-buttons';
import { RadioButtonComponent } from '@syncfusion/ej2-react-buttons';
import { DateTimePickerComponent } from '@syncfusion/ej2-react-calendars';
import assetsData from "../../Assets/Data/assets.json";
import searchUser from "../../Assets/images/searchUser.svg"

class ShareCollection extends Component {
    constructor(props) {
        super(props);
        this.state = {
            recipientEmail: '',
            mailSubject: '',
            streamData: [{
                name: "Distributed Architecture Wh…",
                img: "https://d2uav5q06z9nv6.cloudfront.net/60fab5b857b3bb204aba0af2/images/60fac53a94ce93536b32d72d/007989fe-abe6-4fe3-8aea-851515f37a87",
                updateAt: new Date(),
                completed: 25
            }, {
                name: "Introduction to Operating Syste…",
                img: "https://d2uav5q06z9nv6.cloudfront.net/60fab5b857b3bb204aba0af2/images/60fac53a94ce93536b32d72d/007989fe-abe6-4fe3-8aea-851515f37a87",
                updateAt: new Date(),
                completed: 0
            }],
            isTemplate: false
        }
    }
    // handleSetPassword = (e) => {
    //     let value = e.changedProperties.checked
    // }
    toggleSwitchPass = () => {
        return (
            <div className="toggle">
                <label htmlFor="checked" > Set a password </label>
                <SwitchComponent id="checked" checked={false}></SwitchComponent>
            </div>)
    }
    toggleSwitchTime = () => {
        return (
            <div className="toggle">
                <label htmlFor="checked" > Set an expiry date & time </label>
                <SwitchComponent id="checked" checked={false}></SwitchComponent>
            </div>)
    }
    passwordCont = () => {
        return (
            <div className="ad-input">
                <TextBoxComponent floatLabelType="Never"
                    style={{ width: '100%' }} type="password" placeholder="Enter Password"
                    onChange={(e) => { this.setState({ mailSubject: e.target.value }) }}
                    value={this.state.mailSubject} />
            </div>)
    }
    dateAndTimeCont = () => {
        return (
            <div className="ad-input">
                <DateTimePickerComponent></DateTimePickerComponent>
            </div>
        )
    }
    acrdnContent2 = () => {
        return (
            <div className="_share_advanced_settings">
                <div className="title">
                    <label htmlFor="checked" > Allow recipients to download </label>
                    <SwitchComponent id="checked" checked={true}></SwitchComponent>
                </div>
                <div className="edit-block">
                    <div id="collaborator-tab" className="_share-radio-group">
                        <RadioButtonComponent label="Editable" name="Collaborators" cssClass="radio-btn" />
                        <RadioButtonComponent label="Read Only" name="Collaborators" cssClass="radio-btn" />
                    </div>
                    <p className="warning-text">*This applies to PPTs, Docs, Google Slides and Google Docs.</p>
                </div>
                <div className="title">
                    <label htmlFor="checked" > Allow recipients to reshare </label>
                    <SwitchComponent id="checked" checked={true}></SwitchComponent>
                </div>
                <div className="setpasswordWrap">
                    <AccordionComponent>
                        <AccordionItemsDirective>
                            <AccordionItemDirective header={this.toggleSwitchPass} content={this.passwordCont} />
                        </AccordionItemsDirective>
                    </AccordionComponent>
                </div>
                <div className="setpasswordWrap">
                    <AccordionComponent>
                        <AccordionItemsDirective>
                            <AccordionItemDirective header={this.toggleSwitchTime} content={this.dateAndTimeCont} />
                        </AccordionItemsDirective>
                    </AccordionComponent>
                </div>
            </div>
        );
    }
    handleTemplateOpen = () => {
        this.props.handleWideWidth()
        this.setState({
            isTemplate: true
        })
    }
    content = () => {
        return (
            <p>test</p>)
    }
    handleCloseTemplate = () => {
        this.props.handleCloseTemplate()
        this.setState({ isTemplate: false })

    }
    createTemplate = () => {
        console.log('template created')
    }
    render() {
        const { isTemplate } = this.state
        return (
            <div className="_share_dflex">
                <div className="preview-share-wrap">
                    <div className="preview-container">
                        <p className="turnon-template-heading">compose<br /></p>
                        <p className="template-subheading" style={{ marginTop: 0 }}>quick send
                        </p>
                    </div>
                    <div className="_share-inputs-wrap">
                        <TextBoxComponent floatLabelType="Never" data-testid="recipient-mail"
                            style={{ width: '100%' }} type="text" placeholder="To"
                            onChange={(e) => { this.setState({ recipientEmail: e.target.value }) }}
                            value={this.state.recipientEmail} />
                        <TextBoxComponent floatLabelType="Never" data-testid="mailsubject"
                            style={{ width: '100%', paddingTop: '1em' }} type="text" placeholder="Subject"
                            onChange={(e) => { this.setState({ mailSubject: e.target.value }) }}
                            value={this.state.mailSubject} />
                    </div>
                    {!this.props.hideTools &&
                        <>  <div className="_share-template-select" >
                            <p onClick={this.handleTemplateOpen} data-testid="select-template" >SELECT TEMPLATE</p>
                            <p>INSERT WIDGET</p>
                        </div>
                            <div className="_share-body-content">
                                <div className="side-panel-desc" dangerouslySetInnerHTML={{ __html: assetsData[0].body }}></div>
                            </div>
                        </>
                    }
                    {this.props.showViewContent &&
                        <div className="_share-body-content sectionContent" >
                            <div className="side-panel-desc" >
                                <p> Hello there,</p>

                                <p style={{ marginTop: 10 }}>Here's a collection of assets I'd like to share with you. Hope you find this material<br /> useful.
                                </p>
                                <br />
                                <ButtonComponent cssClass='e-primary email-temp'
                                    data-testid="create-template-btn view-content" onClick={this.viewContent}>
                                    View Content</ButtonComponent>
                                <p style={{ marginTop: 10 }}>Thank You</p>
                            </div>
                        </div>
                    }
                </div>
                {isTemplate &&
                    <div className="_share-template-Selection">
                        <i onClick={this.handleCloseTemplate}
                            className="fas fa-times-circle close-icon-cross"></i>
                        <TextBoxComponent floatLabelType="Never" data-testid="search-input"
                            type="text" placeholder="Type here for search"
                            value={''} />
                        <div className="no-content-block">
                            <img src={searchUser} className="no-template-img" alt="search" />
                            <div className="content-block">
                                <p className="talk-less">Type less, Share more!</p>
                                <p className="desc">Create reusable email templates
                                    for you and your teams.</p>
                                <ButtonComponent cssClass='e-primary email-temp' data-testid="create-template-btn" onClick={this.createTemplate}>
                                    Create new email <br /> template</ButtonComponent>

                            </div>
                        </div>
                    </div>
                }
                <div className="_share-assets-wrap">
                    {!this.props.hideDefault ?
                        <div className="title-wrap">
                            <p className="template-subheading pad1">(sample)Start your Tribez journey here!</p>
                            <p className="template-subheading basicPtag">{this.state.streamData.length} Assets</p>
                            <AccordionComponent>
                                <AccordionItemsDirective>
                                    <AccordionItemDirective header={"+ Advanced Setting"} content={this.acrdnContent2} />
                                </AccordionItemsDirective>
                            </AccordionComponent>

                            <div className="cards-wrap">
                                {this.state.streamData.map((item, index) => (
                                    <div className="e-card  mb1" key={index}>
                                        <div className="e-card-image"
                                            style={{
                                                background: `url(${item.img})`,
                                                backgroundSize: "cover",
                                                backgroundRepeat: 'no-repeat'
                                            }}>
                                        </div>
                                        <div className="e-card-content cardContBg">
                                            <p className="e-card-name">{item.name}</p>
                                        </div>
                                    </div>
                                ))}
                            </div>
                        </div> :
                        <div className="title-wrap">
                            <p className="template-subheading pad1">{this.props.sectionData.sectionName}</p>
                            <div className="cards-wrap">
                                {this.props.sectionData.data.map((item, index) => {
                                    if (index === 0) {
                                        return (<div className="e-card width45 mb1" key={index}>
                                            <div className="e-card-image"
                                                style={{
                                                    background: `url(${item.imageUrl})`,
                                                    backgroundSize: "cover",
                                                    backgroundRepeat: 'no-repeat'
                                                }}>
                                            </div>
                                            <div className="e-card-content cardContBg">
                                                <p className="e-card-name">{this.props.sectionData.sectionCount} assets</p>
                                            </div>
                                        </div>
                                        )
                                    }
                                })}
                            </div>
                        </div>}
                </div>
            </div>);
    }
}

export default ShareCollection;