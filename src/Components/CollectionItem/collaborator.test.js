import React from 'react';
import { mount, shallow } from 'enzyme';
import Collabarator from "./collaborator";
import toJson from 'enzyme-to-json';
import { TextBoxComponent } from '@syncfusion/ej2-react-inputs';

describe('<Collabarator />', () => {
    let wrapper;
    beforeEach(() => {
        wrapper = shallow(<Collabarator />);
    });

    it('should render correctly', async () => {
        expect(toJson(wrapper)).toMatchSnapshot();
    })
    it('textboxComponent handle', () => {
        let btn = wrapper.find(TextBoxComponent)
        btn.simulate('change', { target: { value: 'userName' } })
        wrapper.update()
        expect(wrapper.state('searchUser')).toBe('userName')
    })

})

