import React, { useState, useRef } from 'react';
import { withRouter } from 'react-router';
import "./CollectionItem.scss";
import "../hub/index.scss";
import collectionData from "../../Assets/Data/collectionItem.json"
import { useHistory } from 'react-router-dom';
import SidePreviewPanel from "../sidePreviewPanel";
import TurnOffTemplate from './turnTemplate';
import LiveCollection from './liveCollection';
import Collaborator from './collaborator';
import GenerateLinkPanel from './generateLink';
import ShareCollection from './ShareCollection';
import AddAssetCollection from './AddAssetCollection';
import DefaultProfilePic from '../../Assets/images/profile.jpg';

import { RichTextEditorComponent, Toolbar, FileManager, Inject, Image, Link, HtmlEditor, Count, QuickToolbar, Table } from '@syncfusion/ej2-react-richtexteditor';
// import { ToolbarSettingsModel, FileManager, FileManagerSettingsModel, QuickToolbarSettingsModel } from '@syncfusion/ej2-react-richtexteditor';
// import { ButtonComponent } from '@syncfusion/ej2-react-buttons';
import { UploaderComponent } from '@syncfusion/ej2-react-inputs';
import { TabComponent } from '@syncfusion/ej2-react-navigations';
import AddAssetSection from './AddAssetSection';
import Compose from '../Compose';
import BannerAssetEdit from './BannerAssetEdit';
import axios from 'axios';
import landingVideo from "../../Assets/videos/Fishing_Final.mp4"

import StreamBannerEdit from './StreamBannerEdit';

const CollectionItem = (props) => {
    // Declaring State variables using useState because of functional components.
    // Since there is no api available for now, some of the mock data added in the state for ui render
    const [collectionItemData, setCollectionItemDat] = useState(props.location.collectionData)
    let rteObj = RichTextEditorComponent;
    const name = collectionItemData.name,
        recordsTotal = collectionItemData.assetsCount,
        collections = collectionItemData.assets,
        sections = collectionItemData.sections,
        userName = collectionItemData.createdBy.firstName + " " + collectionItemData.createdBy.lastName,
        userEmail = collectionItemData.createdBy.email;
    // const [sideBar, setSideBar] = useState(false)
    const [data, setData] = useState({})
    // const [sidecontent, setSidecontent] = useState(false)
    const [showTemplatePanel, setShowtemplatepanel] = useState(false)
    const [env, setEnv] = useState(JSON.parse(localStorage.getItem("env")))
    const [isWideShareCollection, setIsWideShareCollection] = useState(false)
    const [showGenerateLinkPanel, setShowgeneratelinkpanel] = useState(false)
    const [showLiveCollectionPanel, setShowlivecollectionpanel] = useState(false)
    const [showCollaboratePanel, setShowcollaboratepanel] = useState(false)
    const [showShareCollection, setShowShareCollection] = useState(false)
    const [showAddAssetCollection, setshowAddAssetCollection] = useState(false)
    const [showAddSection, setshowAddSection] = useState(false)
    const [showEditBanner, setShowEditBanner] = useState(false)
    const [isEditor, setIsEditor] = useState(false)
    const [streamBackground, setStreamBackground] = useState("")
    const tabRef = useRef(null)
    const [deletedSections, setDeletedsection] = useState([])
    const [removeAssets, setRemoveAssets] = useState([])
    const [recipientEmail, setRecipientEmail] = useState({})
    //Use history method for routing 
    const history = useHistory()
    //Nav item 
    const navList = [
        { name: "TEMPLATE(OFF)", icon: "https://app.paperflite.com/public/f1015a70d0c18d23dc0403f88dd07e36.svg" },
        { name: "LIVE COLLECTION(OFF)", icon: "https://app.paperflite.com/public/f1015a70d0c18d23dc0403f88dd07e36.svg" },
        { name: "ADD ASSET", iconClass: "fa fa-plus" },
        { name: "EDIT COLLECTION", iconClass: "fa fa-edit" },
        { name: "COLLABORATE", iconClass: "fa fa-users" },
        { name: "GENERATE LINK", iconClass: "fa fa-link" }]
    // Edit Collection Navigation Lists
    const navListEditor = [
        { name: "+ADD LOGO" },
        { name: "+ADD SECTION" },
        { name: "*ADD ASSET" },
        { name: "EDIT BANNER" }]
    // Handle PDF link which will redirect to specific collection item link
    const handlePdfClick = (item) => {
        history.push({ pathname: "/app/collectionItem", state: { detail: item } })
    }
    //open the sidebar panel 
    const openClick = (item, type) => {
        // this.sidebarobj.show();
        if (type != 4) {
            let body = document.getElementsByTagName('body')
            body[0].className = body[0].className + ' sidecontentnav';
        }
        console.log(type)
        switch (type) {
            case 1:
                setShowtemplatepanel(true)
                break;
            case 2:
                setShowlivecollectionpanel(true)
                break;
            case 3:
                setshowAddAssetCollection(true)
                setIsEditor(true)
                break;
            case 4:
                // setshowAddAssetCollection(true)
                if (collections.length > 0) {
                    collections.map((item, i) => {
                        collections[i]['edit'] = true
                    })
                }
                if (sections.length > 0) {
                    sections.map((item, i) => {
                        sections[i]['edit'] = true
                    })
                }
                setIsEditor(true)
                break;
            case 5:
                setShowcollaboratepanel(true)
                break;
            case 6:
                setShowgeneratelinkpanel(true)
                break;

            default:
                // this.setState({ sidecontent: true })
                break;
        }

    }
    //Add collection item will render based on switch case for these tools
    //for ADD Section and ADD Logo and Edit Banner  and Add Asset Collection item
    const openClickAsset = (item, type) => {
        if (type != 1 && type != 4) {
            let body = document.getElementsByTagName('body')
            body[0].className = body[0].className + ' sidecontentnav';
        }
        switch (type) {
            case 1:
                <div id='dropArea' className='dropArea' >
                    {/* ref={this.dropRef}> */}
                    <UploaderComponent id='fileUpload' type='file' ref={(scope) => { this.uploadObj = scope; }}                        >
                    </UploaderComponent>
                </div>
                break;
            case 2:
                setshowAddSection(true)
                break;
            case 3:
                setshowAddAssetCollection(true)
                setIsEditor(true)
                break;
            case 4:
                setShowEditBanner(true)
                setIsEditor(true)
                break;

            default:
                // this.setState({ sidecontent: true })
                break;
        }

    }

    // share collection panel will render by adding a class to the DOM
    // set a showsharecollecton variable to true
    const handleShareCollection = () => {
        let body = document.getElementsByTagName('body')
        body[0].className = body[0].className + ' sidecontentnav';
        setShowShareCollection(true)
    }
    //close the sidebar panel by removing a panel classname and panel Boolean value to false
    const closeClick = () => {
        let body = document.getElementsByTagName('body')
        body[0].className = body[0].className.replace(' sidecontentnav')
        setShowtemplatepanel(false)
        setShowlivecollectionpanel(false)
        setShowcollaboratepanel(false)
        setShowgeneratelinkpanel(false)
        setShowShareCollection(false)
        setshowAddAssetCollection(false)
        setshowAddSection(false)
    }
    //Close the Edit Banner Panel by removing a class from the DOM
    const closeBanner = () => {
        let body = document.getElementsByTagName('body')
        body[0].className = body[0].className.replace(' sidecontentnav')
        setShowEditBanner(false)
    }

    const onUpdate = () => {

    }
    const onConfirm = () => {

    }
    //Handle Wide width function for rendering the side panel for more width 
    const handleWideWidth = () => {
        setIsWideShareCollection(true)
    }
    //Handle close template which close the panel by reducing width by a state variable
    const handleCloseTemplate = () => {
        setIsWideShareCollection(false)
    }
    const handleDeleteClick = (item) => {

    }
    const closeAddAssetClick = () => {
        let body = document.getElementsByTagName('body')
        body[0].className = body[0].className.replace(' sidecontentnav')
        setshowAddAssetCollection(false)
        setShowShareCollection(false)
        setIsWideShareCollection(false)
    }
    // Content method which will render based on the state variable for multiple collection tool items
    // For Turn Off Template, Live Collection, Add Collaborator , Generating Link , Share Collection, Add Asset collection 

    const content = () => {
        return (
            showTemplatePanel ? <TurnOffTemplate data={data} /> :
                showLiveCollectionPanel ? <LiveCollection data={data} /> :
                    showCollaboratePanel ? <Collaborator data={data} /> :
                        showGenerateLinkPanel ? <GenerateLinkPanel handleWideWidth={() => handleWideWidth()} /> :
                            showShareCollection ?
                                <Compose collectionItemData={collectionItemData} handleWideWidth={() => handleWideWidth()} onRecipientEmail={onRecipientEmail} closeClick={() => closeAddAssetClick()} handleCloseTemplate={() => handleCloseTemplate()} data-testid="compose"></Compose> :
                                // <ShareCollection handleWideWidth={handleWideWidth} handleCloseTemplate={handleCloseTemplate} /> :
                                showAddAssetCollection ? <AddAssetCollection closeClick={closeAddAssetClick} /> : showAddSection ? <AddAssetSection /> : '')
    }
    const onRecipientEmail=(email)=>{
        setRecipientEmail(email)
    }
    const cropImageResponse = (img) => {
        console.log('img', img)
        setStreamBackground(img)
        setShowEditBanner(false)
    }
    const onRemoveAsset = (e, item) => {
        e.preventDefault()
        let assets = removeAssets
        assets.push(item.id)
    }
    const tabContent = (item, i) => {
        return (<React.Fragment key={i}>
            <div className="e-card" key={'card' + i} data-testid={`card-item${i + 1}`} style={{ float: 'left', padding: '0.7em', width: '250px', marginRight: '20px', cursor: 'pointer' }}
                onClick={() => handlePdfClick(item)}>
                <div className="e-card-image" style={{
                    background: `url(${item.icon != null ? item.icon.thumbnail : null})`,
                    backgroundSize: "cover", backgroundRepeat: 'no-repeat', height: '56vh'
                }}>
                    {item.edit ? <div onClick={(e) => { onRemoveAsset(e, item) }}>
                        <i className="fas fa-times-circle close-icon-cross remove-click"></i>
                    </div> : null}
                </div>
                <div className="e-card-title title"
                    style={{
                        fontWeight: '400',
                        fontSize: '.875em',
                        color: '#212529',
                        textAlign: 'left',
                        paddingLeft: '0px',
                        paddingBottom: '0px'
                    }}
                >{item.name} </div>
                <div className="dflex" style={{ display: 'flex', justifyContent: 'flex-start' }}>
                    <p className="__collectionItem_cards_type">{item.type}</p>
                    <p className="__collectionItem_cards_size">{item.size}</p>
                </div>
            </div>

        </React.Fragment>)
    }
    const onGoBack = () => {
        if (collections.length > 0) {
            collections.map((item, i) => {
                collections[i]['edit'] = false
            })
        }
        if (sections.length > 0) {
            sections.map((item, i) => {
                sections[i]['edit'] = false
            })
        }
        setIsEditor(false)
    }
    const onRemoveTab = () => {
        console.log(tabRef)
        let deletedIndex = tabRef.current.selectedID
        let deletedSections = []
        collectionItemData.sections.map((item, idx) => {
            if (idx == deletedIndex) {
                deletedSections.push(item.id)
            }
        })
        setDeletedsection(deletedSections)
    }
    const onSectionAssetsContent = () => {
        return <div className="_collectionItem_cardsWrap">
            {collections.length == 0 && sections.length == 0 ? "Fetching" : null}
            {collections && sections.length == 0 ?
                <div className="_collectionItem_card_container"
                    style={{ display: 'flex', marginTop: '-40px', paddingLeft: '1em', }} >
                    {collections.map((item, i) => {
                        return tabContent(item, i)
                    })}
                </div> : null}
            {sections.length > 0 ? <div className="section-tab-container">
                <div className="_collectionItem-tabs-wrapper">
                    <TabComponent id='defaultTab' ref={tabRef} removing={onRemoveTab} showCloseButton={isEditor}>
                        <div className="e-tab-header">
                            {sections.map((item, index) => {
                                return (
                                    <div key={index} style={{ padding: 10 }}>{item.name}
                                        {/* {item.edit ? <i className="fas fa-times-circle close-icon-cross remove-click" onClick={(e) => onRemoveAsset(e, item)}></i>
                                            : null} */}
                                    </div>
                                )
                            })}
                        </div>
                        <div className="tabcontent e-content">
                            {sections.map((item) => {
                                return (item.assets.length > 0 ? item.assets.map((asset, i) => {
                                    return tabContent(asset, i)
                                }) : null)
                            })}
                        </div>
                    </TabComponent>
                </div>
            </div> : null}
        </div>
    }
    const onEditCollection = () => {

        let payload = {
            "banner": streamBackground,
            "sections": sections,
            "deletedSections": [...deletedSections, ...removeAssets],
            "id": collectionItemData.id,
            "notifyRecipients": false
        }
        axios.put(`${env['apiUrl']}/collections/${collectionItemData.id}`, payload)
            .then(res => {
                console.log('response', res)
            }).catch(e => {
            })
    }
    const sendMail = () => {
        console.log(recipientEmail)
        // closeAddAssetClick()
        let payload = {

        }
        // axios.post(`${env['apiUrl']}/collections/${collectionItemData.id}`, payload)
        //     .then(res => {
        //         console.log('response', res)
        //     }).catch(e => {
        //     })
    }
    return (
        <div className="_collectionItem_container _app-mainbackground" data-testid="container">
            <div className="_collectionItem_img_container" style={{ backgroundImage: streamBackground.length > 0 ? `url(${streamBackground})` : `url(${"https://app.paperflite.com/public/f6833a7f2490565f46333c6c948b6eab.jpeg"})` }}>
                {/* <video type="video/mp4"
                    muted
                    autoplay
                    playsinline loop
                    preload="metadata"
                    id="video-url"

                >
                    <source
                        data-original='https://d2uav5q06z9nv6.cloudfront.net/public/1_banner.mp4'
                        src='https://d2uav5q06z9nv6.cloudfront.net/public/1_banner.mp4'
                        type="video/mp4" />
                </video> */}
                <div className="_collectionItem_texture">
                    {!isEditor ?
                        <>
                            <div className="_collectionItem_header">
                                <div className="_collectionItem_navPanel">
                                    <div className="_collectionItem_back" onClick={() => { props.history.goBack() }} >
                                        <i className="fa fa-chevron-left" style={{ fontSize: '12px', paddingRight: '5px' }}></i> {`BACK`}
                                    </div>
                                    <div className="_collectionItem_navList">
                                        {navList.map((item, index) => (<React.Fragment key={index}>
                                            {item.icon && <img src={item.icon} className="navlist-icon urlimage" alt=""></img>}
                                            {item.iconClass && <span><i className={`${item.iconClass} nav-list-icon`}></i></span>}
                                            <p onClick={() => openClick(item, index + 1)} data-testid={`collection-item${index + 1}`}>{item.name}</p>
                                        </React.Fragment>))}
                                    </div>
                                </div>

                            </div>
                            <div className="_collectionItem_contentBlock">
                                <div className="dFlexSB">
                                    <p className="title">{name}</p>
                                    <div className="share-collection" onClick={handleShareCollection}>
                                        <span>Share <br />Collection</span>
                                        <span className="shareIconWrap">
                                            <i class="fas fa-share-alt"></i></span>
                                    </div>
                                </div>
                                <div className="dFlexSB">
                                    <p className="desc">{`Click here for your personal copy of ${recordsTotal} documents with reference to our discussion. Hope you will find them useful.`}</p>
                                    <div className="sharedby-wrap collectionItem">
                                        <p className="shredBy">Shared By</p>
                                        <p className="f13">{userName}</p>
                                        <p className="f13">{userEmail}</p>
                                        <img src={DefaultProfilePic} alt="default profile" className="default-profile-picture"></img>
                                    </div>
                                </div>
                            </div>
                        </> :
                        <>
                            <div className="_collectionItem_navList editor-nav">
                                {navListEditor.map((item, index) => (<>
                                    {item.icon && <img src={item.icon} className="navlist-icon urlimage" alt=""></img>}
                                    {item.iconClass && <span><i className={`${item.iconClass} nav-list-icon`}></i></span>}
                                    <p onClick={() => openClickAsset(item, index + 1)}>{item.name}</p>
                                </>))}
                            </div>
                            <RichTextEditorComponent id="toolsRTE"
                                ref={(richtexteditor) => { rteObj = richtexteditor }}
                                showCharCount={true}
                            // actionBegin={this.handleFullScreen.bind(this)}
                            // actionComplete={this.actionCompleteHandler.bind(this)} maxLength={2000} 
                            // toolbarSettings={this.toolbarSettings}
                            // fileManagerSettings={this.fileManagerSettings} 
                            // quickToolbarSettings={this.quickToolbarSettings}
                            >
                                <div className="_collectionItem_contentBlock">
                                    <div className="dFlexSB">
                                        <p className="title">{name}</p>
                                    </div>
                                    <div className="dFlexSB"    >
                                        <p className="desc">{`Click here for your personal copy of ${recordsTotal} documents with reference to our discussion. Hope you will find them useful.`}</p>
                                    </div>
                                </div>
                                <Inject
                                    services={[Toolbar, Image, Link, HtmlEditor, Count, QuickToolbar, Table, FileManager]} />
                            </RichTextEditorComponent>
                            <p className="cancel-btn" style={{ left: "75px" }} onClick={() => onGoBack()}>Cancel</p>
                            <p className="cancel-btn" style={{ left: "30px" }} onClick={() => onEditCollection()}>Save</p>
                            {onSectionAssetsContent()}
                        </>

                    }
                </div>
            </div>
            {!isEditor && onSectionAssetsContent()}

            <div className={`templatepreview ${showCollaboratePanel ? 'collaborate-wrap' :
                (showGenerateLinkPanel ? 'generate-link-wrap' :
                    showShareCollection ? (isWideShareCollection ? 'share-collection-wrap-wide' : 'compose-collection-panel share-collection-wrap') :
                        showAddAssetCollection ? 'addAsset-collection-wrap' : '')}`}>
                {showTemplatePanel ? <SidePreviewPanel content={content()} action={true} submitBtn={"Confirm"} cancelBtn={"Cancel"} submitClick={() => onConfirm()} cancelClick={() => closeClick()} /> : null}
                {showAddSection ? <SidePreviewPanel content={content()} action={true} submitBtn={"Confirm"} cancelBtn={"Cancel"} submitClick={() => onConfirm()} cancelClick={() => closeClick()} /> : null}
                {showLiveCollectionPanel ? <SidePreviewPanel content={content()} action={true} submitBtn={"Confirm"} cancelBtn={"Cancel"} submitClick={() => onConfirm()} cancelClick={() => closeClick()} /> : null}
                {showCollaboratePanel ? <SidePreviewPanel content={content()} action={true} submitBtn={"Update"} cancelBtn={"Cancel"} submitClick={() => onUpdate()} cancelClick={() => closeClick()} /> : null}
                {/* {showShareCollection ? <SidePreviewPanel content={content()} action={true} closeClick={() => closeClick()} /> : null} */}
                {showShareCollection ?
                    <SidePreviewPanel content={content()}
                        action={true}
                        submitBtn={"Send"}
                        cancelBtn={"Cancel"}
                        submitClick={() => sendMail()}
                        cancelClick={() => closeClick()}
                        closeClick={() => closeClick()}
                        isPad={true}
                    /> : null}
                {showAddAssetCollection ? <SidePreviewPanel content={content()} action={false} closeClick={() => closeClick()} /> : null}
                {showEditBanner && <StreamBannerEdit openDialog={true} cropImageResponse={cropImageResponse} closeBanner={() => closeBanner()} />}
            </div>
            <div className={'generate-link-panel-wrap'}>
                {showGenerateLinkPanel ? <SidePreviewPanel content={content()} action={false} closeClick={() => closeClick()} /> : null}
            </div>

        </div >
    )
}

export default withRouter(CollectionItem)