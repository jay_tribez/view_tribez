import React from 'react';
import { mount, shallow } from 'enzyme';
import ShareCollection from "./ShareCollection";
import toJson from 'enzyme-to-json';
import { BrowserRouter as Router } from 'react-router-dom';
import { createMemoryHistory } from 'history'

import { render, screen, fireEvent } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";

describe('<ShareCollection />', () => {
    it("on render component page", async () => {
        let wrapper = render(
            <MemoryRouter>
                <ShareCollection />
            </MemoryRouter>
        );
    })
    it("on change the recipient mail", async () => {
        let wrapper = render(
            <MemoryRouter>
                <ShareCollection />
            </MemoryRouter>
        );
        // console.log('screen', screen.getByTestId('recipient-mail'))
        fireEvent.change(screen.getByTestId('recipient-mail'), { target: { value: "" } })
        fireEvent.change(screen.getByTestId('recipient-mail'), { target: { value: "Tribez@123" } })
        // fireEvent.click(screen.getByTestId('getlink-btn'))
    })
    it("on change the mail subject", async () => {
        let wrapper = render(
            <MemoryRouter>
                <ShareCollection />
            </MemoryRouter>
        );
        // console.log('screen', screen.getByTestId('mailsubject'))
        fireEvent.change(screen.getByTestId('mailsubject'), { target: { value: "" } })
        fireEvent.change(screen.getByTestId('mailsubject'), { target: { value: "Tribez@123" } })
        // fireEvent.click(screen.getByTestId('getlink-btn'))
    })
    it("on clicking create new template button", async () => {

        let wrapper = render(
            <MemoryRouter>
                <ShareCollection />
            </MemoryRouter>
        );
        screen.getByTestId('select-template')
        // fireEvent.click(screen.getByTestId('select-template'))

        // console.log('screen', screen.getByTestId('create-template-btn'))
        fireEvent.change(screen.getByTestId('recipient-mail'), { target: { value: "test" } })
        fireEvent.change(screen.getByTestId('mailsubject'), { target: { value: "test2" } })
        // fireEvent.click(screen.getByTestId('create-template-btn'))
    })

})

