import React, { Component } from 'react';
import { UploaderComponent } from '@syncfusion/ej2-react-inputs';
import { Browser } from '@syncfusion/ej2-base';
class BannerList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            bannerList: [
                {
                    "id": "5f0fe9bfbfe6615ee50a5aa4", "type": "stream", "mediaType": "image",
                    "imageUrl": "https://d2uav5q06z9nv6.cloudfront.net/public/library/stream/1/full.jpg",
                    "thumbnailUrl": "https://d2uav5q06z9nv6.cloudfront.net/public/library/stream/1/thumbnail.jpg",
                    "createdDate": 1594878399871, "modifiedDate": null
                },
                {
                    "id": "5f0fe9c0bfe6615ee50a5aa5", "type": "stream", "mediaType": "image",
                    "imageUrl": "https://d2uav5q06z9nv6.cloudfront.net/public/library/stream/2/full.jpg",
                    "thumbnailUrl": "https://d2uav5q06z9nv6.cloudfront.net/public/library/stream/2/thumbnail.jpg",
                    "createdDate": 1594878400305, "modifiedDate": null
                },
                {
                    "id": "5f0fe9c1bfe6615ee50a5aa6", "type": "stream", "mediaType": "image",
                    "imageUrl": "https://d2uav5q06z9nv6.cloudfront.net/public/library/stream/3/full.jpg",
                    "thumbnailUrl": "https://d2uav5q06z9nv6.cloudfront.net/public/library/stream/3/thumbnail.jpg",
                    "createdDate": 1594878401410, "modifiedDate": null
                },
                {
                    "id": "5f0fe9c1bfe6615ee50a5aa7", "type": "stream", "mediaType": "image",
                    "imageUrl": "https://d2uav5q06z9nv6.cloudfront.net/public/library/stream/4/full.jpg",
                    "thumbnailUrl": "https://d2uav5q06z9nv6.cloudfront.net/public/library/stream/4/thumbnail.jpg",
                    "createdDate": 1594878401802, "modifiedDate": null
                },
                {
                    "id": "5f0fe9c2bfe6615ee50a5aa8", "type": "stream", "mediaType": "image",
                    "imageUrl": "https://d2uav5q06z9nv6.cloudfront.net/public/library/stream/5/full.jpg",
                    "thumbnailUrl": "https://d2uav5q06z9nv6.cloudfront.net/public/library/stream/5/thumbnail.jpg",
                    "createdDate": 1594878402108, "modifiedDate": null
                },
                {
                    "id": "5f0fe9c2bfe6615ee50a5aa9", "type": "stream", "mediaType": "image",
                    "imageUrl": "https://d2uav5q06z9nv6.cloudfront.net/public/library/stream/6/full.jpg",
                    "thumbnailUrl": "https://d2uav5q06z9nv6.cloudfront.net/public/library/stream/6/thumbnail.jpg",
                    "createdDate": 1594878402415, "modifiedDate": null
                }, {
                    "id": "5f0fe9c2bfe6615ee50a5aaa", "type": "stream", "mediaType": "image",
                    "imageUrl": "https://d2uav5q06z9nv6.cloudfront.net/public/library/stream/7/full.jpg",
                    "thumbnailUrl": "https://d2uav5q06z9nv6.cloudfront.net/public/library/stream/7/thumbnail.jpg",
                    "createdDate": 1594878402722, "modifiedDate": null
                }, {
                    "id": "5f0fe9c3bfe6615ee50a5aab", "type": "stream", "mediaType": "image",
                    "imageUrl": "https://d2uav5q06z9nv6.cloudfront.net/public/library/stream/8/full.jpg",
                    "thumbnailUrl": "https://d2uav5q06z9nv6.cloudfront.net/public/library/stream/8/thumbnail.jpg",
                    "createdDate": 1594878403029, "modifiedDate": null
                },
                {
                    "id": "5f0fe9c3bfe6615ee50a5aac", "type": "stream", "mediaType": "video",
                    "imageUrl": "https://d2uav5q06z9nv6.cloudfront.net/public/library/stream/9/full.mp4",
                    "thumbnailUrl": "https://d2uav5q06z9nv6.cloudfront.net/public/library/stream/9/thumbnail.jpg",
                    "createdDate": 1594878403337, "modifiedDate": null
                },
                {
                    "id": "5f0fe9c3bfe6615ee50a5aad", "type": "stream", "mediaType": "video",
                    "imageUrl": "https://d2uav5q06z9nv6.cloudfront.net/public/library/stream/10/full.mp4",
                    "thumbnailUrl": "https://d2uav5q06z9nv6.cloudfront.net/public/library/stream/10/thumbnail.jpg",
                    "createdDate": 1594878403644, "modifiedDate": null
                },
                {
                    "id": "5f0fe9c3bfe6615ee50a5aae", "type": "stream", "mediaType": "video",
                    "imageUrl": "https://d2uav5q06z9nv6.cloudfront.net/public/library/stream/11/full.mp4",
                    "thumbnailUrl": "https://d2uav5q06z9nv6.cloudfront.net/public/library/stream/11/thumbnail.jpg",
                    "createdDate": 1594878403951, "modifiedDate": null
                },
                {
                    "id": "5f0fe9c4bfe6615ee50a5aaf", "type": "stream", "mediaType": "video",
                    "imageUrl": "https://d2uav5q06z9nv6.cloudfront.net/public/library/stream/12/full.mp4",
                    "thumbnailUrl": "https://d2uav5q06z9nv6.cloudfront.net/public/library/stream/12/thumbnail.jpg",
                    "createdDate": 1594878404231, "modifiedDate": null
                }]
        }

        this.dropAreaEle = null;
        this.dropContainerEle = null;
        this.dropImageEle = null;
        this.dropContainerRef = element => {
            this.dropContainerEle = element;
        };
        this.dropAreaRef = element => {
            this.dropAreaEle = element;
        };
        this.dropImageRef = element => {
            this.dropImageEle = element;
        };
        this.allowedExtensions = '.jpg,.png,.jpeg'
    }
    componentDidMount = () => {
        this.profileUploadRender()
    }
    profileUploadRender = () => {
        this.dropArea = this.dropAreaEle;
        this.dropElement = this.dropContainerEle;
        if (Browser.isDevice) { this.dropImageEle.style.padding = '0px 10%'; }
        this.uploadObj.element.setAttribute('name', 'UploadFiles');
        document.getElementById('browse').onclick = () => {
            document.getElementsByClassName('e-file-select-wrap')[0].querySelector('button').click();
            return false;
        };
        this.uploadObj.dropArea = this.dropElement;
        this.uploadObj.dataBind();
    }
    onSelect(args) {
        console.log(JSON.stringify(args))
        let file = args.filesData[0].rawFile; let reader = new FileReader();
        reader.addEventListener('load', () => {
            this.props.handleBanner(reader.result)

        }, false);
        if (file) { reader.readAsDataURL(file); }
    }
    render() {
        return (
            <div className="_bannerlist-main">
                <div className="upload-wrap" data-testid="upload-from-text">

                    <div className='control-pane' ref={this.dropContainerRef}>
                        <div className='control-section' id='uploadpreview'>
                            <div className='imagepreview'>
                                <div id='dropArea' ref={this.dropAreaRef} className='dropTarget uploadlocal-image'>
                                    <div id='dropimage' ref={this.dropImageRef} className='file-name-drop'>
                                        <p id='browse'>
                                            {this.state.uploadImg ?
                                                <img id="upload-image" src="" alt="" /> : <div>
                                                    <p>Upload from <span style={{ color: '#db4437' }}>Device</span></p>
                                                </div>}
                                        </p>
                                    </div>
                                    <UploaderComponent data-testid="upload-contact-image" id='previewfileupload' type='file' ref={upload => this.uploadObj = upload}
                                        // asyncSettings={this.asyncSettings}
                                        // success={ this.onUploadSuccess.bind(this) }
                                        selected={(e) => this.onSelect(e, this)}
                                        allowedExtensions={this.allowedExtensions}
                                    ></UploaderComponent>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
                {/* <div className="image-card-wrap"> */}
                {this.state.bannerList.map((item, index) => (
                    <div className="img-wrap" key={index} onClick={() => { this.props.handleBanner(item.imageUrl) }}>
                        <img src={item.thumbnailUrl}
                            alt="banner" ></img>
                    </div>
                ))}

                {/* </div> */}
            </div>

        );
    }
}

export default BannerList;