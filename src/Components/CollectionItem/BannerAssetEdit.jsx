import React, { Component } from 'react';
import './previewNavItem.scss';
import "../CollectionItem/CollectionItem.scss"
// import { TextBoxComponent } from '@syncfusion/ej2-react-inputs';
import { SwitchComponent } from '@syncfusion/ej2-react-buttons';
import collectionBg from '../../Assets/images/_collectionItem_headerbg.jpeg';

import { DialogComponent } from '@syncfusion/ej2-react-popups';
import BannerList from './BannerList';
import ReactCrop from 'react-image-crop';
import BannerFilter from './BannerFilter';
import BannerPosition from './BannerPosition';
import "../../theme.scss"

//Defining as Class component for banner edit functionality
class BannerAssetEdit extends Component {
    // Constructor method -->The constructor is a method used to initialize an object's state in a class.
    // It automatically called during the creation of an object in a class.
    constructor(props) {
        super(props);
        // Initializing state variables as an object because of class component
        this.state = {
            searchUser: '',
            users: [
                { "name": "Admin", },
                { "name": "Karthik Kumar" }
            ],
            bannerNavList: [
                { name: 'ADD BANNER', icon: 'far fa-image' },
                { name: 'CROP', icon: 'fas fa-crop-alt' },
                { name: 'FILTER', icon: 'fas fa-magic' },
                { name: 'POSITION', icon: 'fas fa-columns' },
                { name: 'SHOW SHARED BY', toggle: true }],
            isBannerList: false,
            isBannerCrop: false,
            mainBannerImg: collectionBg,
            crop: {
                unit: '%',
                width: 50,
                height: 50,
            },
            isBannerFilter: false,
            isBannerPosition: false
        }
    }

    //When the image is loaded into the component set to crop object its width and height
    onImageLoaded = image => {
        this.setState({ crop: { width: image.width, height: image.height } });
        return false; // Return false when setting crop state in here.
    };
    //On clicking the 
    openClick = (item, type) => {
        switch (type) {
            case 1:
                return (
                    this.setState({ isBannerList: true })
                )

            case 2:
                return (
                    this.setState({ isBannerCrop: true })
                )

            case 3:
                return (
                    this.setState({ isBannerFilter: true })
                )

            case 4:
                return (
                    this.setState({ isBannerPosition: true })
                )


            default:

                break;
        }

    }

    header(val) {
        return (
            <>
                {!this.state.isBannerList ?
                    <div className="_collectionItem_navList" style={{ width: '70%' }} data-testid="banner-header">
                        {val.map((item, index) => (
                            <>
                                <div className="dFlexCenter">
                                    {item.icon && <i style={{ color: '$primaryColor' }} className={`${item.icon}  nav-list-icon`}></i>}
                                    {item.toggle && <SwitchComponent id="checked" checked={true}></SwitchComponent>}
                                    <p style={{ color: '$primaryColor', fontSize: '12px', marginLeft: '5px' }}
                                        onClick={() => this.openClick(item, index + 1)}
                                    >{item.name}</p>
                                </div>
                            </>
                        ))}
                    </div> :
                    <div className="_collectionItem_navList" style={{ width: '70%' }}>
                        <p style={{ color: 'inherit', cursor: 'auto' }}>
                            <i class="fas fa-chevron-left"
                                onClick={this.closeBannerList}
                                style={{ marginRight: '10px', color: 'inherit', cursor: 'pointer' }}></i>
                            Customize your banner
                        </p>
                    </div>
                }
            </>
        );
    }
    handleBanner = (item) => {
        this.setState({ isBannerList: false, mainBannerImg: item.src })
    }
    closeBannerList = () => {
        this.setState({ isBannerList: false })

    }
    handleClose = () => {
        this.setState({ isBannerFilter: false, isBannerPosition: false })

    }
    footerTemplate() {
        return (<div>
        </div>);
    }

    content() {
        return (
            <>
                {this.state.isBannerList ?
                    <BannerList handleBanner={this.handleBanner} /> :
                    this.state.isBannerCrop ?
                        <ReactCrop src={this.state.mainBannerImg} crop={this.state.crop}
                            onImageLoaded={this.onImageLoaded}
                            onChange={newCrop => this.setState({
                                crop: newCrop
                            })} /> :
                        <div className="_collectionItem_texture">
                            <img src={this.state.mainBannerImg} alt="bg" width="100%" height="90%" /> </div>
                }
                {
                    this.state.isBannerFilter &&
                    <BannerFilter handleClose={this.handleClose} />
                }
                {
                    this.state.isBannerPosition &&
                    <BannerPosition handleClose={this.handleClose} />
                }
            </>
        );
    }
    render() {
        const { openDialog, closeBanner } = this.props
        const { bannerNavList } = this.state
        return (
            <React.Fragment>
                {openDialog &&
                    <div className="_edit-banner-backdrop" data-testid="banner-assetedit">
                        <DialogComponent
                            header={() => this.header(bannerNavList)}
                            footerTemplate={this.footerTemplate}
                            content={() => this.content()}
                            showCloseIcon={true}
                            ref={dialog => this.dialogInstance = dialog}
                            // target='#target'
                            // width={'437px'}
                            // open={this.dialogOpen}
                            close={closeBanner}
                            // height={'255px'}
                            visible={true}
                            className="dialog-wrap"
                        >
                        </DialogComponent>
                    </div>}
            </React.Fragment>
        );
    }
}

export default BannerAssetEdit;