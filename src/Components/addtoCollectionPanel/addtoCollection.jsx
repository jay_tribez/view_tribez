import React, { Component } from 'react';
import { AccordionComponent, AccordionItemDirective, AccordionItemsDirective } from '@syncfusion/ej2-react-navigations';
import { TextBoxComponent } from '@syncfusion/ej2-react-inputs';
import { TreeViewComponent } from '@syncfusion/ej2-react-navigations';
const data = require('../cloneAssetPanel/data').default
class AddToCollectionPanel extends Component {
    //Constructor method -->The constructor is a method used to initialize an object's state in a class. 
    // It automatically called during the creation of an object in a class.
    constructor(props) {
        //Initializing state variables as an object because of class component
        //myCollection variable should inititalize as [] but for UI render just added from mockdata 
        super(props);
        this.state = {
            collectionName: '',
            isChecked: true,
            myCollectionfield: {
                dataSource: data.myCollection,
                id: 'id',
                parentID: 'pid',
                text: 'name',
                hasChildren: 'hasChild'
            },
            allCollections: [
                {
                    "id": "uncategorized", "name": "Uncategorized",
                    "entityType": "collection", "icon": null, "poster": null, "createdDate": 1632795793580, "modifiedDate": 1632795793580, "live": false, "collectionTemplate": false, "type": "uncategorized", "creator": false, "customerLogo": null, "assetsCount": 0, "sectionsCount": 0, "writeAll": false, "readAll": false, "canEdit": false, "canDelete": false, "template": null
                }, {
                    "id": "60fab5c057b3bb204aba0b2e",
                    "name": "(sample)Meet Paperflite", "entityType": "collection", "icon": { "id": "60fac4ab94ce93536b32d6c3", "type": "image", "thumbnail": "https://d2uav5q06z9nv6.cloudfront.net/60fab5b857b3bb204aba0af2/images/60fac4ab94ce93536b32d6c3/a057164b-bfc9-4697-93e4-5344017b71bb", "full": "https://d2uav5q06z9nv6.cloudfront.net/60fab5b857b3bb204aba0af2/images/60fac4ab94ce93536b32d6c3/d42e7062-8789-4a2c-85b8-cbc63d4ff9ef", "key": null }, "poster": null, "createdDate": 1627043264214, "modifiedDate": 1632716130590, "live": false, "collectionTemplate": false, "type": "user_defined", "creator": true, "customerLogo": null, "assetsCount": 0, "sectionsCount": 2, "writeAll": false, "readAll": false, "canEdit": true, "canDelete": true, "template": null
                },
                {
                    "id": "60fab5c057b3bb204aba0b30",
                    "name": "(sample)Start your Paperflite journey here!", "entityType": "collection", "icon": { "id": "60fab5d482216e60989905c0", "type": "image", "thumbnail": "https://d2uav5q06z9nv6.cloudfront.net/60fab5b857b3bb204aba0af2/images/60fab5d482216e60989905c0/15251b84-ed9f-46e7-a4db-fa264e075600", "full": "https://d2uav5q06z9nv6.cloudfront.net/60fab5b857b3bb204aba0af2/images/60fab5d482216e60989905c0/8fa9530c-1954-4746-922f-a13e83ac320d", "key": null }, "poster": null, "createdDate": 1627043264253, "modifiedDate": 1630226337839, "live": true, "collectionTemplate": false, "type": "user_defined", "creator": true, "customerLogo": null, "assetsCount": 0, "sectionsCount": 5, "writeAll": false, "readAll": false, "canEdit": true, "canDelete": true, "template": null
                },
                {
                    "id": "60ff8793a8aa4a541e4de2ad",
                    "name": "ALK Demo", "entityType": "collection", "icon": { "id": "60fac4f594ce93536b32d6f7", "type": "image", "thumbnail": "https://d2uav5q06z9nv6.cloudfront.net/60fab5b857b3bb204aba0af2/images/60fac4f594ce93536b32d6f7/6f488d28-ccfd-4afb-93c5-dc02b322f1ce", "full": "https://d2uav5q06z9nv6.cloudfront.net/60fab5b857b3bb204aba0af2/images/60fac4f594ce93536b32d6f7/ff1301c3-5f41-4a7d-9986-34cf88ae0a8d", "key": null }, "poster": null, "createdDate": 1627359123137, "modifiedDate": 1631905371674, "live": true, "collectionTemplate": false, "type": "user_defined", "creator": true, "customerLogo": null, "assetsCount": 3, "sectionsCount": 0, "writeAll": false, "readAll": false, "canEdit": true, "canDelete": true, "template": null
                },
                {
                    "id": "6146fe2386208413ede9283a",
                    "name": "Tata AIA Life Insurance", "entityType": "collection", "icon": { "id": "6146fd9432ab675e766c0f67", "type": "image", "thumbnail": "https://d2uav5q06z9nv6.cloudfront.net/60fab5b857b3bb204aba0af2/images/6146fd9432ab675e766c0f67/ba71a6a0-46ad-4462-ba2f-6ae3c0f17680", "full": "https://d2uav5q06z9nv6.cloudfront.net/60fab5b857b3bb204aba0af2/images/6146fd9432ab675e766c0f67/97a4319e-e15e-4c7f-8efc-ef055da623e2", "key": null }, "poster": null, "createdDate": 1632042531595, "modifiedDate": 1632067424644, "live": false, "collectionTemplate": false, "type": "user_defined", "creator": true, "customerLogo": null, "assetsCount": 1, "sectionsCount": 0, "writeAll": false, "readAll": false, "canEdit": true, "canDelete": true, "template": null
                },
                {
                    "id": "60fe7a71064b80462bf48f7b",
                    "name": "TGIFS", "entityType": "collection", "icon": { "id": "60fac4e994ce93536b32d6df", "type": "image", "thumbnail": "https://d2uav5q06z9nv6.cloudfront.net/60fab5b857b3bb204aba0af2/images/60fac4e994ce93536b32d6df/4c901cef-3e46-43f4-96c8-4420759036ea", "full": "https://d2uav5q06z9nv6.cloudfront.net/60fab5b857b3bb204aba0af2/images/60fac4e994ce93536b32d6df/79fb31c8-b599-4dda-9a66-b519a3bf9fe8", "key": null }, "poster": null, "createdDate": 1627290225245, "modifiedDate": 1627290225245, "live": false, "collectionTemplate": false, "type": "user_defined", "creator": true, "customerLogo": null, "assetsCount": 2, "sectionsCount": 0, "writeAll": false, "readAll": false, "canEdit": true, "canDelete": true, "template": null
                },
                {
                    "id": "60fe7bc4064b80462bf48f8c",
                    "name": "TGIFS-1", "entityType": "collection", "icon": { "id": "60fac4e994ce93536b32d6df", "type": "image", "thumbnail": "https://d2uav5q06z9nv6.cloudfront.net/60fab5b857b3bb204aba0af2/images/60fac4e994ce93536b32d6df/4c901cef-3e46-43f4-96c8-4420759036ea", "full": "https://d2uav5q06z9nv6.cloudfront.net/60fab5b857b3bb204aba0af2/images/60fac4e994ce93536b32d6df/79fb31c8-b599-4dda-9a66-b519a3bf9fe8", "key": null }, "poster": null, "createdDate": 1627290564025, "modifiedDate": 1627290564025, "live": false, "collectionTemplate": false, "type": "user_defined", "creator": true, "customerLogo": null, "assetsCount": 2, "sectionsCount": 0, "writeAll": false, "readAll": false, "canEdit": true, "canDelete": true, "template": null
                },
                {
                    "id": "6142ea9d9e70782f3c5141f4",
                    "name": "Tribez", "entityType": "collection", "icon": null, "poster": null, "createdDate": 1631775389021, "modifiedDate": 1631775389021, "live": false, "collectionTemplate": false, "type": "user_defined", "creator": true, "customerLogo": null, "assetsCount": 0, "sectionsCount": 0, "writeAll": false, "readAll": false, "canEdit": true, "canDelete": true, "template": null
                },
                {
                    "id": "60fabf8f9f08f15dea9d96cd",
                    "name": "Tribez.ai - An overview", "entityType": "collection", "icon": { "id": "60fac51694ce93536b32d720", "type": "image", "thumbnail": "https://d2uav5q06z9nv6.cloudfront.net/60fab5b857b3bb204aba0af2/images/60fac51694ce93536b32d720/19eb2980-3fd5-4f38-a841-5417acae6151", "full": "https://d2uav5q06z9nv6.cloudfront.net/60fab5b857b3bb204aba0af2/images/60fac51694ce93536b32d720/1002246a-72fc-4a5b-8f47-59af519bfac4", "key": null }, "poster": null, "createdDate": 1627045775332, "modifiedDate": 1632457223969, "live": false, "collectionTemplate": false, "type": "user_defined", "creator": true, "customerLogo": null, "assetsCount": 0, "sectionsCount": 2, "writeAll": false, "readAll": false, "canEdit": true, "canDelete": true, "template": null
                }
            ]
        }
    }
    //On change the input value of collection name function , it will assign the values whenever input onchange triggers
    //The typed value will be assign to collectionName state variable
    onHandlecollectionName = (e) => {
        this.setState({ collectionName: e.target.value });
    }
    //This is a content method for accordion when user click on "+ Create a collection" field,
    // Text input box should render
    acrdnContent1 = () => {
        return (<div style={{ marginTop: '10px' }}>
            <TextBoxComponent
                cssClass="e-outline" floatLabelType=""
                type="text" placeholder="Enter a collection name"
                //  onChange={(e) => { this.setState({ searchUser: e.target.value }) }}
                value={this.state.collectionName}
                onChange={this.onHandlecollectionName}
            />
            <p>Please enter a name for the new collection you’d like to create. The selected assets <br />
                will automatically be added to this new collection.</p>
        </div>)
    }
    //Render Method for class component
    render() {
        return (<React.Fragment>
            <div className="preview-container-wrap collection-dropdown-wrapper">
                <div className="preview-container" style={{ paddingBottom: 0 }}>
                    <p className="turnon-template-heading" data-testid="cloning-heading">Collections</p>
                    <p className="template-subheading" data-testid="cloning-subheading">personalized content storyboards<br /></p>
                    <div className="collection-container">
                        <AccordionComponent>
                            <AccordionItemsDirective>
                                <AccordionItemDirective header={"Create New Collection"} content={this.acrdnContent1} />
                            </AccordionItemsDirective>
                        </AccordionComponent>
                        <div className="collection-item-wrapper addcollection">
                            <div className="view-wrapper">
                                <TreeViewComponent fields={this.state.myCollectionfield} showCheckBox={this.state.isChecked} nodeChecked={this.nodeChecked} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment >);
    }
}
export default AddToCollectionPanel;