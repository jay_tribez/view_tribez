import React from 'react';
import { render, screen, fireEvent } from "@testing-library/react";
import { mount } from 'enzyme';
import Addtocollection from './addtoCollection';
import { AccordionComponent, AccordionItemDirective, AccordionItemsDirective, TreeViewComponent } from '@syncfusion/ej2-react-navigations';
import { TextBoxComponent } from '@syncfusion/ej2-react-inputs';
const data = require('../cloneAssetPanel/data').default

let acrdnContent1 = () => {
    return (<div style={{ marginTop: '10px' }}>
        <TextBoxComponent
            data-testid="collection-name"
            id="collection-name"
            cssClass="e-outline" floatLabelType=""
            type="text" placeholder="Enter a collection name"
            value={''}
        />
        <p>Please enter a name for the new collection you’d like to create. The selected assets <br />
            will automatically be added to this new collection.</p>
    </div>)
}
let myCollectionfield = {
    dataSource: data.myCollection,
    id: 'id',
    parentID: 'pid',
    text: 'name',
    hasChildren: 'hasChild'
}
describe('Initial rendering of Add to Coolection component panel initial rendering', () => {
    it('on render', async () => {
        let wrapper = render(
            <Addtocollection />
        );
        expect(screen.getByTestId('cloning-heading').textContent).toBe('Collections')
        expect(screen.getByTestId('cloning-subheading').textContent).toBe('personalized content storyboards')
    })
    it('create new collection -->accordion component should render correctly', async () => {
        let wrapper = render(
            <Addtocollection />
        );
        const accordion = render(
            <AccordionComponent>
                <AccordionItemsDirective>
                    <AccordionItemDirective header={"Create New Collection"} content={acrdnContent1} />
                </AccordionItemsDirective>
            </AccordionComponent>)
        expect(accordion.container.querySelector(".e-acrdn-header-content").textContent).toBe('Create New Collection')
    })
    it('accordion component should render with textcomponent', async () => {
        let wrapper = render(
            <Addtocollection />
        );
        const accordion = render(
            <AccordionComponent>
                <AccordionItemsDirective>
                    <AccordionItemDirective header={"Create New Collection"} content={acrdnContent1} />
                </AccordionItemsDirective>
            </AccordionComponent>)
        expect(accordion.container.querySelector(".e-acrdn-header-content").textContent).toBe('Create New Collection')
        fireEvent.click(accordion.container.querySelector(".e-acrdn-header-content"))
        expect(accordion.container.querySelector('#collection-name').tagName).toBe('INPUT')
    })
    it('Create collection panel should render treeview component', async () => {
        let wrapper = render(
            <Addtocollection />
        );
        const tree = render(<TreeViewComponent fields={myCollectionfield} showCheckBox={true} nodeChecked={true} />)
        expect(tree.container.querySelector(".e-list-text").textContent).toBe('(sample)Meet Paperflite')
    })

})