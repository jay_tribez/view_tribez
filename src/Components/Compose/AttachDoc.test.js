import React from 'react';
import { mount, shallow } from 'enzyme';
import AttachDocComponent from "./AttachDoc";
import toJson from 'enzyme-to-json';
import AttachDoc from './AttachDoc';

import { render, screen, fireEvent } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";

describe('<AttachDocComponent />', () => {
   
    it('On component should render', async () => {
        render(
            <MemoryRouter>
                <AttachDocComponent />
            </MemoryRouter >
        );
        screen.getByTestId('attachment-doc')
        expect(screen.getByTestId('subtitle').textContent).toBe('Attach assets to your email')

        // expect(toJson(wrapper)).toMatchSnapshot();
    })


})
