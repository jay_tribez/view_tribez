import React, { Component } from 'react';
import { TextBoxComponent } from '@syncfusion/ej2-react-inputs';
import { AccordionComponent, AccordionItemDirective, AccordionItemsDirective } from '@syncfusion/ej2-react-navigations';
import { ButtonComponent, SwitchComponent } from '@syncfusion/ej2-react-buttons';
import { RadioButtonComponent } from '@syncfusion/ej2-react-buttons';
import { DateTimePickerComponent } from '@syncfusion/ej2-react-calendars';
import assetsData from "../../Assets/Data/assets.json";
import searchUser from "../../Assets/images/searchUser.svg"
import AttachDoc from './AttachDoc';
import AddAssetCollection from '../CollectionItem/AddAssetCollection';
import { RichTextEditorComponent, Toolbar, Inject, Image, Link, HtmlEditor, Count, QuickToolbar, Table } from '@syncfusion/ej2-react-richtexteditor';
import {
    withRouter
} from "react-router-dom";
import EmailChipInput from "../Common/validation/emailChip";
import { closest, Touch, SwipeEventArgs, isNullOrUndefined } from '@syncfusion/ej2-base';
import streanLoadingImg from "../../Assets/images/stream_loading_icon.jpg";

function swipeable() {
    let fanStructuteCard = document.querySelectorAll('#horizontal_product .e-card');
    let len = fanStructuteCard.length;
    [].slice.call(fanStructuteCard).forEach((ele) => {
        ele.style.removeProperty('transform');
    });
    let transformRatio = 2;
    let temp;
    let divide = (parseInt((len / 2).toString(), 10));
    temp = transformRatio;
    for (let i = divide - 1; i >= 0; i--) {
        fanStructuteCard[i].style.transform = 'rotate(' + (temp) + 'deg)';
        temp += transformRatio;
    }
    transformRatio = 2;
    temp = transformRatio;
    for (let i = divide + 1; i < len; i++) {
        fanStructuteCard[i].style.transform = 'rotate(' + (-temp) + 'deg)';
        temp += transformRatio;
    }
}
class Compose extends Component {
    constructor(props) {
        super(props);
        console.log(this.props);
        this.state = {
            recipientEmail: {},
            mailSubject: '',
            streamData: [{
                name: "Distributed Architecture Wh…",
                img: "https://d2uav5q06z9nv6.cloudfront.net/60fab5b857b3bb204aba0af2/images/60fac53a94ce93536b32d72d/007989fe-abe6-4fe3-8aea-851515f37a87",
                updateAt: new Date(),
                completed: 25
            }, {
                name: "Introduction to Operating Syste…",
                img: "https://d2uav5q06z9nv6.cloudfront.net/60fab5b857b3bb204aba0af2/images/60fac53a94ce93536b32d72d/007989fe-abe6-4fe3-8aea-851515f37a87",
                updateAt: new Date(),
                completed: 0
            }],
            isTemplate: false,
            isAttachment: false,
            content: assetsData[0].body,
            composeDetails: {},
            collectionItemData: this.props.collectionItemData
        }
    }
    // handleSetPassword = (e) => {
    //     let value = e.changedProperties.checked
    // }
    componentDidMount = () => {
        this.rendereComplete()
    }
    rendereComplete() {
        let ele = document.getElementById('horizontal_product');
        swipeable();
        new Touch(ele, { swipe: this.touchSwipeHandler });
        let cards = document.querySelectorAll('#horizontal_product .e-card');
        [].slice.call(cards).forEach((ele) => {
            ele.querySelector('img').onmousedown = () => { return false; };
        });
    }
    toggleSwitchPass = () => {
        return (
            <div className="toggle">
                <label htmlFor="checked" > Set a password </label>
                <SwitchComponent id="checked" checked={false}></SwitchComponent>
            </div>)
    }
    toggleSwitchTime = () => {
        return (
            <div className="toggle">
                <label htmlFor="checked" > Set an expiry date & time </label>
                <SwitchComponent id="checked" checked={false}></SwitchComponent>
            </div>)
    }
    passwordCont = () => {
        return (
            <div className="ad-input">
                <TextBoxComponent floatLabelType="Never"
                    style={{ width: '100%' }} type="password" placeholder="Enter Password"
                    onChange={(e) => { this.setState({ mailSubject: e.target.value }) }}
                    value={this.state.mailSubject} />
            </div>)
    }
    dateAndTimeCont = () => {
        return (
            <div className="ad-input">
                <DateTimePickerComponent></DateTimePickerComponent>
            </div>
        )
    }

    updateEmailSubject = (e) => {
        this.setState({ mailSubject: e.value })
        localStorage.setItem('subject', e.value)
    }
    acrdnContent2 = () => {
        return (
            <div className="_share_advanced_settings">
                <div className="title">
                    <label htmlFor="checked" > Allow recipients to download </label>
                    <SwitchComponent id="checked" checked={true}></SwitchComponent>
                </div>
                <div className="edit-block">
                    <div id="collaborator-tab" className="_share-radio-group">
                        <RadioButtonComponent label="Editable" name="Collaborators" cssClass="radio-btn" />
                        <RadioButtonComponent label="Read Only" name="Collaborators" cssClass="radio-btn" />
                    </div>
                    <p className="warning-text">*This applies to PPTs, Docs, Google Slides and Google Docs.</p>
                </div>
                <div className="title">
                    <label htmlFor="checked" > Allow recipients to reshare </label>
                    <SwitchComponent id="checked" checked={true}></SwitchComponent>
                </div>
                <div className="setpasswordWrap">
                    <AccordionComponent>
                        <AccordionItemsDirective>
                            <AccordionItemDirective header={this.toggleSwitchPass} content={this.passwordCont} />
                        </AccordionItemsDirective>
                    </AccordionComponent>
                </div>
                <div className="setpasswordWrap">
                    <AccordionComponent>
                        <AccordionItemsDirective>
                            <AccordionItemDirective header={this.toggleSwitchTime} content={this.dateAndTimeCont} />
                        </AccordionItemsDirective>
                    </AccordionComponent>
                </div>
            </div>
        );
    }
    handleTemplateOpen = () => {
        this.props.handleWideWidth()
        this.setState({
            isTemplate: true
        })
    }
    content = () => {
        return (
            <p>test</p>)
    }
    handleCloseTemplate = () => {
        this.props.handleCloseTemplate()
        this.setState({ isTemplate: false, isAttachment: false })

    }
    handleAttchDoc = () => {
        this.props.handleWideWidth()
        this.setState({
            isAttachment: true
        })
    }
    onEmailTemplate = () => {
        let body = document.getElementsByTagName('body')
        body[0].className = body[0].className.replace(' sidecontentnav')
        this.props.history.push('/app/settings/emailtemplates')
    }
    closeClick = () => {
        let body = document.getElementsByTagName('body')
        body[0].className = body[0].className.replace(' sidecontentnav')
        this.setState({ isTemplate: false, isAttachment: false })
        this.props.closeClick()
    }
    emailrecipient = (recipientEmail) => {
        this.setState({ recipientEmail }, () => {
            let composeDetails = this.state.composeDetails
            composeDetails["recipientEmail"] = this.state.recipientEmail
            composeDetails["emailBody"] = this.rteObj.value
            composeDetails["details"] = this.state.collectionItemData
            console.log("composeDetails",JSON.stringify(composeDetails))
            this.props.onRecipientEmail(composeDetails)
        })
    }
    render() {
        const { isTemplate, isAttachment } = this.state
        return (
            this.state.collectionItemData && <div className="_share_dflex">
                <div className="_compose-preview-share-wrap">
                    <div className="preview-container">
                        <p className="turnon-template-heading" data-testid='compose-title'>compose<br /></p>
                        <p className="template-subheading" data-testid="compose-subtitle" style={{ marginTop: 0 }}>quick send
                        </p>
                    </div>
                    <div className="_share-inputs-wrap">
                        <div><EmailChipInput label={"To"} actionBtn={["CC", "BCC"]} emailrecipient={this.emailrecipient} /></div>
                        <TextBoxComponent floatLabelType="Never"
                            style={{ width: '100%', paddingTop: '1em' }} type="text" placeholder="Subject"
                            onChange={(e) => { this.updateEmailSubject(e) }}
                            value={this.state.mailSubject} />
                    </div>
                    {/* <div className="_share-template-select">
                        <p onClick={this.handleTemplateOpen}>SELECT TEMPLATE</p>
                        <p>INSERT WIDGET</p>
                    </div> */}
                    <div className="_share-body-content">
                        {/* <div className="side-panel-desc" dangerouslySetInnerHTML={{ __html: assetsData[0].body }}></div> */}
                        <RichTextEditorComponent ref={(richtexteditor) => { this.rteObj = richtexteditor; }} iframeSettings={this.iframeSetting} value={this.state.content} onChange={(e) => { this.setState({ content: e.target.value }) }}>
                            <Inject services={[Image, Link, HtmlEditor, QuickToolbar]} />
                        </RichTextEditorComponent>
                    </div>
                    <div className="add-button-wrap">
                        <p className="attach-doc">Attach<br />Documents</p>
                        <div className="add-button" onClick={this.handleAttchDoc}>+</div>
                    </div>
                </div>
                {isTemplate &&
                    <div className="_share-template-Selection">
                        <i onClick={this.handleCloseTemplate}
                            className="fas fa-times-circle close-icon-cross"></i>
                        <TextBoxComponent floatLabelType="Never"
                            type="text" placeholder="Type here for search"
                            value={''} />
                        <div className="no-content-block">
                            <img src={searchUser} className="no-template-img" alt="search" />
                            <div className="content-block">
                                <p className="talk-less">Type less, Share more!</p>
                                <p className="desc">Create reusable email templates
                                    for you and your teams.</p>
                                <ButtonComponent cssClass='e-primary email-temp' onClick={() => this.onEmailTemplate()}>
                                    Create new email <br /> template</ButtonComponent>
                            </div>
                        </div>
                    </div>
                }
                {isAttachment ?
                    <>
                        {/* <AddAssetCollection closeClick={this.handleCloseTemplate} uploadType={"compose"} /> */}
                        <AttachDoc handleCloseTemplate={this.handleCloseTemplate} closeClick={this.closeClick} />

                        <div className="_share-assets-wrap">
                            <div className="title-wrap">
                                <p className="template-subheading basicPtag">0 Assets</p>
                                <div className="no-assets-wrap">
                                    <p className="vacant">This space vacant / available / to let</p>
                                    <p className="desc">No assets added yet. Click the big pink + to add them from your collections, or just drag & drop them in from your device.</p>
                                </div>
                            </div>
                        </div>
                    </>
                    :
                    this.state.collectionItemData.sections &&
                    <div className="_share-assets-wrap">
                        <div className="title-wrap">
                            <p className="template-subheading basicPtag" style={{ padding: "3em 2em" }}>{this.state.collectionItemData.name}</p>
                            <div className="no-assets-wrap">

                                <div className='control-pane'>
                                    <div className='control-section card-control-section swipe_card_layout'>
                                        {this.state.collectionItemData.sections.map((item, i) => {
                                            return <div className="e-card-resize-container" key={i}>
                                                <div className='row'>
                                                    <div className="row card-layout">
                                                        <div className="col-xs-12 col-sm-12 col-lg-12 col-md-12" id="horizontal_product">
                                                            <div className="e-card e-card-horizontal">
                                                                <img src={`${item.icon != null ? item.icon : streanLoadingImg}`} style={{ height: '150px' }} alt="sydney" />
                                                                <div className='e-card-stacked'>
                                                                    <div className="e-card-content" style={{ height: '150px' }}>
                                                                        <p>{item.name}</p>
                                                                        <p style={{ fontWeight: "bold", borderTop: "1px solid", width: "100%", paddingTop: "10px" }}>{this.state.collectionItemData.sections.length} Sections</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        })}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>}


            </div>);
    }
}

export default withRouter(Compose);