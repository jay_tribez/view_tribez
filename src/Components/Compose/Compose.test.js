import React from 'react';
import { mount, shallow } from 'enzyme';
import ComposeComponent from "./Compose";
import toJson from 'enzyme-to-json';

import { render, screen, fireEvent } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";

describe('<ComposeComponent />', () => {
    it('On component should render', async () => {
        render(
            <MemoryRouter>
                <ComposeComponent />
            </MemoryRouter >
        );
        // screen.getByTestId('attachment-doc')
        expect(screen.getByTestId('compose-subtitle').textContent).toBe('quick send')
        expect(screen.getByTestId('compose-title').textContent).toBe('compose')

        // expect(toJson(wrapper)).toMatchSnapshot();
    })
       // let wrapper;
    // beforeEach(() => {
    //     wrapper = shallow(<ComposeComponent />
    //     );
    // });

    // it('should render correctly', async () => {
    //     expect(toJson(wrapper)).toMatchSnapshot();
    // })

})
