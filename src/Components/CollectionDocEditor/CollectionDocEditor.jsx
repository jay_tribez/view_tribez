import React, { Component } from 'react';
import FileViewer from 'react-file-viewer';
import SamplePDF from '../../Assets/Data/samplepdf.pdf';

const file = SamplePDF
const type = 'pdf'

//This is a class component only to render a pdf viewer
//Just pass a file url and file type
class CollectionDocEditor extends Component {
  onError(e) {
    console.log(e, 'error in file-viewer');
  }
  render() {
    return (
      <div style={{ border: '1px solid', width: '90%', margin: '0 auto' }}>
        <FileViewer
          fileType={type}
          filePath={file}
          onError={this.onError}
          style={{}} />
      </div>
    );
  }


}

export default CollectionDocEditor