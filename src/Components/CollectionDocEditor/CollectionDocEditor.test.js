import React from 'react';
import { shallow } from 'enzyme';
import CollectionDocEditor from "./CollectionDocEditor";
import toJson from 'enzyme-to-json';
import  FileViewer  from 'react-file-viewer';


describe('<CollectionDocEditor />', () => {
    let wrapper;
    beforeEach(() => {
        wrapper = shallow(<CollectionDocEditor />);
    });

    it('should render correctly', async () => {
        expect(toJson(wrapper)).toMatchSnapshot();
    })
})

