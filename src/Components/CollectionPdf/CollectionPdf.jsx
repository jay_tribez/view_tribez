import React, { Component, useEffect, useRef, useState } from 'react';
import { useLocation, withRouter } from 'react-router-dom';
import {
    PdfViewerComponent, Toolbar, Magnification, Navigation, LinkAnnotation, BookmarkView,
    ThumbnailView, Print, TextSelection, TextSearch, Annotation, FormFields, Inject
} from '@syncfusion/ej2-react-pdfviewer';
import "./CollectionPdf.scss";
import Filepdf from "../../Assets/files/Brochure.pdf";
import { DocumentEditorContainerComponent } from '@syncfusion/ej2-react-documenteditor';
import { DraggableCore } from 'react-draggable';
import ReactPlayer from 'react-player'
import CollectionDocEditor from '../CollectionDocEditor';
import axios from 'axios';
import FileViewer from 'react-file-viewer';
import img_asset from "../../Assets/images/img_asset.jpeg";
import SidePreviewPanel from "../sidePreviewPanel";
import GenerateLink from '../CollectionItem/generateLink';
import AnalyticsPanel from "../analyticsPanel/analyticsPanel";
import Compose from '../Compose';
import EditAssets from "../assetPreview/editAssets";
import "../hub/index.scss";
import AddToCollectionPanel from '../addtoCollectionPanel/addtoCollection';
import CloneAssetsPanel from '../cloneAssetPanel/cloneAssetPanel';
import assetsData from '../conversation/assets.json'

const file = '../../Assets/images/banner1.png'
const fType = 'png'

//Defining a functional Component for collection pdf component

const CollectionPdf = (props) => {
    //declaring State variables using useState because of functional components.
    // Since there is no api available for now, some of the mock data added in the state for ui render

    const [showGenerateLinkPanel, setShowgeneratelinkpanel] = useState(false)
    const [showComposePanel, setShowComposepanel] = useState(false)
    const [isWideScreen, setIsWideScreen] = useState(false)
    const [showAnalyticsPanel, setShowAnalyticsPanel] = useState(false)
    const [showEditAssetsPanel, setShowEditAssetsPanel] = useState(false)
    const [openMoreOption, setOpenMoreOption] = useState(false)
    const [assetData, setAssetData] = useState({
        assetName: "(sample)Tribez Support Center",
        assetImg: 'https://d2uav5q06z9nv6.cloudfront.net/60fab5b857b3bb204aba0af2/images/6142e740a4114c1ae09e2419/afbc0e7e-f240-4028-b38e-9fef130d6a2b',
        size: '151KB',
        contentType: 'Image',
        imgSource: 'Tribez Cloud',
        publisher: "Karthi",
        lastUpdated: "16 Sep 2021"
    })
    const [showAddtoCollectionPanel, setShowAddtoCollectionPanel] = useState(false)
    const [showCloneAssetsPanel, setShowCloneAssetsPanel] = useState(false)
    const [downloadLink, setDownloadLink] = useState('https://api.paperflite.com/api/2.0/assets/6142e72fd28b5d6f704dfa82/download?modifiedDate=1631774512000&amp;access_token=99a88a93-256c-4727-b078-7cade1f23618')
    const hostUrl = 'https://ej2services.syncfusion.com/production/web-services/';
    const location = useLocation();
    const yt = "https://www.youtube.com/watch?v=PQb2ekeczRE";
    const container = useRef()
    const { type, img } = location.state.detail;
    const tempImg = "https://api.paperflite.com/api/2.0/assets/60fab5b957b3bb204aba0afd/download?modifiedDate=1576140741000&amp;preview=true&amp;access_token=7af8384c-bbcd-4801-b0d4-2e6308714a6d"

    //useEffect is a lifecycle method for functional components same as componentdidmount in class components
    //It  will execute only one time 
    useEffect(() => {
        // const blobUrl = "https://clipapp-dev-files.s3.ap-south-1.amazonaws.com/reportfiles/80f5e4ed-1efa-4c58-8701-7ec9a3b07950/30511dae-ff93-456e-b60b-2df70c74bba3/pdf_annotation_rzpr969z2.pdf"
        // const config = { responseType: 'blob' };
        // let pdfData
        // axios.get(blobUrl, config).then(response => {
        //     pdfData = new File([response], 'fileName');
        // });
        // var reader = new window.FileReader();
        // //read the blob data 
        // reader.readAsDataURL(pdfData);
        // var base64data = reader.result;
        // var pdfviewer = (document.getElementById('container')).ej2_instances[0];
        // pdfviewer.load(base64data, null);

    }, [])
    //Collection item navigation list item variable
    const navList = [
        { name: "SHARE", icon: 'fa fa-share-alt' },
        { name: "DOWNLOAD", icon: 'fa fa-download' },
        { name: "GENERATE LINK", icon: 'fa fa-link' },
        { name: "ANALYTICS", icon: 'fa fa-chart-bar' },
        { name: "", icon: 'fas fa-ellipsis-h' }
    ]
    // More icon dropdown lists item variable
    const moreNavList = [
        { name: "Edit", icon: 'fa fa-share-alt' },
        { name: "Add a collection", icon: 'fa fa-download' },
        { name: "Clone Assets", icon: 'fa fa-link' },
        { name: "Edit Image", icon: 'fa fa-chart-bar' }
    ]
    //Render component by a dynamic function based on doc type as parameter
    //Based on the switch case, Based on the asset doc type, 
    //Video Player component for Video
    //PDF Viewer component for PDF files 
    //CollectionDoc Editor for Docx file 
    //Draggable Core component for Image type
    const switchContent = (item) => {
        switch (item) {
            case 'PDF':
                return (
                    <PdfViewerComponent id="container"
                        documentPath="PDF_Succinctly.pdf"
                        serviceUrl="https://ej2services.syncfusion.com/production/web-services/api/pdfviewer"
                        style={{ 'height': '640px' }}>
                        <Inject services={[Toolbar, Magnification, Navigation, LinkAnnotation,
                            BookmarkView, ThumbnailView, Print, TextSelection, TextSearch, Annotation,
                            FormFields]} />
                    </PdfViewerComponent>
                )
            case 'IMAGE':
                return (<>
                    <DraggableCore

                    >
                        <div>
                            <img src={img_asset} className="handle"
                                draggable="true" width="300px" className="mt2" alt="content" />

                        </div>
                    </DraggableCore>

                </>)
            case 'VIDEO':
                return (
                    <ReactPlayer style={{ padding: '3em 2em' }} playing={true}
                        controls={true} width="90%" url={yt} />
                )
            case 'DOC':
                return (
                    <CollectionDocEditor />
                )
            default:
                return "Loading ..."
        }
    }
    //Handle Wide width function for rendering the side panel for more width 
    const handleWideWidth = () => {
        setIsWideScreen(true)
    }
    //Handle close template which close the panel by reducing width by a state variable
    const handleCloseTemplate = () => {
        setIsWideScreen(false)
    }

    // Content method which will render based on the state variable for multiple collection tool items
    // For Generate Link, Compose Mail, Analytics,Add to Collection, Clone Asset, Edit assets Panel 
    const content = () => {
        return (
            showGenerateLinkPanel ? <GenerateLink handleWideWidth={() => handleWideWidth()} handleCloseTemplate={() => handleCloseTemplate()} /> :
                showComposePanel ? <Compose handleWideWidth={() => handleWideWidth()} handleCloseTemplate={() => handleCloseTemplate()} /> :
                    showAnalyticsPanel ? <AnalyticsPanel /> :
                        showAddtoCollectionPanel ? <AddToCollectionPanel /> :
                            showCloneAssetsPanel ? <CloneAssetsPanel /> :
                                showEditAssetsPanel ? <EditAssets /> : ''
        )
    }

    const send = (data) => {
        const email_data = {
            toAddress: [JSON.parse(localStorage.getItem('email'))],
            subject: localStorage.getItem('subject'),
            message:data,
            assets:["asset_id"],
            canDownload:true,
            canReshare:true,
            liveTalk:false,
            downloadEditable:true,
            password:null,
            expiryDate:null,
        }
        console.log(email_data);
    }

    //close the sidebar panel
    const closeClick = () => {
        let body = document.getElementsByTagName('body')
        body[0].className = body[0].className.replace(' sidecontentnav')
        // document.getElementById('sidebar-menu').style.display = 'block'
        setShowgeneratelinkpanel(false)
        setShowComposepanel(false)
        setShowAnalyticsPanel(false)
        setIsWideScreen(false)
        setShowEditAssetsPanel(false)
        setShowCloneAssetsPanel(false)
        setShowAddtoCollectionPanel(false)
    }
    // Open the sidebar panel  based on the switch case it will render specific panel
    const openClick = (item, type) => {
        let body = document.getElementsByTagName('body')
        body[0].className = body[0].className + ' sidecontentnav';
        console.log(type)
        switch (type) {
            case 0:
                setShowComposepanel(true)
                break;
            case 1:
                // download;
                break
            case 2:
                setShowgeneratelinkpanel(true)
                break;
            case 3:
                // document.getElementById('sidebar-menu').style.display = 'none'
                setShowAnalyticsPanel(true)
                break;
            case 4:
                setShowAddtoCollectionPanel(true)
                break;
            case 5:
                setShowCloneAssetsPanel(true)
                break;

            default:
                // this.setState({ sidecontent: true })
                break;
        }
    }
    //Open more action event
    const openMoreOptionPanelClick = (item, type) => {
        // this.sidebarobj.show();
        if (type !== 3) {
            let body = document.getElementsByTagName('body')
            body[0].className = body[0].className + ' sidecontentnav';
        }
        console.log(type)
        switch (type) {
            case 0:
                setShowEditAssetsPanel(true)
                break;
            case 1:
                setShowAddtoCollectionPanel(true)
                break
            case 2:
                setShowCloneAssetsPanel(true)
                break;
            case 3:
                window.open(`${window.location.origin}/assets/image/6142e72fd28b5d6f704dfa82/edit`)
                break;
            default:
                break;
        }
    }
    //Toggle Event for more option when on click the dropdown
    const openMoreOptionClick = () => {
        setOpenMoreOption(!openMoreOption)
    }
    return (
        //This is an asset item
        <React.Fragment>
            <div className="_collectionpdf_container ">
                <div className="_collectionpdf_header">
                    <div className="_collectionpdf_pdfName">
                        <p className="title">
                            (sample)Tribez Support Center</p>
                        <p className="desc">151.94 | PDF | Tribez Cloud</p>
                    </div>
                    <div className="_collectionPdf_navList">
                        {navList.map((item, i) => {
                            if (String(item.name).toLowerCase() == 'download') {
                                return (<>
                                    <p><i class={item.icon + ' collection-icons'}></i><a href={downloadLink} target="_blank">{item.name}</a></p>
                                </>)
                            } else if (String(item.name).toLowerCase() == '') {
                                return (<>
                                    <p onClick={() => openMoreOptionClick(item, i)}><i class={item.icon + ' collection-icons'}></i>{item.name}</p>
                                </>)
                            }
                            else {
                                return (<>
                                    <p onClick={() => openClick(item, i)}><i class={item.icon + ' collection-icons'}></i>{item.name}</p>
                                </>)
                            }
                        })}
                        {openMoreOption ? <div className="more-option-wrap">{moreNavList.map((item, i) => {
                            return (<>
                                <p onClick={() => openMoreOptionPanelClick(item, i)}><i class={item.icon + ' collection-icons'} style={{ textTransform: "uppercase" }}></i>{item.name}</p>
                            </>)
                        })}</div> : null}
                    </div>
                </div>
                <div className="__collectionPdf_Main_wrap">{switchContent(type)}</div>
            </div>
            {showGenerateLinkPanel ?
                <div className={`sidecontentnav templatepreview  ${isWideScreen ? 'share-collection-wrap-wide' : 'share-collection-wrap'}`}>
                    <SidePreviewPanel content={content()} action={false} closeClick={() => closeClick()} /></div> : null}
            {/* {showComposePanel ? <SidePreviewPanel content={content()} action={false} closeClick={() => closeClick()} /> : null} */}
            {showComposePanel ?
                <div className={`sidecontentnav templatepreview ${isWideScreen ? 'share-collection-wrap-wide' : 'share-collection-wrap'}`}>
                    <SidePreviewPanel
                        content={content()}
                        action={true}
                        submitBtn={"Send"}
                        cancelBtn={"Cancel"}
                        cancelClick={() => closeClick()}
                        closeClick={() => closeClick()}
                        submitClick = {() => send(assetsData[0].body)}
                        isPad={true} />
                </div> : null}
            {showAddtoCollectionPanel && <SidePreviewPanel content={content()} action={true} submitBtn={"Add"} cancelBtn={"Cancel"} cancelClick={() => closeClick()} closeClick={() => closeClick()} />}
            {showCloneAssetsPanel && <SidePreviewPanel content={content()} action={true} submitBtn={"Clone"} cancelBtn={"Cancel"} cancelClick={() => closeClick()} closeClick={() => closeClick()} />}
            {showAnalyticsPanel ? <AnalyticsPanel assetData={assetData} content={content()} action={false} closeClick={() => closeClick()} /> : null}
            {showEditAssetsPanel ? <SidePreviewPanel assetData={assetData} content={content()} action={true} submitBtn={"Send"} cancelBtn={"Cancel"}cancelClick={() => closeClick()} closeClick={() => closeClick()} /> : null}
        </React.Fragment>

    )
}

export default withRouter(CollectionPdf)
