import { ColumnDirective, ColumnsDirective, GridComponent } from '@syncfusion/ej2-react-grids';
import { TextBoxComponent } from '@syncfusion/ej2-react-inputs';
import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import AddAssetCollection from '../CollectionItem/AddAssetCollection';
import SidePreviewPanel from '../sidePreviewPanel';
import "./EmailTemplateCreate.scss";
import "../CollectionItem/previewNavItem.scss";
import "../hub/index.scss"
import { SwitchComponent } from '@syncfusion/ej2-react-buttons';
import { RichTextEditorComponent, Toolbar, Inject, Image, Link, HtmlEditor, Count, QuickToolbar, Table } from '@syncfusion/ej2-react-richtexteditor';
import { ToolbarSettingsModel, FileManager, FileManagerSettingsModel, QuickToolbarSettingsModel } from '@syncfusion/ej2-react-richtexteditor';


const EmailTemplateCreate = (props) => {
    const history = useHistory()
    const [showAddAssetCollection, setshowAddAssetCollection] = useState(false)

    const handleBack = () => {
        history.goBack()
    }
    const dsSource = [{ name: 'Untitled', owner: 'Karthi' }]
    const transactionTemplate = (data) => {
        return data.name
    }
    const comTemplate = (data) => {
        return data.owner
    }
    const handleAddAsset = () => {
        let body = document.getElementsByTagName('body')
        body[0].className = body[0].className + ' sidecontentnav';
        setshowAddAssetCollection(true)
    }
    const closeClick = () => {
        let body = document.getElementsByTagName('body')
        body[0].className = body[0].className.replace(' sidecontentnav')
        setshowAddAssetCollection(false)
    }
    const content = () => {
        return (
            <AddAssetCollection />
        )
    }
    return (<>
        <div className="_create-temp-main">
            <div className="_temp-name-wrap">
                <p className="back-btn" onClick={handleBack}><i className="fa fa-chevron-left" style={{ fontSize: '12px', paddingRight: '5px' }}></i>{` Back`}</p>
                <p className="title" data-testid="title">Email Templates</p>
                <TextBoxComponent
                    cssClass="e-outline" floatLabelType="Auto"
                    type="text" placeholder="Search Email Templates"
                    data-testid="email-template-input"
                    //  onChange={(e) => { this.setState({ searchUser: e.target.value }) }}
                    value={''}
                />
                <div className="owner-wrap">
                    <GridComponent dataSource={dsSource}>
                        <ColumnsDirective>
                            <ColumnDirective field='name' headerText='Template Name' textAlign="left" template={transactionTemplate}></ColumnDirective>
                            <ColumnDirective field='isRead' headerText='Owner' textAlign="left" template={comTemplate}></ColumnDirective>
                        </ColumnsDirective>
                    </GridComponent>
                </div>
            </div>
            <div className="_temp-create-wrap">
                <p className="add-title" data-testid="add-asset-btn" onClick={handleAddAsset}>+Add Asset</p>
                <TextBoxComponent
                    cssClass="e-outline" floatLabelType="Auto"
                    type="text" placeholder="Template Name"
                    //  onChange={(e) => { this.setState({ searchUser: e.target.value }) }}
                    value={''}
                />
                <div className="default-settings">
                    <div className="public-wrap">
                        <p className="title">SET AS PUBLIC</p>
                        <SwitchComponent id="checked" checked={false}></SwitchComponent>
                    </div>
                    <div className="public-wrap">
                        <p className="title">SET AS DEFAULT</p>
                        <SwitchComponent id="checked" checked={false}></SwitchComponent>
                    </div>
                </div>
                <TextBoxComponent
                    cssClass="e-outline" floatLabelType="Auto"
                    type="text" placeholder="Email Subject"
                    data-testid="mailsubject"
                    //  onChange={(e) => { this.setState({ searchUser: e.target.value }) }}
                    value={''}
                />
                <div className="editor-wrap">
                    <p className="title">Email Content:</p>
                    <RichTextEditorComponent id="toolsRTE"
                        showCharCount={false}
                    >
                        {/* <>
                            Hello there,<br />
                            Here's a collection of assets exclusively personalized for you! Hope you find this material useful.<br />
                            I'd love to hear what you think about them. Please feel free to get in touch at any time.
                        </> */}
                        <Inject
                            services={[Toolbar, Image, Link, HtmlEditor, Count, QuickToolbar, Table, FileManager]} />
                    </RichTextEditorComponent>
                </div>
                <div className="action-wrap">
                    <p className="create">Create</p>
                    <p className="cancel" data-testid="cancel-temp-btn" onClick={handleBack}>Cancel</p>
                </div>
            </div>
        </div>
        <div className="templatepreview addAsset-collection-wrap">
            {showAddAssetCollection ?
                <SidePreviewPanel content={content()} data-testid="close-sidenav" action={false} closeClick={() => closeClick()} /> : null}
        </div>
    </>
    )
}
export default EmailTemplateCreate