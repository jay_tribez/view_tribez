import React from 'react';
import { mount, shallow } from 'enzyme';
import EmailTemplateCreate from "./EmailTemplateCreate";

import { render, screen, fireEvent } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";

describe('<EmailTemplateCreate />', () => {
    it('should render EmailTemplateCreate component correctly', async () => {
        render(
            <MemoryRouter>
                <EmailTemplateCreate />
            </MemoryRouter>
        );
        expect(screen.getByTestId('title').textContent).toBe('Email Templates')
        // screen.getByTestId('subtitle')
        // expect(toJson(wrapper)).toMatchSnapshot();
    })
    it('On clicking cancel button', async () => {
        render(
            <MemoryRouter>
                <EmailTemplateCreate />
            </MemoryRouter>
        );
        fireEvent.click((screen.getByTestId('cancel-temp-btn')))
    })
    it('On clicking add assets button', async () => {
        render(
            <MemoryRouter>
                <EmailTemplateCreate />
            </MemoryRouter>
        );
        fireEvent.click((screen.getByTestId('add-asset-btn')))
    })

    it('On clicking close button of sidenav panel should be closed', async () => {
        render(
            <MemoryRouter>
                <EmailTemplateCreate />
            </MemoryRouter>
        );
        // expect(screen.getByTestId('close-sidenav'))
    })
    it("on change the mail subject input field", async () => {
        let wrapper = render(
            <MemoryRouter>
                <EmailTemplateCreate />
            </MemoryRouter>
        );
        // console.log('screen', screen.getByTestId('mailsubject'))
        fireEvent.change(screen.getByTestId('mailsubject'), { target: { value: "" } })
        fireEvent.change(screen.getByTestId('mailsubject'), { target: { value: "Tribez@123" } })
        // fireEvent.click(screen.getByTestId('getlink-btn'))
    })
    it("on change the mail template input field", async () => {
        let wrapper = render(
            <MemoryRouter>
                <EmailTemplateCreate />
            </MemoryRouter>
        );
        // console.log('screen', screen.getByTestId('mailsubject'))
        fireEvent.change(screen.getByTestId('email-template-input'), { target: { value: "" } })
        fireEvent.change(screen.getByTestId('email-template-input'), { target: { value: "Tribez@123" } })
        // fireEvent.click(screen.getByTestId('getlink-btn'))
    })


})