import React, { useEffect, useState } from 'react';
import { withRouter } from 'react-router';
import "../CollectionItem/CollectionItem.scss";
import "../hub/index.scss";
import "../CollectionItem/previewNavItem.scss"
import axios from 'axios';
import { useHistory } from 'react-router-dom';
import collectionResponse from './view-tribez-collectionRes.json';
import DefaultProfilePic from '../../Assets/images/profile.jpg';
import { TabComponent } from '@syncfusion/ej2-react-navigations';
import ShareCollection from '../CollectionItem/ShareCollection';
import SidePreviewPanel from "../sidePreviewPanel";
import Compose from '../Compose';

import { RichTextEditorComponent, FileManager, Toolbar, Inject, Image, Link, HtmlEditor, Count, QuickToolbar, Table } from '@syncfusion/ej2-react-richtexteditor';
import CollectionItem from '../CollectionItem';
import loader from '../loader';

function CollectionPreview(props) {
    const [collectionId, setCollectionId] = useState("")
    const [env, setEnv] = useState(JSON.parse(localStorage.getItem("env")))
    const [collectionItemData, setCollectionItemData] = useState({})
    const [isLoading, setIsloading] = useState(true)
    const [showShareCollection, setShowShareCollection] = useState(false)
    const [isWideShareCollection, setIsWideShareCollection] = useState(false)
    const [isEditor, setIsEditor] = useState(false)
    const [isAssetTrue, setIsAssetTrue] = useState(true)
    const [recipientEmail, setRecipientEmail] = useState({})
    const [collectionAssets, setCollectionAssets] = useState([])
    const [showAddAssetCollection, setshowAddAssetCollection] = useState(false)

    const history = useHistory()
    const handlePdfClick = (item) => {
        history.push({ pathname: "/app/collectionItem", state: { detail: item } })
    }
    const sectionData = {
        sectionName: "(sample)Start your Paperflite journey here!",
        data: [
            {
                itemName: 'CleverStory Product Walkthrough',
                link: "/collection/614afd3029254e27d87f5c3b/s/614afd2f29254e27d87f5c38/a/60b0a7c7bf008d6a18838fdaa",
                imageUrl: "https://d2uav5q06z9nv6.cloudfront.net/5adebd333a038c2136412d80/images/60b0a7c99b6b19517f506316/24acbef5-531a-45fa-8a57-6f3fb92b4331"
            },
            {
                itemName: 'Centerline Digital 2021 B2B Content Consumption Insights Report',
                link: "/collection/614afd3029254e27d87f5c3b/s/614afd2f29254e27d87f5c38/a/60b0a7c7bf008d6a18838fdaa",
                imageUrl: "https://d2uav5q06z9nv6.cloudfront.net/5adebd333a038c2136412d80/images/60b0a7c99b6b19517f506316/24acbef5-531a-45fa-8a57-6f3fb92b4331"
            }

        ],
        sectionCount: 3
    }
    const sendMail = () => {
        console.log(recipientEmail)
        // closeAddAssetClick()
        let payload = {

        }
        // axios.post(`${env['apiUrl']}/collections/${collectionItemData.id}`, payload)
        //     .then(res => {
        //         console.log('response', res)
        //     }).catch(e => {
        //     })
    }
    useEffect(() => {
        let getCollectionId = props.linkResponse.entityId
        setCollectionId(getCollectionId);
        setCollectionItemData(collectionResponse);

        axios.get(`${env['apiUrl']}/collections/list/${collectionId}`)
            .then(res => {
                let collectionRes = res.data
                setIsloading(false)
                setCollectionItemData(collectionRes);
            }).catch((err => {
                setIsloading(false)
                if (collectionResponse.sectionsCount == 0) {
                    setIsAssetTrue(true)
                }
                else {
                    setIsAssetTrue(false)
                }
                setCollectionItemData(collectionResponse);
            }))
    }, [collectionItemData, collectionId])

    const handleShareCollection = () => {
        let body = document.getElementsByTagName('body')
        body[0].className = body[0].className + ' sidecontentnav';
        setShowShareCollection(true)
    }

    //Handle Wide width function for rendering the side panel for more width 
    const handleWideWidth = () => {
        setIsWideShareCollection(true)
    }
    //Handle close template which close the panel by reducing width by a state variable
    const handleCloseTemplate = () => {
        setIsWideShareCollection(false)
    }
    const handleDeleteClick = (item) => {

    }
    const closeAddAssetClick = () => {
        let body = document.getElementsByTagName('body')
        body[0].className = body[0].className.replace(' sidecontentnav')
        setshowAddAssetCollection(false)
        setShowShareCollection(false)
        setIsWideShareCollection(false)
    }
    //close the sidebar panel by removing a panel classname and panel Boolean value to false
    const closeClick = () => {
        let body = document.getElementsByTagName('body')
        body[0].className = body[0].className.replace(' sidecontentnav')
        setShowShareCollection(false)
    }

    const showcollection = (item, index) => {
        collectionItemData.sections.map((sectionItem, i) => {
            if (i == index && sectionItem.name == item.name) {
                setCollectionAssets(sectionItem)
            }
        })
    }

    const onSectionAssetsContent = () => {
        const sections = collectionItemData.sections
        // const assets = collectionItemData.assets
        return <div className="_collectionItem_cardsWrap">
            {/* {assets && sections.length == 0 ?
                <div className="_collectionItem_card_container"
                    style={{ display: 'flex', marginTop: '-40px', paddingLeft: '1em', }} >
                    {assets.map((item, i) => {
                        return tabContent(item, i)
                    })}
                </div> : null} */}
            {sections.length > 0 ?
                <div className="section-tab-container">
                    <div className="_collectionItem-tabs-wrapper">
                        <TabComponent id='defaultTab'>
                            <div className="e-tab-header">
                                {sections.map((item, index) => {
                                    return (
                                        <div key={index} style={{ padding: 10 }} onClick={() => { showcollection(item, index) }}>{item.name}
                                        </div>
                                    )
                                })}
                            </div>
                            <div className="tabcontent e-content">
                                {sections.map((item) => {
                                    return (item.assets.length > 0 ?
                                        item.assets.map((asset, i) => {
                                            return tabContent(asset, i)
                                        })
                                        : null)
                                })}
                            </div>
                        </TabComponent>
                    </div>
                </div> : null}
        </div>
    }
    const tabContent = (item, i) => {
        return (<React.Fragment key={i}>
            <div className="e-card" key={'card' + i} data-testid={`card-item${i + 1}`} style={{ padding: '0.7em', float: 'left', width: '250px', marginRight: '20px', cursor: 'pointer' }}
            // onClick={() => handlePdfClick(item)}>
            >
                <div className="e-card-image" style={{
                    background: `url(${item.icon != null ? item.icon.thumbnail : null})`,
                    backgroundSize: "cover", backgroundRepeat: 'no-repeat', height: '56vh'
                }}>
                </div>
                <div className="e-card-title title"
                    style={{
                        fontWeight: '400',
                        fontSize: '.875em',
                        color: '#212529',
                        textAlign: 'left',
                        paddingLeft: '0px',
                        paddingBottom: '0px'
                    }}
                >{item.name} </div>
                <div className="dflex" style={{ display: 'flex', justifyContent: 'flex-start' }}>
                    <p className="__collectionItem_cards_type">{item.type}</p>
                    <p className="__collectionItem_cards_size">{item.size}</p>
                </div>
            </div>
        </React.Fragment>)
    }
    const bytesToSize = (bytes) => {
        var sizes = ['', 'KB', 'MB', 'GB', 'TB'];
        if (bytes == 0) return '0 Byte';
        var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
        return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
    }
    const content = () => {
        return (
            showShareCollection ?
                // <Compose collectionItemData={collectionItemData}
                //     handleWideWidth={() => handleWideWidth()}
                //     onRecipientEmail={onRecipientEmail}
                //     closeClick={() => closeAddAssetClick()}
                //     handleCloseTemplate={() => handleCloseTemplate()}
                //     data-testid="compose">
                // </Compose> 
                <ShareCollection
                    hideTools={true}
                    hideDefault={true}
                    showViewContent={true}
                    sectionData={sectionData}
                    handleWideWidth={handleWideWidth}
                    handleCloseTemplate={handleCloseTemplate} />
                : '')
    }
    const onRecipientEmail = (email) => {
        setRecipientEmail(email)
    }
    const sections = collectionItemData.sections
    // const assets = collectionItemData.assets
    return (
        <div className="_collectionItem_container -collection-section-viewwrap" data-testid="container" style={{ width: '100%' }} >
            {isLoading ?
                <React.Fragment>
                    <loader />
                </React.Fragment>
                :
                <React.Fragment>
                    <div className="_collectionItem_img_container " style={{ height: '50vh' }}>
                        <div className="_collectionItem_texture" style={{ height: '50vh' }}>
                            {!isEditor ?
                                <>
                                    <div className="_collectionItem_header">
                                        <div className="_collectionItem_navPanel">
                                        </div>
                                    </div>
                                    <div className="_collectionItem_contentBlock" style={{ paddingLeft: '40px' }}>
                                        <div className="dFlexSB">
                                            <p className="title" style={{ fontSize: '2.5rem' }}>{collectionItemData.name}</p>
                                            <div className="share-collection" onClick={handleShareCollection}>
                                                <span>Share <br />Collection</span>
                                                <span className="shareIconWrap">
                                                    <i class="fas fa-share-alt"></i></span>
                                            </div>
                                        </div>
                                        <div className="dFlexSB">
                                            <div dangerouslySetInnerHTML={{ __html: collectionItemData.template }}></div>
                                            <div className="sharedby-wrap" style={{ marginTop: '110px' }}>
                                                <p className="shredBy">Shared By</p>
                                                <img src={DefaultProfilePic} style={{ borderRadius: '50%', marginTop: '-10px' }} alt="profile" className="default-profile-picture"></img>
                                                <p className="f13">{collectionItemData.createdBy.firstName + ' ' + collectionItemData.createdBy.lastName}</p>
                                                <p className="f13">{collectionItemData.createdBy.email}</p>
                                            </div>
                                        </div>
                                        <div className="schedule-btn-wrap" style={{ marginRight: '30px', position: 'relative', marginTop: '-20px', float: 'right' }}>
                                            <span className="schedule-btn" data-testid="schedule-meet-btn" onClick={() => { history.push('/calendso/followup') }}>
                                                <p>Schedule a Meet</p>
                                            </span>
                                        </div>
                                    </div>
                                </> :
                                <>

                                    <RichTextEditorComponent id="toolsRTE"
                                        //  ref={(richtexteditor) => { this.rteObj = richtexteditor }}
                                        showCharCount={true}
                                    // actionBegin={this.handleFullScreen.bind(this)}
                                    // actionComplete={this.actionCompleteHandler.bind(this)} maxLength={2000} 
                                    // toolbarSettings={this.toolbarSettings}
                                    // fileManagerSettings={this.fileManagerSettings} 
                                    // quickToolbarSettings={this.quickToolbarSettings}
                                    >
                                        <>
                                            <div className="_collectionItem_contentBlock">
                                                <div className="dFlexSB">
                                                    <p className="title">{collectionItemData.name}</p>
                                                </div>
                                                <div className="dFlexSB"    >
                                                    <p className="desc">{`Click here for your personal copy of ${collectionItemData.assetsCount} documents with reference to our discussion. Hope you will find them useful.`}</p>
                                                </div>
                                            </div>

                                            <div className="_collectionItem_cardsWrap">

                                                {collectionItemData.assets.length > 0 ? <div className="_collectionItem_card_container"
                                                    style={{ display: 'flex', paddingLeft: '1em', }} >
                                                    {collectionItemData.assets.map((item, i) => {
                                                        return (
                                                            <>
                                                                <div className="e-card" key={i}
                                                                    style={{ padding: '0.7em', width: '250px', marginRight: '20px', cursor: 'pointer' }}
                                                                    onClick={() => handleDeleteClick(item)}
                                                                >
                                                                    <div >
                                                                        <i
                                                                            className="fas fa-times-circle close-icon-cross remove-click"></i>
                                                                    </div>
                                                                    <div className="e-card-image" style={{
                                                                        // background: `url(${item.icon})`,
                                                                        background: `url(${item.icon != null ? item.icon.thumbnail : null})`,
                                                                        backgroundSize: "cover", backgroundRepeat: 'no-repeat', height: '56vh'
                                                                    }}>
                                                                    </div>
                                                                </div>

                                                            </>)
                                                    })}
                                                </div> : "Fetching"}
                                            </div>
                                        </>
                                        <Inject
                                            services={[Toolbar, Image, Link, HtmlEditor, Count, QuickToolbar, Table, FileManager]} />
                                    </RichTextEditorComponent>
                                    <p className="cancel-btn" onClick={() => setIsEditor(false)}>Cancel</p>
                                </>

                            }
                        </div>
                    </div>
                    {isAssetTrue ?
                        <React.Fragment>
                            <div className="collection-assets-lists">
                                <div className="tabcontent e-content">
                                    <div className="tab-content-wrapper" id="tab-content-wrapper1" data-testid="content1" >
                                        {collectionItemData.assets && collectionItemData.assets.map((item, i) => {
                                            return (<div className="card-item-preview" key={i} >
                                                <div className="card-item-content" onClick={() => handlePdfClick(item)}>
                                                    <div className="image-content">
                                                        <img width={'100%'} alt="" src={item.icon != null ? item.icon.thumbnail : null}></img>
                                                    </div>
                                                    <div className="content-itemname">
                                                        <p>{item.name}</p>
                                                    </div>
                                                </div>
                                            </div>)
                                        })}
                                    </div>
                                </div>
                            </div>
                        </React.Fragment>
                        :
                        onSectionAssetsContent()
                    }
                    {/* <React.Fragment>
                            <div className="_collectionItem_cardsWrap">
                                {sections.length > 0 ?
                                    <div className="section-tab-container">
                                        <div className="_collectionItem-tabs-wrapper">
                                            <TabComponent id='defaultTab'>
                                                <div className="e-tab-header">
                                                    {sections.map((item, index) => {
                                                        return (
                                                            <div key={index} style={{ padding: 10 }} onClick={() => { showcollection(item, index) }}>{item.name}
                                                            </div>
                                                        )
                                                    })}
                                                </div>
                                                <div className="tabcontent e-content">
                                                    {collectionAssets.assets && collectionAssets.assets.length > 0 ?
                                                        collectionAssets.assets.map((asset, i) => {
                                                            return tabContent(asset, i)
                                                        })
                                                        : null}
                                                </div>
                                            </TabComponent>
                                        </div>
                                    </div> : null}
                            </div>
                        </React.Fragment>
                    } */}
                </React.Fragment>}
            <div className={`templatepreview ${showShareCollection ? (isWideShareCollection ? 'share-collection-wrap-wide' : 'compose-collection-panel share-collection-wrap') : ''}`}>
                {showShareCollection ?
                    <SidePreviewPanel content={content()}
                        action={true}
                        submitBtn={"Send"}
                        cancelBtn={"Cancel"}
                        submitClick={() => sendMail()}
                        cancelClick={() => closeClick()}
                        closeClick={() => closeClick()}
                        isPad={true}
                    /> : null}
            </div>

        </div >
    )
}

export default withRouter(CollectionPreview)