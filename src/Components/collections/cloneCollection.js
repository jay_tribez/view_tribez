import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { TextBoxComponent } from '@syncfusion/ej2-react-inputs';
import defaultImage from '../../Assets/images/stream_loading_icon.jpg';

class CloneCollection extends Component {
    constructor(props) {
        super(props);
        console.log(props)
    }
    render() {
        return (
            <div className='control-pane'>
                <div className='control-section' id='uploadpreview'>
                    <div className='imagepreview'>
                        <h3 className="cs-hd">Cloning Collection</h3>
                        <div className="clone-icon">
                            <img src={this.props.cloneCollectionDetails.icon != null ? this.props.cloneCollectionDetails.icon.thumbnail : defaultImage} />
                        </div>
                        <p className="clone-title">{this.props.cloneCollectionDetails.name}</p>
                    </div>
                    <div className="clone-form">
                        <TextBoxComponent floatLabelType="auto"
                            style={{ width: '100%', paddingTop: '1em' }} type="text"
                            placeholder="Add a collection name"
                            value={''} />
                        <p>Get a head-start at building collections.</p><br />
                        <p>Cloning will create an exact copy of this collection so you can make finer edits and start sharing right away.</p>
                    </div>
                </div>
            </div>);
    }
}
export default withRouter(CloneCollection);