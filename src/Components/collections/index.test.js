import React from 'react';
import { render, screen, fireEvent } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";
import Collections from "./index";
// import { TextBoxComponent } from '@syncfusion/ej2-react-inputs';
// import { ButtonComponent } from '@syncfusion/ej2-react-buttons';
// import { ChipListComponent, ChipsDirective, ChipDirective } from '@syncfusion/ej2-react-buttons';

describe('collection component is tesing', () => {
    it('on render', async () => {
        let hubWrapper = render(
            <MemoryRouter>
                <Collections />
            </MemoryRouter>
        );
    })
    it('on rename', async () => {
        let hubWrapper = render(
            <MemoryRouter>
                <Collections />
            </MemoryRouter>
        );
        fireEvent.click(screen.getByText("rename"))

    })
    it('on delete', async () => {
        let hubWrapper = render(
            <MemoryRouter>
                <Collections />
            </MemoryRouter>
        );
        fireEvent.click(screen.getByText("delete"))

    })
    
    it('on clone', async () => {
        let hubWrapper = render(
            <MemoryRouter>
                <Collections />
            </MemoryRouter>
        );
        fireEvent.click(screen.getByText("clone"))

    })
})