import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { CheckBoxComponent } from '@syncfusion/ej2-react-buttons';
import defaultImage from '../../Assets/images/stream_loading_icon.jpg';
class DeleteCollection extends Component {
    constructor(props) {
        super(props);
        console.log(props)
    }
    onChangeDeleteall = () => {

    }
    render() {
        return (
            <div className='control-pane'>
                <div className='control-section' id='uploadpreview'>
                    <div className='imagepreview'>
                        <h3 className="cs-hd">Deleting</h3>
                        {this.props.details.type == "asset" &&
                            <p className="count">{this.props.details.records.length} Asset(s)</p>
                        }
                        {this.props.details.type == "collection" &&
                            <>  <p className="count">{this.props.details.noOfCollections} Collection(s)</p>
                                <p className="delete-title">Are you sure you would like to delete the selected collections?</p>
                                <div className="collection-wrapper">{this.props.details.collectionRecords.map((item, index) => {
                                    return (
                                        <div className="individual-collection" key={index}>
                                            <img className="delete-collection-img" src={item.icon !== null ? item.icon.thumbnail : defaultImage} alt="add" width={30} height={35} />
                                            <p className="delete-collection-name">{item.name}</p>
                                        </div>
                                    )
                                })}
                                </div>
                            </>
                        }
                        {this.props.details.type == "folder" &&
                            <><p className="count">{this.props.details && this.props.details.name} folder</p>
                                <p className="delete-title"><CheckBoxComponent label="Delete all collections in the folder?" onChange={this.onChangeDeleteall} /></p>
                            </>
                        }

                        {/* <p>{this.props.deleteDetails.type == "collection_lists" ? '' : ''}</p> */}
                        {/* <div className="clone-icon">
                            <img src={this.props.cloneCollectionDetails.src} />
                    </div> */}
                        {/* <p className="clone-title">{this.props.deleteDetails.data.length}</p> */}
                    </div>
                </div>
            </div>);
    }
}
export default withRouter(DeleteCollection);