import React, { Component } from 'react';
import { enableRipple } from '@syncfusion/ej2-base';
import { withRouter } from 'react-router-dom';
import "./collection.scss";
import axios from "axios";
import "../hub/index.scss";
// import "../CollectionItem/previewNavItem.scss";
import { TextBoxComponent } from '@syncfusion/ej2-react-inputs';
import { ButtonComponent } from '@syncfusion/ej2-react-buttons';
import { ChipListComponent, ChipsDirective, ChipDirective } from '@syncfusion/ej2-react-buttons';
import PageTitle from '../Common/PageTitle';
import { CheckBoxComponent } from '@syncfusion/ej2-react-buttons';
import ProfileHeader from '../Common/profileHeader/profileHeader';
import SidePreviewPanel from "../sidePreviewPanel";
import CloneCollection from "./cloneCollection";
import DeleteCollection from "./deleteCollection";
import defaultImage from '../../Assets/images/stream_loading_icon.jpg';
import allCollectionResponse from './allcollection.json';
import CreateNewCollection from '../CreateNewCollection/createNewCollection';
import UserAlertInformation from "../Common/validation/alertInformation";
import TribezService from "../../utils/apiService"

enableRipple(true);

class Collections extends Component {
    constructor(props) {
        super(props);
        this.TribezService = new TribezService()
        this.state = {
            env: JSON.parse(localStorage.getItem("env")),
            searchValue: '',
            newCollectionName: '',
            uncategorizedImage: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEASABIAAD/2wBDABALDA4MChAODQ4SERATGCgaGBYWGDEjJR0oOjM9PDkzODdASFxOQERXRTc4UG1RV19iZ2hnPk1xeXBkeFxlZ2P/2wBDARESEhgVGC8aGi9jQjhCY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2P/wAARCAFeASwDAREAAhEBAxEB/8QAGwABAAMBAQEBAAAAAAAAAAAAAAUGBwQCAQP/xAA0EAEAAgICAAIFCQkBAAAAAAAAAQIDBAURBhIHEyExcRQXMlVhgZHB4RUWQVRlkpOhoyL/xAAUAQEAAAAAAAAAAAAAAAAAAAAA/8QAFBEBAAAAAAAAAAAAAAAAAAAAAP/aAAwDAQACEQMRAD8A0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFZ5jxnx3FbNtaa5M+Wv0ox9dV+zsEhwfP6fOYrW1bWi9Pp47x1MAlgfAVXkvHXG6O3bXpTLsTWerWp11E/mCa4jmNTmdX5Rp3mYierVtHU1n7QSIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPzy3jHivefdWszIMK2s07G1mzWnub3m34yC3+jHDa3J7ebufLTFFfjMz+gNKBxcvn+S8Tt5++vJitP8AoGG2mbWmZ98g0P0YYbRr72ae/La1ax93YL4AAAAAAAAAAAAAAAAAAAAAAAAAAAD4Cpcl484/S2r4MeLJsTSerWrMRHf2A5PnJ1P5DN/fAOfkPSBg2tDPr4tLLS+XHNItNo9ncAoALP4T8Ta/AYNiuTWvlvltE91tEdRALB85Op/IZv74BH8744w8pxOfTw6mTFbLER5rWiYiOwUgFv8ADHi7W4PjZ1b6mTLebzebVtEAmPnJ1P5DN/fAJbg/GOhzGxGtFb4M9vo1v7rfCQWMAAAAAAAAAAAAAAAAAAAAAAAAAHjJXz47U7680THYMg5TwryunuZKV1MubH5p8uTHXzRMA4v2Fyv1ds/45B+Gzx25p0i2zq5cNbT1E3pMdg5Qd+Ph+SzY65MejsXpaO4tGOZiYB6/YXK/V2z/AI5Bz7WjtaU1ja18mGbe7z1mOwcwJCvCcpasWrx+zMT7Yn1cg+/sLlfq7Z/xyCe8JeGeSnmNfb2MF9fDgvF5m8dTbr+EQDUQAAAAAAAAAAAAAAAAAAAAAAAAAAAAZ56T9nvJpa0T7oteY/0Ci0rN71rHvmeoBumhgjW0NfBEderx1r+EA6QZd6Sdn1vO4sET7MOKPxn2grXG4J2eR1sER36zJWv+wbpWsVpFY90R0D0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAADJfSBs+v8TZaRPcYaVp/rv8wRfh7X+V87pYOu4tlr38InsG2gAxnxbs/KvEu9eJ7iMnkj7vYD9/BGv8p8T6vcdxj7yT90A2AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHyfZAMQ53Z+V81u5++4vlt18OwTHo91vX+JKXmPZhx2v+X5g1gH5Z8kYcOTJPupWbT90AwnYyzn2cuWffe82n75BcfRlrefktrYmPZjxxWJ+2Z/QGlAAAAAAAAAAAAAAAAAAAAAAAAAAAAAznxd4t38PKZdHRyTr0wz5bWiP/AFaQV396uc+ss/4gT4o5uYmJ5HP1P2giZmbTMzPcz7ZB06HI7nG5LZNPPfDe0dTNf4wDt/ennPrLP+IPOXxJzGbFbHk5DNal46tEz74BEg7dDlt7jYvGls3wxf6Xln3g6/3p5z6yz/iDq4/xnzGrsUvl2rbGPv8A9Uye3uAaxr5Yz6+PNWJiMlYtET9sA/YAAAAAAAAAAAAAAAAAAAAAAAAAFZ8Q+DtXmdj5VTLbXzzHVpiO4t8YBC/Np/Uv+X6gjOf8GRwvGX3Lb3rPLaKxTyddzP3gqQLfwPgq3M8Zj3J3PU+eZiK+Tv2RPxBIfNp/Uv8Al+oIfxN4UrwGnizTueutkv5Yr5Ov4fEFYBdeL8A25DjsG3be9VOakW8nq++v9g6vm0/qX/L9QdXH+jzWwbFcu3tWz0rPfkivlifiC61rFaxWsdREdRAPQAAAAAAAAAAAAAAAAAAAAAAAAAAAKT6Tc804vVwR7smWZn7o/UGag2zw5rfJOA0sPXUxiiZ+M+38wSgM89J+efXaWv7eora/5AouKk5MtKR77TEQDddLDGvpYMMR1GPHWv4QDoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABG81w+rzWn8n2onqJ7revvrIK9pej3R19muXPs5NilZ7jHNYrE/EFxrEVrERHUR7IgHoERz/AIf1edwVpnm1MlPoZK++ARHE+BNHjtymzlz32LY57rW1YrET+YLaD6AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD/9k=',
            collections: [
                { "name": "All Collections", default: true },
                { "name": "Uncategorized", default: true },
                { "name": "Collaborated", default: true },
                { "name": "Live", default: true },
                { "name": "Templates", default: true }
            ],
            cardsCollection: [],
            cloneCollectionItem: [],
            showAddPanel: false,
            isDelete: false,
            hideToolbar: false,
            renameClicked: false,
            deleteClicked: false,
            selectedChip: 0,
            showEmptyPanel: false,
            selectedCollectionName: '',
            render: true,
            isClone: false,
            isClonePanel: false,
            isDeletePanel: false,
            cloneCollectionDetails: {},
            moreNavList: [
                { name: "Name", value: "name" },
                { name: "Created", value: "createdDate" },
                { name: "Modified", value: "modifiedDate" }
            ],
            sortItem: 'Created',
            page: 0,
            limit: 40,
            deleteItems: [],
            userDetails: JSON.parse(localStorage.getItem('whoami')),
            createCollectionPanel: false,
            userInfoShow: false,
            userInfo: {},
            folderDelete: undefined
        }
        this.chipRef = React.createRef(this.chipRef)
    }
    componentDidMount() {//We want load list of folders and collection datas initially
        this.getFolders()        //list of folders
        // this.onFilter("createdDate") // list of collections
    }
    getFolders = async () => { // get folder
        let data = await this.TribezService.apiService("Folder", "get", "apiUrl", "/collection/folder/list")
        console.log(data)
    }
    onFilter = (filter) => { // get collection
        // axios.get(`${this.state.env['apiUrl']}/collections?page=${this.state.page}&limit=${this.state.limit}&sortBy=${filter}&orderBy=DESC`)

        this.setState({
            cardsCollection: [
                {
                    "id": "1",
                    "name": "Sample Collection",
                    "icon": null
                }
            ]
        })
        axios.get(`${this.state.env['apiUrl']}/collections/list`)
            .then(res => {
                console.log('response', res)
                if (res.data.length > 0) {
                    this.onUserInfo(true, { status: "success", message: "Collection lists has been fetched successfully." })
                    this.setState({ cardsCollection: res.data, cloneCollectionItem: res.data })
                } else {
                    this.onUserInfo(true, { status: "success", message: "No collections" })
                    this.setState({ noData: true })
                }
            }).catch(e => {
                this.onUserInfo(true, { status: "failure", message: "Collection lists are not getting." })
                this.setState({
                    cardsCollection: [
                        {
                            "id": "1",
                            "name": "Sample Collection",
                            "icon": null
                        }
                    ]
                });
            })
    }
    addNewFolder = (e) => {
        if (e.target.textContent.includes("Add New Folder")) {
            this.setState({ newCollectionName: '', showAddPanel: true, showEmptyPanel: false })
        }
        // else if (!e.target.textContent.includes("Add New Folder") && !e.target.textContent.includes("All Collections")) {
        //     this.setState({ showEmptyPanel: true, showAddPanel: false, newCollectionName: '', selectedCollectionName: e.target.textContent })
        // }
        else if (e.target.textContent.includes("All Collections")) {
            this.setState({
                showEmptyPanel: false,
                showAddPanel: false,
                newCollectionName: '',
                selectedCollectionName: e.target.textContent
            }, () => {
                this.onFilter('createdDate')
            })
        }
        else {
            this.setState({
                showEmptyPanel: false,
                showAddPanel: false,
                newCollectionName: '',
                selectedCollectionName: e.target.textContent
            })
            if (e.target.textContent) {
                this.setState({ selectedCollectionName: e.target.textContent })
                this.state.collections.filter(item => {
                    if (e.target.textContent == item.name) {
                        axios.get(`${this.state.env['apiUrl']}/collections?list=${String(item.name).toLowerCase()}&page=${this.state.page}&limit=${this.state.limit}&orderBy=DESC`)
                            .then(res => {
                                console.log('response', res)
                                if (res.data.length > 0) {
                                    this.setState({ showEmptyPanel: true, showAddPanel: false, newCollectionName: '', })
                                } else {
                                    this.setState({ showEmptyPanel: true })
                                }
                            }).catch(e => {
                                console.log('error', e)
                                this.setState({ showEmptyPanel: true, showAddPanel: false, newCollectionName: '', })

                            })
                    }
                })
            }
        }
    }
    createFolder = () => { // create a folder
        const { collections } = this.state
        if (this.state.newCollectionName.length > 0) {
            let payload = {
                "name": this.state.newCollectionName,
                userId: '1'
            }
            // -----------------------
            axios.post(`${this.state.env['apiUrl']}/collection/folder`, payload)
                .then(res => {
                    console.log('response', res)
                    collections.push({ name: this.state.newCollectionName })
                    this.setState({ render: false }, () => {
                        this.onUserInfo(true, { status: "success", message: "New Folder has been created successully" })
                        this.setState({ collections, showAddPanel: false, newCollectionName: '', render: true })
                    })
                }).catch(e => {
                    this.onUserInfo(true, { status: "failure", message: "Failure" })
                })
        }
        else {
            this.onUserInfo(true, { status: "failure", message: "Folder Name cannot be empty" })
        }
    }
    deleteCollections = () => {
        let collections = this.state.collections
        collections.map((item, i) => {
            if (!item.default) {
                collections[i]['isDelete'] = true
            }
        })
        this.setState({ render: false, collections }, () => {
            this.setState({ isDelete: true, hideToolbar: true, renameClicked: false, isClone: false, deleteClicked: true, render: true, })
        })
        // const { collections } = this.state
    }
    cancelToolbar = () => {
        let collections = this.state.collections
        collections.map((item, i) => {
            if (!item.default) {
                collections[i]['isEditable'] = false
            }
        })
        this.setState({ render: false, collections }, () => {
            this.setState({ hideToolbar: false, render: true, isDelete: false, isClone: false, renameClicked: false, deleteClicked: false, isModification: false })
        })
    }
    renameCollections = () => {
        let collections = this.state.collections
        collections.map((item, i) => {
            if (!item.default) {
                collections[i]['isEditable'] = true
            }
        })
        this.setState({ collections }, () => {
            this.setState({ hideToolbar: true, renameClicked: true, deleteClicked: false, isDelete: false, isClone: false, createCollectionPanel: false })
        })
    }
    cloneCollections = () => {
        this.setState({ hideToolbar: true, renameClicked: false, deleteClicked: false, isDelete: false, isClone: true, createCollectionPanel: false })
    }
    onCollectionFilter = (item, i) => {
        this.setState({ selectedChip: i }, () => {
            axios.get(`${this.state.env['apiUrl']}/collections?list=${item.id}&page=${this.state.page}&limit=${this.state.limit}&orderBy=DESC`)
                .then(res => {
                    console.log('response', res)
                    if (res.data.length > 0) {
                        this.setState({ cardsCollection: [] }, () => {
                            this.setState({ cardsCollection: res.data.records, cloneCollectionItem: res.data.records })
                        })
                    } else {
                        this.setState({ showEmptyPanel: true })
                    }
                }).catch(e => {
                    console.log('error', e)
                })
        })

    }

    deleteChip = (item) => {
        console.log(item)
        item['type'] = "folder"
        let body = document.getElementsByTagName('body')
        body[0].className = body[0].className + ' sidecontentnav';
        this.setState({ isDeletePanel: true, folderDelete: item })
    }
    handleCollectionItemClick = (item) => {
        if (item.id == "uncategorized") {
            this.props.history.push('/app/uncategorized')
        }
        else {
            let payload = {
                "type": "VIEWED",
                "userId": this.state.userDetails.userId != undefined ? this.state.userDetails.userId != undefined ? this.state.userDetails.userId : null : null
            }
            let cd = {
                "id": "60fab5c057b3bb204aba0b30",
                "name": "(sample)Start your Paperflite journey here!",
                "entityType": "collection",
                "icon": null,
                "poster": null,
                "createdDate": 1627043264253,
                "modifiedDate": 1633617807023,
                "template": "<h2><strong>A Journey to remember, begins now!</strong></h2><p>&nbsp;</p><p>Remember the bewitching tours of places you could never believe you went to, in the first place?</p><p>Think Disneyland, Warner Bros Studio or NASA (or IKEA?!).&nbsp; This On-boarding guide is definitely—a hundred percent—going to be one such tour.&nbsp;</p><p><br>It is going to be a fun look-see around the Paperflite platform and help you understand how you can use the platform in the best possible way.&nbsp;</p><p>&nbsp;</p>",
                "customerLogo": null,
                "collectionTemplate": true,
                "type": "user_defined",
                "assets": [
                    {
                        "id": "60fac4cd2c436914d8056120",
                        "name": "Distributed Architecture Whitepaper",
                        "entityType": "asset",
                        "icon": {
                            "id": "60fac4e994ce93536b32d6df",
                            "type": "image",
                            "thumbnail": "https://d2uav5q06z9nv6.cloudfront.net/60fab5b857b3bb204aba0af2/images/60fac4e994ce93536b32d6df/4c901cef-3e46-43f4-96c8-4420759036ea",
                            "full": "https://d2uav5q06z9nv6.cloudfront.net/60fab5b857b3bb204aba0af2/images/60fac4e994ce93536b32d6df/79fb31c8-b599-4dda-9a66-b519a3bf9fe8",
                            "key": null
                        },
                        "poster": null,
                        "createdDate": 1627047117327,
                        "modifiedDate": 1627047765458,
                        "alias": "distributed-architecture-whitepaper",
                        "autoCreateAlias": true,
                        "excludeFromSearch": false,
                        "summary": "",
                        "tags": [],
                        "metadata": {
                            "content_type": "application/pdf",
                            "modified_date": 1627047118000,
                            "url": "60fab5b857b3bb204aba0af2/assets/20a5e2dc-5253-40dc-8dbf-bef0c6d8e189",
                            "md5_checksum": null,
                            "original_name": "Distributed Architecture Whitepaper.pdf",
                            "content_length": 393951,
                            "extension": "pdf",
                            "etag": "830e5fa5ebf1b28fa832b8bf0b1045b6",
                            "pages": 8
                        },
                        "viewer": "enhanced_viewer",
                        "preview": false,
                        "author": {
                            "id": "60fab5b957b3bb204aba0af5",
                            "firstName": "Karthi",
                            "lastName": "Nagaiyan",
                            "email": "karthi@tribez.ai",
                            "title": null,
                            "workPhone": "",
                            "mobilePhone": "",
                            "emailSignature": null,
                            "icon": {
                                "id": "60fab5b957b3bb204aba0af4",
                                "type": "image",
                                "thumbnail": "https://d2uav5q06z9nv6.cloudfront.net/60fab5b857b3bb204aba0af2/images/60fab5b957b3bb204aba0af4/988d2419-5c9f-404f-aa8a-7e240cc3ee3e",
                                "full": "https://d2uav5q06z9nv6.cloudfront.net/60fab5b857b3bb204aba0af2/images/60fab5b957b3bb204aba0af4/29d2cad6-907d-42a5-8a8f-c27047c13cdc",
                                "key": null
                            },
                            "createdDate": null,
                            "modifiedDate": null,
                            "admin": true,
                            "permissions": null,
                            "consent": null,
                            "lastActivityDate": null,
                            "overrideSSOConfiguration": false
                        },
                        "canEdit": false,
                        "status": "OK",
                        "favorite": false,
                        "source": null,
                        "contentModifiedDate": 1627047118000,
                        "customFields": [
                            {
                                "id": "60fac67d2d65cc3d445d527c",
                                "name": "Content type",
                                "value": [
                                    {
                                        "id": "aedc7011-7460-65ba-cf0a-0b15dec1e6fe",
                                        "value": "Videos"
                                    }
                                ]
                            },
                            {
                                "id": "60fac6cafbf3463399876af2",
                                "name": "Campaign stage",
                                "value": [
                                    {
                                        "id": "32b613a1-f2c1-b222-53f4-ac6f13bb4f23",
                                        "value": "Consideration"
                                    }
                                ]
                            }
                        ],
                        "previewUrl": null,
                        "settings": {
                            "share": true,
                            "download": true
                        },
                        "readingMetrics": null,
                        "viewerMode": null,
                        "canonicalUrl": null,
                        "recommendedTags": null,
                        "officeDocument": false
                    },
                    {
                        "id": "60fac5a32d65cc3d445d525b",
                        "name": "Introduction to Operating System",
                        "entityType": "asset",
                        "icon": {
                            "id": "60fac5aa82216e60989906b3",
                            "type": "image",
                            "thumbnail": "https://d2uav5q06z9nv6.cloudfront.net/60fab5b857b3bb204aba0af2/images/60fac5aa82216e60989906b3/34e1f588-695c-4c7f-a2c5-4d427a0cce96",
                            "full": "https://d2uav5q06z9nv6.cloudfront.net/60fab5b857b3bb204aba0af2/images/60fac5aa82216e60989906b3/2ec4c8ac-4c16-4ff1-a14d-f07e81601e1e",
                            "key": null
                        },
                        "poster": {
                            "id": "60fac5ac82216e60989906b4",
                            "type": "image",
                            "thumbnail": "https://d2uav5q06z9nv6.cloudfront.net/60fab5b857b3bb204aba0af2/images/60fac5ac82216e60989906b4/6aee9c67-6660-4f55-a846-68cdd1432201",
                            "full": "https://d2uav5q06z9nv6.cloudfront.net/60fab5b857b3bb204aba0af2/images/60fac5ac82216e60989906b4/66d1f37b-b2cd-41e0-92c6-d1335b016bc2",
                            "key": null
                        },
                        "createdDate": 1627047331979,
                        "modifiedDate": 1627109611398,
                        "alias": "introduction-to-operating-system-youtube",
                        "autoCreateAlias": true,
                        "excludeFromSearch": false,
                        "summary": "OS: Introduction to Operating System\nTopics Discussed:\n1. Introduction to Operating System (OS)\n2. What is an Operating System (OS)\n3. Types of Operating System (OS)\n4. Functions of Operating System (OS)\n5. Goals of Operating System (OS)\n\nFollow Neso Academy on Instagram: @nesoacademy\n\nContribute: http://www.nesoacademy.org/donate\n\nMemberships: https://bit.ly/2U7YSPI\n\nBooks: http://www.nesoacademy.org/recommended-books\n\nWebsite ► http://www.nesoacademy.org/\nFacebook ► https://goo.gl/Nt0PmB\nTwitter      ► https://twitter.com/nesoacademy\n\nMusic:\nAxol x Alex Skrindo - You [NCS Release]\n\n#OperatingSystemByNeso #os #OperatingSystem",
                        "tags": [
                            "os",
                            "operating system",
                            "operating systems",
                            "os lectures",
                            "os lecture",
                            "os tutorial",
                            "operating system lectures",
                            "operating system tutorial",
                            "operating system in hindi",
                            "operating system concepts",
                            "operating system gate lectures",
                            "gate os lectures",
                            "operating system for gate",
                            "gate operating system",
                            "operating system lectures for gate",
                            "operating system introduction",
                            "introduction to operating system",
                            "gate computer science",
                            "introduction to os",
                            "computer science os"
                        ],
                        "metadata": {
                            "content_type": "application/url",
                            "modified_date": 1627047331979,
                            "url": "https://youtu.be/vBURTt97EkA",
                            "md5_checksum": "d73df4314ce72d4c4d01e81b1279b8a8",
                            "original_name": "Introduction to Operating System - YouTube",
                            "content_length": 0,
                            "external_video_provider": "youtube",
                            "external_video_id": "vBURTt97EkA",
                            "external_video_type": "video/youtube",
                            "video_segment_length": 10,
                            "video_duration": 1005
                        },
                        "viewer": "video_viewer",
                        "preview": false,
                        "author": {
                            "id": "60fab5b957b3bb204aba0af5",
                            "firstName": "Karthi",
                            "lastName": "Nagaiyan",
                            "email": "karthi@tribez.ai",
                            "title": null,
                            "workPhone": "",
                            "mobilePhone": "",
                            "emailSignature": null,
                            "icon": {
                                "id": "60fab5b957b3bb204aba0af4",
                                "type": "image",
                                "thumbnail": "https://d2uav5q06z9nv6.cloudfront.net/60fab5b857b3bb204aba0af2/images/60fab5b957b3bb204aba0af4/988d2419-5c9f-404f-aa8a-7e240cc3ee3e",
                                "full": "https://d2uav5q06z9nv6.cloudfront.net/60fab5b857b3bb204aba0af2/images/60fab5b957b3bb204aba0af4/29d2cad6-907d-42a5-8a8f-c27047c13cdc",
                                "key": null
                            },
                            "createdDate": null,
                            "modifiedDate": null,
                            "admin": true,
                            "permissions": null,
                            "consent": null,
                            "lastActivityDate": null,
                            "overrideSSOConfiguration": false
                        },
                        "canEdit": false,
                        "status": "OK",
                        "favorite": false,
                        "source": null,
                        "contentModifiedDate": 1627047331979,
                        "customFields": [
                            {
                                "id": "60fac67d2d65cc3d445d527c",
                                "name": "Content type",
                                "value": [
                                    {
                                        "id": "48af54fc-42d2-69d0-27ba-97aea74ffc09",
                                        "value": "Blogs"
                                    }
                                ]
                            },
                            {
                                "id": "60fac6cafbf3463399876af2",
                                "name": "Campaign stage",
                                "value": [
                                    {
                                        "id": "c4ca2c99-1098-ff07-d8ac-e5d6bcbe4297",
                                        "value": "Awareness"
                                    }
                                ]
                            }
                        ],
                        "previewUrl": null,
                        "settings": {
                            "share": true,
                            "download": true
                        },
                        "readingMetrics": null,
                        "viewerMode": null,
                        "canonicalUrl": null,
                        "recommendedTags": null,
                        "officeDocument": false
                    }
                ],
                "sections": [
                    {
                        "id": "60fab5c057b3bb204aba0b32",
                        "name": "Guidebook",
                        "entityType": "collection_section",
                        "icon": null,
                        "poster": null,
                        "createdDate": 1627043264273,
                        "modifiedDate": 1627043264273,
                        "assets": []
                    },
                    {
                        "id": "60fab5c057b3bb204aba0b33",
                        "name": "Curate & Discover Content",
                        "entityType": "collection_section",
                        "icon": null,
                        "poster": null,
                        "createdDate": 1627043264299,
                        "modifiedDate": 1627043264299,
                        "assets": []
                    },
                    {
                        "id": "60fab5c057b3bb204aba0b34",
                        "name": "Create Personalised Storyboard",
                        "entityType": "collection_section",
                        "icon": null,
                        "poster": null,
                        "createdDate": 1627043264315,
                        "modifiedDate": 1627043264315,
                        "assets": []
                    },
                    {
                        "id": "60fab5c057b3bb204aba0b35",
                        "name": "Distribute Content",
                        "entityType": "collection_section",
                        "icon": null,
                        "poster": null,
                        "createdDate": 1627043264327,
                        "modifiedDate": 1627043264327,
                        "assets": []
                    },
                    {
                        "id": "60fab5c057b3bb204aba0b36",
                        "name": "Track & Measure Content",
                        "entityType": "collection_section",
                        "icon": null,
                        "poster": null,
                        "createdDate": 1627043264359,
                        "modifiedDate": 1627043264359,
                        "assets": []
                    }
                ],
                "creator": true,
                "background": null,
                "collaboratorsCount": 0,
                "sharedRecipientsCount": 0,
                "assetsCount": 2,
                "sectionsCount": 0,
                "createdBy": {
                    "id": "60fab5b957b3bb204aba0af5",
                    "firstName": "Karthi",
                    "lastName": "Nagaiyan",
                    "email": "karthi@tribez.ai",
                    "title": null,
                    "workPhone": "",
                    "mobilePhone": "",
                    "emailSignature": null,
                    "icon": null,
                    "createdDate": null,
                    "modifiedDate": null,
                    "admin": true,
                },
                "canEdit": true,
                "canDelete": true,
                "banner": null,
                "readAll": false,
                "writeAll": false,
                "live": true
            }
            this.props.history.push({ pathname: `/app/collection/${item.name}`, collectionData: cd })
            axios.get(`${this.state.env['apiUrl']}/collections/list/${item.id}`)
                .then(res => {
                    axios.post(`${this.state.env['apiUrl']}/collections/analytics`, payload)
                        .then(res => {
                            console.log('response', res)
                            this.props.history.push({ pathname: `/app/collection/${item.name}`, collectionData: item })
                        }).catch(e => {
                            this.props.history.push({ pathname: `/app/collection/${item.name}`, collectionData: item })
                        })
                }).catch(e => {
                })
        }
    }
    openNotificationPanel = () => {

    }
    handleSave = () => {
        let renameDatas = []

        this.state.cardsCollection.map(item => {
            if (item.isModified) {
                item['type'] = "collection"
                renameDatas.push(item)
            }
        })
        this.state.collections.map(item => {
            if (item.isModified) {
                item['type'] = "folder"
                renameDatas.push(item)
            }
        })
        if (renameDatas.length > 0) {
            renameDatas.map(item => {
                let url = item.type == "folder" ? "collection/folder" : `collections/list/${item.id}`
                let payload = { "id": item.id, "name": item.name }
                axios.put(`${this.state.env['apiUrl']}/${url}`, payload)
                    .then(res => {
                        this.onUserInfo(true, { status: "success", message: `${item.type} has been renamed successully` })
                        this.setState({ hideToolbar: false, isDelete: false, renameClicked: false, isModification: false })
                    }).catch(e => {
                        this.onUserInfo(true, { status: "Failure", message: "Failure" })
                        this.setState({ hideToolbar: false, isDelete: false, renameClicked: false, isModification: false })
                    })
            })
        }

    }
    handleDelete = () => {
        let body = document.getElementsByTagName('body')
        body[0].className = body[0].className + ' sidecontentnav';
        let collection = this.state.collections
        this.setState({ render: false }, () => {
            this.setState({ render: true, collections: collection, hideToolbar: false, isDelete: false, renameClicked: false, isDeletePanel: true, createCollectionPanel: false })
        })
    }

    closeClick = () => {
        let body = document.getElementsByTagName('body')
        body[0].className = body[0].className.replace(' sidecontentnav')
        this.setState({ isClonePanel: false, isDeletePanel: false, isDelete: false, hideToolbar: false, renameClicked: false, isClone: false, createCollectionPanel: false, deleteItems: [] })
    }
    //open the sidebar
    openClick = (e, item, idx) => {
        // this.sidebarobj.show();
        let deleteDetails = this.state.deleteItems
        if (this.state.isClone) {
            let body = document.getElementsByTagName('body')
            body[0].className = body[0].className + ' sidecontentnav';
            this.setState({ cloneCollectionDetails: item }, () => {
                this.setState({ isClonePanel: true, createCollectionPanel: false })
            })
        }
        else {
            this.state.cardsCollection.map((item1, index) => {
                if (index == idx && e.checked == true) {
                    deleteDetails.push(item1)
                }
                else if (e.checked == false && item1.name == item.name) {
                    deleteDetails.filter((d, i) => {
                        if (item.name == d.name) {
                            deleteDetails.splice(i, 1)
                        }
                    })
                }
            })
            this.setState({ deleteItems: deleteDetails })
        }

    }

    content = () => {
        let deleteDetails = this.state.folderDelete != undefined ? this.state.folderDelete : this.state.deleteItems;
        return (
            this.state.createCollectionPanel ?
                <CreateNewCollection closeClick={this.closeClick} /> :
                this.state.isDeletePanel ?
                    <DeleteCollection details={deleteDetails} /> :
                    this.state.isClonePanel ?
                        <CloneCollection cloneCollectionDetails={this.state.cloneCollectionDetails} /> : null)
    }
    selectSortItem = (item) => {
        this.setState({ sortItem: item.name })
        this.onFilter(item.value)
    }
    onClone = () => {
        let payload = { "name": "TClone", "collectionId": "60fe7a71064b80462bf48f7b" }
        axios.post(`${this.state.env['apiUrl']}/collections`, payload)
            .then(res => {
                console.log('response', res)
                this.setState({ userInfoShow: true, userInfo: { status: "success", message: "Collection cloned successully" } })
                this.closeClick()
            }).catch(e => {
                this.setState({ userInfoShow: true, userInfo: { status: "failure", message: "Failure" } })
                this.closeClick()
            })
    }
    onDelete = () => {
        if (this.state.folderDelete) {
            let payload = {
                id: this.state.folderDelete.id
            }
            axios.delete(`${this.state.env['apiUrl']}/collection/folder`, { data: payload })
                .then(res => {
                    console.log('response', res)
                    this.onUserInfo(true, { status: "success", message: `${this.state.folderDelete.name} folder has been deleted successfully.` }, () => {
                        let collections = []
                        this.state.collections.map((item, i) => {
                            if (this.state.folderDelete.id != item.id) {
                                collections.push(item)
                            }
                        })
                        this.setState({ collections }, () => {
                            this.setState({ folderDelete: undefined })
                        })
                    })
                }).catch(e => {
                    this.onUserInfo(true, { status: "failure", message: `failure` }, () => { })
                })
        } else {
            this.state.deleteItems.map((item, idx) => {
                axios.delete(`${this.state.env['apiUrl']}/collections/list/${item.id}?userId=${this.state.userDetails.userId != undefined ? this.state.userDetails.userId != undefined ? this.state.userDetails.userId : null : null}`)
                    .then(res => {
                        if (idx == this.state.deleteItems.length) {
                            this.onUserInfo(true, { status: "success", message: "Collection Item has been deleted successully" })
                        }
                        console.log('response', res)
                    }).catch(e => {
                        this.onUserInfo(true, { status: "failure", message: "Failure" })
                    })
            })
        }

    }
    onEditCollectionName = (e, item, i) => {
        let cardsCollection = this.state.cardsCollection
        cardsCollection.map((data, di) => {
            cardsCollection[di]['focus'] = false
        })
        cardsCollection[i]['name'] = e.target.value
        cardsCollection[i]['focus'] = true
        cardsCollection[i]['isModified'] = true
        console.log(cardsCollection)
        // console.log(allCollectionResponse.records)
        this.setState({ cardsCollection: [] }, () => {
            this.setState({ cardsCollection })
        })
    }
    createCollection = () => {
        let body = document.getElementsByTagName('body')
        body[0].className = body[0].className + ' sidecontentnav';
        this.setState({ createCollectionPanel: true })
    }
    onUserInfo = (status, msg) => {
        this.setState({ userInfoShow: true, userInfo: msg }, () => {
            setTimeout(() => {
                this.setState({ userInfoShow: false })
                this.closeClick()
            }, 2000)
        })
    }
    chipInputChange = (e, i) => {
        let collections = this.state.collections
        collections.map((data, di) => {
            collections[di]['focus'] = false
        })
        collections[i]['name'] = e.target.value
        collections[i]['focus'] = true
        collections[i]['isModified'] = true
        this.setState({ collections: [] }, () => {
            this.setState({ collections })
        })
    }
    render() {
        return (
            <React.Fragment>
                <div className="_app-mainbackground page-container">
                    <div className="col-lg-12 col-sm-12 col-md-12 header-menu" >
                        <ProfileHeader userName={"Karthi"} openNotificationPanel={this.openNotificationPanel} />
                    </div>
                    <div>
                        <PageTitle title={"Collections"} subTitle={"personalized content storyboards"} searchBar={true} />
                        {!this.state.hideToolbar ?
                            <div className="toolbars-wrapper">
                                <div className="collection-toolbar">
                                    <div className="toolbars">
                                        <div className="toolbar-tab" data-testid="rename" onClick={this.renameCollections}>RENAME</div>
                                        <div className="separator"></div>
                                        <div className="toolbar-tab" data-testid="delete" onClick={this.deleteCollections}>DELETE</div>
                                        <div className="separator"></div>
                                        <div className="toolbar-tab" data-testid="clone" onClick={this.cloneCollections}>CLONE</div>
                                    </div>
                                </div>
                                <div className="sort-action-toolbar" style={{ position: 'relative' }}>
                                    <p className="sort-by" style={{ position: 'relative' }}
                                        onClick={() => {
                                            let boolean = this.state.showSortDown;
                                            this.setState({ showSortDown: !boolean });
                                        }}>Sort By: <span style={{ color: '#0ba2e3', fontWeight: '600' }}>{this.state.sortItem}</span>
                                        <i className="arrow down"></i>
                                    </p>

                                    {this.state.showSortDown && <div className="sort-dropdown">
                                        {this.state.moreNavList.map((item, index) => {
                                            return (
                                                <p className={`sort-item ${this.state.sortItem === item.name ? 'active' : ''}`} onClick={() => { this.selectSortItem(item); this.setState({ showSortDown: false }); }}>{item.name}</p>
                                            )
                                        })}</div>}
                                </div>
                            </div> :
                            <div className="toolbars-wrapper info">
                                <div className="toolbars">
                                    {this.state.deleteClicked ?
                                        <p>Select content below and click here to
                                            <ButtonComponent cssClass='e-primary small-btn' onClick={this.handleDelete}>Delete</ButtonComponent> or
                                            <ButtonComponent cssClass='e-primary small-btn' onClick={this.cancelToolbar}>Cancel</ButtonComponent>
                                        </p> : (
                                            this.state.renameClicked ?
                                                <p>Rename the collections and click here
                                                    <ButtonComponent cssClass='e-primary small-btn' onClick={this.handleSave}>Save</ButtonComponent> or
                                                    <ButtonComponent cssClass='e-primary small-btn' onClick={this.cancelToolbar}>Cancel</ButtonComponent>
                                                </p> :
                                                <p>Select on a collection to be cloned or
                                                    <ButtonComponent cssClass='e-primary small-btn' onClick={this.cancelToolbar}>Cancel</ButtonComponent>
                                                </p>)}
                                </div>
                            </div>
                        }
                        <hr className="hr-tag"></hr>
                        <div className="buttons-wrapper">
                            {this.state.render ? <React.Fragment>
                                {this.state.collections.length > 0 ? <div className="custom-chip-wrap">
                                    <div className="custom-chip-container">
                                        <p onClick={this.addNewFolder.bind(this)}>+ Add New Folder</p>
                                    </div>
                                    {this.state.collections.map((item, index) => {
                                        return (<div className={this.state.selectedChip == index ? "custom-chip-container active-chip" : "custom-chip-container"} key={index}>
                                            {!item.default ? <div>
                                                {item.isEditable ? <input value={item.name} autoFocus={item.focus} className={"custom-chip-input"} onChange={(e) => this.chipInputChange(e, index)} /> : <p onClick={() => this.onCollectionFilter(item, index)}>{item.name}</p>}
                                                {item.isDelete ? <i class="fas fa-times" onClick={() => this.deleteChip(item, index)}></i> : null}</div> : <p onClick={() => this.onCollectionFilter(item, index)}>{item.name}</p>}
                                        </div>)
                                    })}
                                </div> : null}
                                {/* <ChipListComponent id="chip-collection" data-testid="chip-collection" onClick={this.addNewFolder.bind(this)} selectedChips={this.state.selectedChip} selection="Single">
                                    <ChipsDirective>
                                        <ChipDirective cssClass="chip-class" key="0" text="+ Add New Folder" ></ChipDirective>
                                        {this.state.collections.filter(item => item.default).map((item, index) => {
                                            return (<ChipDirective cssClass="chip-class" key={index + 1} text={item.name}>
                                            </ChipDirective>)
                                        })}
                                    </ChipsDirective>
                                </ChipListComponent>
                                <ChipListComponent id="chip-collection" ref={this.chipRef} selection={"Single"} enableDelete={this.state.isDelete} delete={this.deleteChip} onClick={this.onCollectionFilter.bind(this)} >
                                    <ChipsDirective>
                                        {this.state.collections.filter(item => !item.default).map((item, index) => {
                                            return (<ChipDirective cssClass="chip-class" key={index + 1} text={item.name}>
                                            </ChipDirective>)
                                        })}
                                    </ChipsDirective>
                                </ChipListComponent> */}
                            </React.Fragment>
                                : null}
                        </div>

                        {this.state.showAddPanel ?
                            <div className="addCollection-wrapper">
                                <TextBoxComponent
                                    style={{ width: '100%' }}
                                    type="text"
                                    className="addcollection-input"
                                    placeholder="Enter a folder name"
                                    onChange={(e) => { this.setState({ newCollectionName: e.target.value }) }}
                                    value={this.state.newCollectionName}
                                />
                                <p className="create-cltn-btn" onClick={this.createFolder}>CREATE</p>
                                <p className="cancel-cltn-btn" onClick={() => { this.setState({ showAddPanel: false, newCollectionName: '' }) }}>CANCEL</p>
                            </div>
                            : null}
                        {
                            this.state.showEmptyPanel ?
                                <div className="empty-list-box">
                                    <img alt="empty" style={{ width: '300px', marginTop: '20px' }}
                                        src="https://app.paperflite.com/public/6ecf85fca1424cc5c1698e8f621a7c3f.svg"></img>
                                    <p className="empty-list-text1">Are you Everest? Because this folder is M-T!</p>
                                    <p className="empty-list-text2">{this.state.selectedCollectionName} collections will show up here!</p>
                                </div>
                                :
                                <div className="cards-wrapper">
                                    <div className="cards-collection" onClick={this.createCollection}>
                                        <div className="card-box">
                                            <img className="addnew-img default"
                                                alt="Create New Collection"
                                                src="data:image/svg+xml;base64,PHN2ZyBpZD0iTGF5ZXJfMSIgZGF0YS1uYW1lPSJMYXllciAxIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAxMDQuMTIgMTA0LjEyIj48ZGVmcz48c3R5bGU+LmNscy0xe2ZpbGw6I2U0MTg1OTtzdHJva2U6I2U0MTg1OTtzdHJva2UtbWl0ZXJsaW1pdDoxMDtzdHJva2Utd2lkdGg6NXB4O308L3N0eWxlPjwvZGVmcz48cGF0aCBjbGFzcz0iY2xzLTEiIGQ9Ik05Ny41OCw1NWgtOTFhMi45NCwyLjk0LDAsMCwxLDAtNS44N2g5MWEyLjk0LDIuOTQsMCwwLDEsMCw1Ljg3WiIvPjxwYXRoIGNsYXNzPSJjbHMtMSIgZD0iTTUyLjA2LDEwMC41MWEyLjk0LDIuOTQsMCwwLDEtMi45NC0yLjkzaDB2LTkxYTIuOTQsMi45NCwwLDAsMSw1Ljg3LDB2OTFBMi45MywyLjkzLDAsMCwxLDUyLjA2LDEwMC41MVoiLz48L3N2Zz4="></img>
                                        </div>
                                        <p className="card-name">{"Create New Collection"}</p>
                                    </div>
                                    {this.state.cardsCollection && this.state.cardsCollection.length > 0 ? this.state.cardsCollection.map((item, index) => {
                                        return (
                                            <React.Fragment>
                                                <div className={`cards-collection ${this.state.isDelete || this.state.renameClicked || this.state.isClone ? 'hide-transition' : ''}`} >
                                                    <div className="card-box" onClick={() => this.handleCollectionItemClick(item)}>
                                                        <img className="addnew-img" alt={item.name}
                                                            src={String(item.name).toLowerCase().includes('assets library') ? this.state.uncategorizedImage : (item.icon != undefined || item.icon != null ? item.icon.thumbnail : defaultImage)}>
                                                        </img>
                                                    </div>
                                                    {!item.name.toLowerCase().includes('assets library') && (this.state.isDelete !== undefined && this.state.isDelete) || (!item.name.toLowerCase().includes('assets library') && this.state.isClone) ?
                                                        <p className="card-name">{item.name}<CheckBoxComponent change={(e) => this.openClick(e, item, index)} cssClass="check-box-modify" /></p>
                                                        : (!item.name.toLowerCase().includes('assets library') && this.state.renameClicked) ?
                                                            <> <textarea value={item.name} id={`collection${index}`} autoFocus={item.focus} className="edit-collection"
                                                                onChange={(e) => { this.onEditCollectionName(e, item, index) }}
                                                                style={{
                                                                    marginTop: 10, width: "100%", border: 'none', borderBottom: '1px solid #dbdbdb', background: 'transparent',
                                                                    color: item.modified != undefined ? item.modified ? "#0ba2e3" : "#000" : "#000"
                                                                }} />
                                                            </>
                                                            : <p className="card-name">{item.name}</p>}
                                                </div>
                                            </React.Fragment>)
                                    }) : null
                                    }
                                </div >
                        }
                    </div>
                </div >
                <div className="clone-panel-wrap">{this.state.isClonePanel && <SidePreviewPanel content={this.content()} action={true} submitBtn={"Clone"} cancelBtn={"Cancel"} cancelClick={() => this.closeClick()} submitClick={() => this.onClone()} closeClick={() => this.closeClick()} />}
                </div>
                <div className="delete-panel-wrap">{this.state.isDeletePanel && <SidePreviewPanel content={this.content()} action={true} submitBtn={"Delete"} cancelBtn={"Cancel"} cancelClick={() => this.closeClick()} submitClick={() => this.onDelete()} closeClick={() => this.closeClick()} />}
                </div>
                <div className="create-stream-wrap new-collection">{this.state.createCollectionPanel && <SidePreviewPanel content={this.content()} action={false} closeClick={() => this.closeClick()} />}
                </div>
                {this.state.userInfoShow ? <div><UserAlertInformation info={this.state.userInfo} /></div> : null}

            </React.Fragment >
        )
    }
}
export default withRouter(Collections);