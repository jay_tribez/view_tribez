import React, { useEffect, useState } from 'react';
import { withRouter } from 'react-router';
import "../CollectionItem/CollectionItem.scss";
import "../hub/index.scss";
import "../CollectionItem/previewNavItem.scss"
import axios from 'axios';

function ResourceNotFound(props) {
    return (
        <div className="resource-wrap-container">
            <div className="resource-wrap">
                <div className="notfound-text">
                    <h1>404</h1>
                </div>
                <div className="notfound-info">
                    <p>
                        Oops...The page that you are looking
                        <br />for does not exist!Luckily enough we,
                        <br />have some pages that do exist
                    </p>
                </div>
            </div>
        </div>
    )
}


export default withRouter(ResourceNotFound)
