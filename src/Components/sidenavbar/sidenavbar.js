import React, { Component } from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    useRouteMatch,
    useParams
} from "react-router-dom";
import './index.css';

import { SidebarComponent } from '@syncfusion/ej2-react-navigations';
import { MenuComponent } from '@syncfusion/ej2-react-navigations';
import { SampleBase } from './sample-base';
import routes from "../../router"
const menuItems = require('./sidenavbar.json')
// export class SidebarWithMenu extends React.Component {
class SidebarWithMenu extends Component {
    constructor(props) {
        super(props);
        this.state = {}
        this.mediaQuery = '(min-width: 600px)';
        this.menuItems = menuItems;
        this.enableDock = true;
        this.dockSize = '52px';
        this.width = '220px';
        this.target = '.main-content';
    }
    componentDidMount = () => {
        this.openClick()
    }
    onMenuClick = (item) => {
        console.log('item', item.item.text)
        let url = String(item.item.text).toLowerCase()
        this.props.history.push(`/main/${url}`)
    }
    render() {
        return (<div className="control-section sidebar-menu-wrap">
            <div id="wrapper">
                <div className="col-lg-12 col-sm-12 col-md-12">
                    <SidebarComponent id="sidebar-menu"
                        // ref={Sidebar => this.dockBar = Sidebar}
                        ref={Sidebar => this.sidebarobj = Sidebar}
                        // enableDock={this.enableDock}
                        enableDock={true}
                        // mediaQuery={this.mediaQuery}
                        // dockSize={this.dockSize}
                        dockSize={'52px'}
                        isOpen={true}
                        width='220px'
                        target={this.target}>

                        <div className="main-menu">
                            <p className="main-menu-header">MAIN</p>
                            <MenuComponent items={this.menuItems} select={this.onMenuClick} orientation='Vertical' cssClass='dock-menu'></MenuComponent>
                        </div>
                        <div className="action">
                            <p className="main-menu-header">ACTION</p>
                            <button className="e-btn action-btn" id="action-button">+ Button</button>
                        </div>
                    </SidebarComponent>
                    <div className="main-content" id="maintext">
                        <div className="sidebar-menu-content">
                            {routes.map((route, idx) => {
                                return route.component ? (
                                    <Route
                                        key={idx}
                                        path={route.path}
                                        exact={route.exact}
                                        name={route.name}
                                        render={props => (
                                            <route.component {...props} />
                                        )} />
                                ) : (null);
                            })}
                        </div>
                    </div>
                </div>
            </div>
        </div>);
    }
    //open the sidebar
    openClick = () => {
        console.log(this.sidebarobj.isOpen)
        if (this.sidebarobj.isOpen) {
            this.sidebarobj.hide();
        }
    }
}
export default SidebarWithMenu;