
const colors = {
    bodyBG: '#F9F9F9',
    primary: '#0ba2e3',
    secondary: '#3d4465',
    white: "#fff",
    black: "#000",
    btnHover: '#24274e',
    inputBorder: 'red',
    inputBg: '#fff',
    inputText: '#6A707E00',
    inputFocus: '#f8d5c5',
    labelText: '#7e7e7e',
    textGrey: '#6A707E',
    primaryLight: 'rgb(11 162 227 / 10%);',
    textLight: '#ABAFB3',
    sideMenuActiveText: '#23274E',
    sideMenuActiveBg: '#DAEDF6',
    success: '#14A54D',
    warning: '#F89747',
    lightBg: 'rgb(1 152 215 / 10%)',
    fontPrimary: '#0067E1',
    contentBg: '#F3F6F9',
    darkTitle: '#212529'
}
const fontSize = {
    inputBorder: '1px',
    inputText: '0.875rem',
    titleFont: '16px',
    primaryFontSize: '14px',
    secFontSize: '12px',
    titleSize: '1.375rem'
}
const layouts = {
    primaryButton: {
        background: colors.sideMenuActiveText,
        borderRadius: '24px',
        color: colors.white,
        width: '100%',
        ":hover": {
            background: colors.btnHover,
            borderColor: colors.btnHover
        }
    },
    primaryBtn: {
        background: colors.primary,
        color: colors.white,
        width: '100%',
        height: '50px',
        lineHeight: '50px',
        borderColor: colors.primary,
        borderRadius: '15px',
        fontWeight: '700',
        fontSize: '0.813rem',
        ":hover": {
            background: colors.btnHover,
            borderColor: colors.btnHover
        }
    },
    secondaryBtn: {
        background: colors.white,
        color: colors.primary,
        width: '100%',
        border: `1px solid ${colors.primary}`,
        borderRadius: '4px',
        fontSize: '0.75rem',
        ":hover": {
            background: colors.primary,
            color: colors.white,

        }
    },
    inputStyle: {
        marginTop: '0.25rem',
        ".MuiOutlinedInput-input": {
            background: `${colors.inputBg} !important`,
            border: `${fontSize.inputBorder} solid ${colors.inputBorder}`,
            color: `${colors.inputText} !important`,
            fontSize: fontSize.inputText,
            padding: '0.375rem 0.75rem',
            borderRadius: '0.75rem',
            // height: '41px',
            ":before": { display: 'none' },
            ":after": { display: 'none' },
            ":focus": {
                borderColor: colors.inputFocus
            }
        },
        ".MuiOutlinedInput-notchedOutline":{
            border: `${fontSize.inputBorder} solid ${colors.inputBorder}`,
        },
        ".MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-notchedOutline":{
            borderColor: colors.inputFocus
        }
    },

    titleStyle: {
        fontWeight: 500,
        fontSize: '24px',
        color: colors.textGrey,
        textAlign: 'center',
        letterSpacing: "0.32px"
    },

    labelStyleBold: {
        fontWeight: 'bolder',
        fontSize: '16px',
        color: colors.labelText,
        letterSpacing: "0.32px"
    },

    labelStyle: {
        fontSize: '16px',
        color: colors.labelText,
        letterSpacing: "0.32px"
    },

    labelStyleLink: {
        fontSize: '16px',
        color: colors.labelText,
        letterSpacing: "0.32px",
        cursor: 'pointer',
        ":hover": {
            color: colors.primary
        }
    },
    mainTitle: {
        fontSize: '1.2rem',
        color: colors.fontPrimary
    },
    darkLabel: {
        fontSize: '1.375rem',
        color: colors.darkTitle
    }
}

const images = {
}

const tribzTheme = {
    colors,
    fontSize,
    layouts,
    images
};

export default tribzTheme;