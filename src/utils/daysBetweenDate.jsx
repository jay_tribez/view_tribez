import moment from "moment";

const DaysBetweenDate = (date, mi) => {
    let momentTime = moment(date);
    let today = moment();
    let num = mi === undefined ? today.diff(momentTime, "minutes") : mi;
    let d = Math.floor(num / 1440); // 60*24
    let h = Math.floor((num - d * 1440) / 60);
    let m = Math.round(num % 60);
    let s = Number(((num - d * 24 * 60 - h * 60 - m) * 60).toFixed(0));
    let result = ""
    switch (true) {
        case (d > 0):
            result = d > 7 ? Math.round(d / 7) : Math.ceil(d);
            result = d > 7 ? `${result} ${result === 1 ? "week" : "weeks"} ago` : `${result} ${result === 1 ? "day" : "days"} ago`;
            break;
        case (m === 0 && s === 0):
            result = "now";
            break;
        case (h === 0 && m === 0):
            result = (s > 9 ? s : ("0" + s)) + " secs ago";
            break;
        case (h === 0):
            result = (m > 9 ? m : ("0" + m)) + " mins ago";
            break;

        default:
            result = (h > 9 ? h : ("0" + h)) + ":" + (m > 9 ? m : ("0" + m));
            break;
    }
    return result;

}
export default DaysBetweenDate