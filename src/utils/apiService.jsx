import axios from 'axios';
import React from 'react';
import UserAlertInformation from "../Components/Common/validation/alertInformation";
let env = JSON.parse(localStorage.getItem("env"))
class TribezService extends React.Component {
    async apiService(name, method, baseUri, url, payload, header) {
        let headers = {}
        let authDetails = JSON.parse(localStorage.getItem("authDetails"))
        if (header != undefined) {
            headers = header
            headers['Authorization'] = authDetails['access_token']
        } else {
            headers['Authorization'] = authDetails['access_token']
        }
        switch (method) {
            case "post":
                await axios.post(`${env[baseUri]}${url}`, payload, { headers: headers })
                    .then(async response => {
                        this.alerInfo({ status: "success", message: `${name} has been added successully` })
                        return response
                    }).catch(async err => {
                        console.log(err)
                        this.alerInfo({ status: "failure", message: `${name} failure` })
                        return { "failed": true }
                    })

                break;
            case "get":
                await axios.get(`${env[baseUri]}${url}`, { headers: headers })
                    .then(async response => {
                        console.log(response)
                        this.alerInfo({ status: "success", message: `${name} has been fetched successully` })
                        return response
                    }).catch(async err => {
                        console.log(err)
                        this.alerInfo({ status: "failure", message: `${name} failure` })
                        return { "failed": true }
                    })
                break;
            case "put":
                await axios.put(`${env[baseUri]}${url}`, payload, { headers: headers })
                    .then(async response => {
                        this.alerInfo({ status: "success", message: `${name} has been updated successully` })
                        return response
                    }).catch(async err => {
                        console.log(err)
                        this.alerInfo({ status: "failure", message: `${name} failure` })
                        return { "failed": true }
                    })
                break;

            default:
                await axios.delete(`${env[baseUri]}${url}`, { data: payload }, { headers: headers })
                    .then(async response => {
                        this.alerInfo({ status: "success", message: `${name} has been fetched successully` })
                        return response
                    }).catch(async err => {
                        console.log(err)
                        this.alerInfo({ status: "failure", message: `${name} failure` })
                        return { "failed": true }
                    })
                break;
        }
    }
    async alerInfo(userInfo) {
        let body = document.getElementsByTagName("body")['0']
        console.log('body', body)
        let createDiv = document.createElement('div')
        var att = document.createAttribute("class");       // Create a "class" attribute
        att.value = `toast-wrap-${userInfo.status}`;                           // Set the value of the class attribute
        createDiv.setAttributeNode(att);
        let createTextNode = document.createTextNode(userInfo.message)
        createDiv.appendChild(createTextNode)
        body.appendChild(createDiv)
        setTimeout(() => {
            let toastElt = document.getElementsByClassName(`toast-wrap-${userInfo.status}`)
            for (let i = 0; i < toastElt.length; i++) {
                let element = toastElt[i];
                element.remove()

            }
        }, 2000)
        return <h1>failure</h1>
    }
}

export default TribezService;