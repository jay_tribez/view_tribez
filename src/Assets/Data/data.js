
const myStream = [
    { "id": 1, "name": "Blogs", "hasChild": true, "isChecked": true },
    { "id": 2, "pid": 1, "name": "Special Blogs" },

    { "id": 3, "name": "Computer", "hasChild": true },
    { "id": 4, "pid": 3, "name": "JN Groups" },
    { "id": 5, "pid": 3, "name": "Tribez", "hasChild": true },
    { "id": 6, "pid": 5, "name": "Tata" },

    { "id": 7, "name": "Guides", "hasChild": true },
    { "id": 8, "name": "Off Roading", "hasChild": true },
    { "id": 9, "name": "Paperflite", "hasChild": true },
    { "id": 10, "pid": 9, "name": "Tribez" },

    { "id": 11, "name": "Sample Paperflite Stream", "hasChild": true, },
    { "id": 12, "pid": 11, "name": "FRU Group" },

    { "id": 13, "name": "TGIFS", "hasChild": true },
    { "id": 14, "pid": 13, "name": "PDF Small" },

    { "id": 15, "name": "Welcome to Paperflite!", "hasChild": true, },
    { "id": 16, "pid": 15, "name": "(sample)How-to's", "hasChild": true, },
    { "id": 17, "pid": 16, "name": "(sample)PDFs" },
    { "id": 18, "pid": 16, "name": "(sample)Videos" },

    { "id": 19, "name": "White papers", "hasChild": true },
]
const myCollection = [
    { "id": 1, "name": "(sample)Meet Paperflite", "hasChild": true, "isChecked": true },
    { "id": 2, "pid": 1, "name": "Sub Section" },
    { "id": 3, "pid": 1, "name": "Main Section" },
    { "id": 4, "name": "(sample)Start your Paperflite journey here!", "hasChild": true },
    { "id": 5, "pid": 4, "name": "Guidebook" },
    { "id": 6, "pid": 4, "name": "Curate & Discover Content" },
    { "id": 7, "pid": 4, "name": "Create Personalised Storyboard" },
    { "id": 8, "pid": 4, "name": "Distribute Content" },
    { "id": 9, "pid": 4, "name": "Track & Measure Content" },
    { "id": 10, "name": "ALK Demo", "hasChild": true, },
    { "id": 11, "name": "Tata AIA Life Insurance", "hasChild": true, },
    { "id": 12, "name": "TGIFS", "hasChild": true, },
    { "id": 13, "name": "TGIFS-1", "hasChild": true, },
    { "id": 14, "name": "Tribez", "hasChild": true, },
    { "id": 15, "name": "Tribez.ai - An overview", "hasChild": true },
    { "id": 16, "pid": 15, "name": "Intro to Tribez.ai" },
    { "id": 17, "pid": 15, "name": "Others" }
]

const data = { myStream, myCollection }
export default data