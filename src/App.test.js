// app.test.js
import { render, screen } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import { createMemoryHistory } from 'history'
import React, { Component, Suspense } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useRouteMatch,
  useParams,
  withRouter
} from "react-router-dom";

import '@testing-library/jest-dom'

import App from './app';
import Loader from "./Components/loader"
import LanguageSettings from "./languageSettings"
import AppLayout from "./appLayout";
test('full app rendering/navigating', () => {
  const history = createMemoryHistory()
  
  history.push('/app')
  render(
    <Router history={history}>
      <React.Suspense fallback="loading">
        <AppLayout />
      </React.Suspense>

    </Router>,
  )

  // // verify page content for expected route
  // // often you'd use a data-testid or role query, but this is also possible
  // expect(screen.getByText(/you are home/i)).toBeInTheDocument()

  // const leftClick = {button: 0}
  // userEvent.click(screen.getByText(/about/i), leftClick)

  // // check that the content changed to the new page
  // expect(screen.getByText(/you are on the about page/i)).toBeInTheDocument()
})

// test('landing on a bad page', () => {
//   const history = createMemoryHistory()
//   history.push('/some/bad/route')
//   render(
//     <Router history={history}>
//       <App />
//     </Router>,
//   )

//   expect(screen.getByText(/no match/i)).toBeInTheDocument()
// })

// test('rendering a component that uses useLocation', () => {
//   const history = createMemoryHistory()
//   const route = '/some-route'
//   history.push(route)
//   render(
//     <Router history={history}>
//       <LocationDisplay />
//     </Router>,
//   )

//   expect(screen.getByTestId('location-display')).toHaveTextContent(route)
// })